package com.qsl;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/5/10 17:10
 **/
public class TemplateFileLocateTest {

    public static void main(String[] args) throws IOException {
        String pdfFilePath = "template/hello.txt";
        Resource resource = new ClassPathResource(pdfFilePath);
        InputStream inputStream = resource.getInputStream();
        BufferedReader bis = new BufferedReader(new InputStreamReader(inputStream));
        String s = bis.readLine();
        System.out.println(s);
        bis.close();
        inputStream.close();
    }
}
