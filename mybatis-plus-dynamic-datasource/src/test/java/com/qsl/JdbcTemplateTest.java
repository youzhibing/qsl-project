package com.qsl;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.druid.sql.SQLUtils;
import com.lee.Application;
import com.lee.meta.ColumnMeta;
import com.lee.meta.IndexColumnMeta;
import com.lee.meta.IndexMeta;
import com.lee.meta.TableMeta;
import com.lee.sql.DBSql;
import com.lee.sql.TableQueryParam;
import com.lee.type.IndexTypeEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2024/2/7 14:46
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class JdbcTemplateTest {

    @Resource
    private DataSource dataSource;

    @Test
    public void test() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet("select * from tbl_order");
        SqlRowSetMetaData rsmd = sqlRowSet.getMetaData();
        int columnNum = rsmd.getColumnCount();
        while(sqlRowSet.next()) {
            for (int i = 1; i <= columnNum; i++) {
                Object obj = sqlRowSet.getObject(i);
                System.out.println(obj.getClass().getName());
                System.out.println(obj);
                System.out.println("================");
            }
        }
    }

    @Test
    public void getTableMeta() throws SQLException {
        Connection connection = dataSource.getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        ResultSet tableResultSet = databaseMetaData.getTables(connection.getCatalog(), connection.getSchema(),
                "tbl_sync", new String[]{"TABLE"});
        while (tableResultSet.next()) {
            System.out.println("tableCatalog = " + tableResultSet.getString("TABLE_CAT"));
            System.out.println("tableSchema = " + tableResultSet.getString("TABLE_SCHEM"));
            System.out.println("tableName = " + tableResultSet.getString("TABLE_NAME"));
            System.out.println("tableType = " + tableResultSet.getString("TABLE_TYPE"));
            System.out.println("remarks = " + tableResultSet.getString("REMARKS"));
        }
        tableResultSet.close();
        connection.close();
    }

    @Test
    public void getColumnMeta() throws SQLException {
        Connection connection = dataSource.getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        ResultSet columnResultSet = databaseMetaData.getColumns(connection.getCatalog(), connection.getSchema(),
                "tbl_sync", null);
        while (columnResultSet.next()) {
            System.out.println("ColumnName = " + columnResultSet.getString("COLUMN_NAME"));
            System.out.println("ColumnType = " + columnResultSet.getInt("DATA_TYPE"));
            System.out.println("ColumnTypeName = " + columnResultSet.getString("TYPE_NAME"));
            System.out.println("isAutoIncrement = " + columnResultSet.getString("IS_AUTOINCREMENT"));
            System.out.println("isNullable = " + columnResultSet.getString("IS_NULLABLE"));
            System.out.println("Precision = " + columnResultSet.getInt("COLUMN_SIZE"));
            System.out.println("Scale = " + columnResultSet.getInt("DECIMAL_DIGITS"));
            System.out.println("DefaultValue = " + columnResultSet.getString("COLUMN_DEF"));
            System.out.println("Remarks = " + columnResultSet.getString("REMARKS"));
            System.out.println("===================================");
        }
        columnResultSet.close();
        connection.close();
    }

    @Test
    public void getPrimaryKey() throws SQLException {
        Connection connection = dataSource.getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        ResultSet primaryKeysResultSet = databaseMetaData.getPrimaryKeys(connection.getCatalog(), connection.getSchema(), "tbl_sync");
        while (primaryKeysResultSet.next()) {
            String columnName = primaryKeysResultSet.getString("COLUMN_NAME");
            short keySeq = primaryKeysResultSet.getShort("KEY_SEQ");
            String pkName = primaryKeysResultSet.getString("PK_NAME");
            System.out.println(columnName + " - " + keySeq + " - " + pkName);
        }
        primaryKeysResultSet.close();
        connection.close();
    }

    @Test
    public void getIndex() throws SQLException {
        Connection connection = dataSource.getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        ResultSet indexResultSet = databaseMetaData.getIndexInfo(connection.getCatalog(), connection.getSchema(), "tbl_sync", false, false);
        while (indexResultSet.next()) {
            String indexName = indexResultSet.getString("INDEX_NAME");
            String columnName = indexResultSet.getString("COLUMN_NAME");
            boolean nonUnique = indexResultSet.getBoolean("NON_UNIQUE");
            short ordinalPosition = indexResultSet.getShort("ORDINAL_POSITION");
            System.out.println(columnName + " - " + ordinalPosition + " - " + indexName +  " - " + nonUnique);
        }
        indexResultSet.close();
        connection.close();
    }

    @Test
    public void getMySQLCreateTableSql() throws SQLException {
        Connection connection = dataSource.getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        ResultSet tableResultSet = databaseMetaData.getTables(connection.getCatalog(), connection.getSchema(), "tbl_sync", new String[]{"TABLE"});
        TableMeta tableMeta = new TableMeta();
        while (tableResultSet.next()) {
            tableMeta.setTableName(tableResultSet.getString("TABLE_NAME"));
            tableMeta.setTableType(tableResultSet.getString("TABLE_TYPE"));
            tableMeta.setRemarks(tableResultSet.getString("REMARKS"));
        }
        // 获取列元数据
        ResultSet columnResultSet = databaseMetaData.getColumns(connection.getCatalog(), connection.getSchema(), "tbl_sync", null);
        List<ColumnMeta> columnMetas = new ArrayList<>();
        while (columnResultSet.next()) {
            ColumnMeta columnMeta = new ColumnMeta();
            columnMeta.setColumnName(columnResultSet.getString("COLUMN_NAME"));
            columnMeta.setColumnType(columnResultSet.getInt("DATA_TYPE"));
            columnMeta.setColumnTypeName(columnResultSet.getString("TYPE_NAME"));
            columnMeta.setIfAutoIncrement("YES".equalsIgnoreCase(columnResultSet.getString("IS_AUTOINCREMENT")));
            columnMeta.setIfNullable("YES".equalsIgnoreCase(columnResultSet.getString("IS_NULLABLE")) ? 1 : 0);
            columnMeta.setPrecision(columnResultSet.getInt("COLUMN_SIZE"));
            columnMeta.setScale(columnResultSet.getInt("DECIMAL_DIGITS"));
            columnMeta.setDefaultValue(columnResultSet.getString("COLUMN_DEF"));
            columnMeta.setRemarks(columnResultSet.getString("REMARKS"));
            columnMeta.setIfUnsigned(columnMeta.getColumnTypeName().contains("UNSIGNED"));
            columnMetas.add(columnMeta);
        }
        columnResultSet.close();
        // 获取主键元数据
        ResultSet primaryKeyResultSet = databaseMetaData.getPrimaryKeys(connection.getCatalog(), connection.getSchema(), "tbl_sync");
        IndexMeta primaryKeyMeta = new IndexMeta();
        while (primaryKeyResultSet.next()) {
            IndexColumnMeta indexColumnMeta = new IndexColumnMeta(primaryKeyResultSet.getString("COLUMN_NAME"), primaryKeyResultSet.getShort("KEY_SEQ"));
            primaryKeyMeta.setIndexName(primaryKeyResultSet.getString("PK_NAME"));
            primaryKeyMeta.getIndexColumns().add(indexColumnMeta);
        }
        primaryKeyResultSet.close();
        // 获取索引元数据
        ResultSet indexResultSet = databaseMetaData.getIndexInfo(connection.getCatalog(), connection.getSchema(), "tbl_sync", false, false);
        Map<String, IndexMeta> indexMetaMap = new HashMap<>();
        while (indexResultSet.next()) {
            String indexName = indexResultSet.getString("INDEX_NAME");
            if (indexName.equals(primaryKeyMeta.getIndexName())) {
                continue;
            }
            IndexMeta indexMeta = indexMetaMap.get(indexName);
            if (ObjectUtil.isNull(indexMeta)) {
                indexMeta = new IndexMeta(indexName);
                indexMetaMap.put(indexName, indexMeta);
            }
            indexMeta.setIndexType(indexResultSet.getBoolean("NON_UNIQUE") ? IndexTypeEnum.NORMAL : IndexTypeEnum.UNIQUE);
            indexMeta.getIndexColumns().add(new IndexColumnMeta(indexResultSet.getString("COLUMN_NAME"), indexResultSet.getShort("ORDINAL_POSITION")));
        }
        indexResultSet.close();

        DBSql dbSql = DBSql.instance("mysql");
        TableQueryParam queryParam = TableQueryParam.getInstance(connection.getCatalog(), connection.getSchema(), "tbl_sync",
                "test_db", null,"obj_db");
        String createTableSql = dbSql.getCreateTableSql(queryParam, tableMeta, columnMetas, primaryKeyMeta, indexMetaMap);
        System.out.println(SQLUtils.formatMySql(createTableSql));
    }
}
