package com.lee.exception;

/**
 * @description: 异常
 * @author: 青石路
 * @date: 2024/4/30 15:31
 */
public class DataSourceException extends RuntimeException {

    public DataSourceException(String message) {
        super(message);
    }

    public DataSourceException(Exception e) {
        super(e);
    }
}