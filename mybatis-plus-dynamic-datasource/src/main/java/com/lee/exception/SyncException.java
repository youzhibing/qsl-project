package com.lee.exception;

/**
 * @description: 异常
 * @author: 青石路
 * @date: 2024/4/30 15:31
 */
public class SyncException extends RuntimeException {

    public SyncException(String message) {
        super(message);
    }

    public SyncException(Exception e) {
        super(e);
    }
}