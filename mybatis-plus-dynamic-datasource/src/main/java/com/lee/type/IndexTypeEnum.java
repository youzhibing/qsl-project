package com.lee.type;

/**
 * @description: 索引类型
 * @author: 青石路
 * @date: 2024/4/28 17:41
 */
public enum IndexTypeEnum {

    /**
     * 唯一索引
     */
    UNIQUE,

    /**
     * 普通索引
     */
    NORMAL;
}
