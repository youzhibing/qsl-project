package com.lee.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lee.dto.UserDTO;
import com.lee.entity.TblUser;

import java.util.List;

/**
 * @Description:
 * @Author: 博客园@青石路
 **/
public interface IUserService extends IService<TblUser> {

    List<UserDTO> testBigDecimal();

    /**
     * 查询全部
     * @return
     */
    List<TblUser> listAll();
}
