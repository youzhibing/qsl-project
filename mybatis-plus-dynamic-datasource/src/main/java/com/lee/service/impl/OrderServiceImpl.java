package com.lee.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lee.entity.TblOrder;
import com.lee.mapper.OrderMapper;
import com.lee.service.IOrderService;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @Author: 博客园@青石路
 **/
@Service
@DS("mssql_db")
public class OrderServiceImpl extends ServiceImpl<OrderMapper, TblOrder> implements IOrderService {
}
