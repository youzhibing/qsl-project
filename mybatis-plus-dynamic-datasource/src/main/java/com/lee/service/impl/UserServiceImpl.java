package com.lee.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lee.dto.UserDTO;
import com.lee.entity.TblUser;
import com.lee.mapper.UserMapper;
import com.lee.service.IUserService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description:
 * @Author: 博客园@青石路
 **/
@Service
@DS("mssql_db")
public class UserServiceImpl extends ServiceImpl<UserMapper, TblUser> implements IUserService {

    @Override
    public List<TblUser> listAll() {
        System.out.println("你好呀");
        return this.list();
    }

    @Override
    public List<UserDTO> testBigDecimal() {
        BigDecimal perPrice = new BigDecimal("0.07");
        int scale = perPrice.scale();
        BigDecimal maxPrice = new BigDecimal("250");
        return this.baseMapper.testBigDecimal(perPrice, maxPrice, 202303, "zygj");
    }
}
