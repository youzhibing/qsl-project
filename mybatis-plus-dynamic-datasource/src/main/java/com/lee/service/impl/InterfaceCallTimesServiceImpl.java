package com.lee.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lee.entity.TblInterfaceCallTimes;
import com.lee.mapper.InterfaceCallTimesMapper;
import com.lee.service.InterfaceCallTimesService;
import com.lee.vo.InterfaceCallSummaryVO;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/4/27 17:24
 **/
@Service
@DS("mssql_db")
public class InterfaceCallTimesServiceImpl extends ServiceImpl<InterfaceCallTimesMapper, TblInterfaceCallTimes>
        implements InterfaceCallTimesService {

    @Override
    public List<InterfaceCallSummaryVO> summary(Integer startMonth, Integer endMonth) {
        return this.baseMapper.summary(new BigDecimal("25"), new BigDecimal("0.03"), startMonth, endMonth);
    }
}
