package com.lee.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lee.entity.TblOrder;

/**
 * @Description:
 * @Author: 博客园@青石路
 **/
public interface IOrderService extends IService<TblOrder> {
}
