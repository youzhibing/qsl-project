package com.lee.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lee.entity.TblInterfaceCallTimes;
import com.lee.vo.InterfaceCallSummaryVO;

import java.util.List;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/4/27 17:23
 **/
public interface InterfaceCallTimesService extends IService<TblInterfaceCallTimes> {

    List<InterfaceCallSummaryVO> summary(Integer startMonth, Integer endMonth);
}
