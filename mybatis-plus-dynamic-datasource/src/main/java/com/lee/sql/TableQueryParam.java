package com.lee.sql;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @description: 表查询参数
 * @author: youzhibing
 * @date: 2024/5/6 10:21
 */
@Getter
@Setter
@Accessors(chain = true)
public class TableQueryParam {

    private String catalog;
    private String schema;
    private String tableName;

    private String targetCatalog;

    /**
     * 目标库schema
     */
    private String targetSchema;

    /**
     * 目标表名字
     */
    private String targetTableName;

    public static TableQueryParam getInstance(String catalog, String schema, String tableName, String targetSchema,
        String targetTableName) {
        return new TableQueryParam().setCatalog(catalog).setSchema(schema).setTableName(tableName)
            .setTargetSchema(targetSchema).setTargetTableName(targetTableName);
    }

    public static TableQueryParam getInstance(String catalog, String schema, String tableName, String targetCatalog, String targetSchema,
                                              String targetTableName) {
        return new TableQueryParam().setCatalog(catalog).setSchema(schema).setTableName(tableName)
                .setTargetCatalog(targetCatalog).setTargetSchema(targetSchema).setTargetTableName(targetTableName);
    }
}
