package com.lee.sql;

import com.lee.exception.DataSourceException;
import com.lee.meta.ColumnMeta;
import com.lee.meta.IndexMeta;
import com.lee.meta.TableMeta;
import com.lee.type.ProductTypeEnum;

import java.util.List;
import java.util.Map;

/**
 * @description: 数据库sql
 * @author: 青石路
 * @date: 2024/4/26 14:26
 */
public interface DBSql {

    static DBSql instance(String dbType) {
        ProductTypeEnum typeEnum = ProductTypeEnum.of(dbType);
        switch (typeEnum) {
            case MYSQL:
                return MysqlSql.getInstance();
            case ORACLE:
                return OracleSql.getInstance();
            case SQLSERVER:
                return SqlServerSql.getInstance();
            default:
                break;
        }

        throw new DataSourceException("不支持的数据库类型");
    }

    /**
     * 获取创建表sql
     * @param queryParam 查询参数
     * @return sql
     */
    String getTableDDLSql(TableQueryParam queryParam);

    /**
     * 获取查询表字段sql
     * @param queryParam 查询参数
     * @return sql
     */
    String getTableColumnSql(TableQueryParam queryParam);

    /**
     * 获取查询列最大值sql
     * @param queryParam 查询参数
     * @return sql
     */
    String getMaxValueSql(TableQueryParam queryParam, String columnName);

    /**
     * 获取创建表sql
     * @param queryParam 查询参数
     * @param tableMeta 表元素据
     * @param columnMetas 列元数据集合
     * @param primaryKeyMeta 主键元数据
     * @param indexMetaMap 索引元数据集合
     * @return 建表 SQL
     */
    String getCreateTableSql(TableQueryParam queryParam, TableMeta tableMeta, List<ColumnMeta> columnMetas, IndexMeta primaryKeyMeta, Map<String, IndexMeta> indexMetaMap);
}