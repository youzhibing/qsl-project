package com.lee.controller;

import com.lee.dto.UserDTO;
import com.lee.entity.TblUser;
import com.lee.service.IUserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Author: 博客园@青石路
 **/
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private IUserService userService;

    @GetMapping("/list")
    public List<TblUser> list() {
        return userService.listAll();
    }

    @GetMapping("/testBigDecimal")
    public List<UserDTO> testBigDecimal() {
        return userService.testBigDecimal();
    }
}
