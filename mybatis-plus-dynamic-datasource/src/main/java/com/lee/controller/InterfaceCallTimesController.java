package com.lee.controller;

import com.lee.entity.TblInterfaceCallTimes;
import com.lee.service.InterfaceCallTimesService;
import com.lee.vo.InterfaceCallSummaryVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/4/27 17:22
 **/
@RestController
@RequestMapping("/interface")
public class InterfaceCallTimesController {

    @Resource
    private InterfaceCallTimesService interfaceCallTimesService;

    @GetMapping("/list")
    public List<TblInterfaceCallTimes> list() {
        return interfaceCallTimesService.list();
    }

    @GetMapping("/summary")
    public List<InterfaceCallSummaryVO> summary(@RequestParam("startMonth") Integer startMonth,
                                                @RequestParam("endMonth") Integer endMonth) {
        return interfaceCallTimesService.summary(startMonth, endMonth);
    }
}
