package com.lee.controller;

import com.lee.entity.TblOrder;
import com.lee.service.IOrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:
 * @Author: 博客园@青石路
 **/
@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private IOrderService orderService;

    @GetMapping("/list")
    public List<TblOrder> list() {
        List<TblOrder> list = orderService.list();
        return list;
    }
}