package com.lee.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lee.entity.TblOrder;

/**
 * @Description:
 * @Author: 博客园@青石路
 **/
public interface OrderMapper extends BaseMapper<TblOrder> {
}
