package com.lee.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lee.entity.TblInterfaceCallTimes;
import com.lee.vo.InterfaceCallSummaryVO;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/4/27 17:19
 **/
public interface InterfaceCallTimesMapper extends BaseMapper<TblInterfaceCallTimes> {

    /**
     * 汇总
     * @param upperLimit 每个接口每月上限
     * @param perCall   接口每次调用付费
     * @param startMonth 起始月份
     * @param endMonth 截止月份
     * @return
     */
    List<InterfaceCallSummaryVO> summary(@Param("upperLimit") BigDecimal upperLimit,
                                         @Param("perCall") BigDecimal perCall,
                                         @Param("startMonth") Integer startMonth,
                                         @Param("endMonth") Integer endMonth);
}
