package com.lee.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lee.dto.UserDTO;
import com.lee.entity.TblUser;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description:
 * @Author: 博客园@青石路
 **/
public interface UserMapper extends BaseMapper<TblUser> {

    /**
     * 测试 数据库Decimal 精度
     * @return
     */
    List<UserDTO> testBigDecimal(@Param("perPrice") BigDecimal perPrice,
                                 @Param("maxPrice") BigDecimal maxPrice,
                                 @Param("reportDate") Integer reportDate,
                                 @Param("orgName") String orgName);
}
