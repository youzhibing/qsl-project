package com.lee.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2023/4/23 19:40
 */
@Data
public class UserDTO {



    private String orgName;

    private BigDecimal totalFee;
}
