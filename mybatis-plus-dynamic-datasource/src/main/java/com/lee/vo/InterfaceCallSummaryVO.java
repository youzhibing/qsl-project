package com.lee.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/4/27 17:32
 **/
@Data
public class InterfaceCallSummaryVO {

    private Integer callMonth;

    private BigDecimal monthFee;
}
