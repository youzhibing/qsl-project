package com.lee.meta;

import com.lee.type.IndexTypeEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: 索引元信息
 * @author: 青石路
 * @date: 2024/4/28 17:28
 */
public class IndexMeta {

    /**
     * 索引名
     */
    private String indexName;

    /**
     * 索引类型
     */
    private IndexTypeEnum indexType;

    /**
     * 索引列集合
     */
    private List<IndexColumnMeta> indexColumns;

    public IndexMeta() {
        this.indexColumns = new ArrayList<>();
    }

    public IndexMeta(String indexName) {
        this.indexName = indexName;
        this.indexColumns = new ArrayList<>();
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public IndexTypeEnum getIndexType() {
        return indexType;
    }

    public void setIndexType(IndexTypeEnum indexType) {
        this.indexType = indexType;
    }

    public List<IndexColumnMeta> getIndexColumns() {
        return indexColumns;
    }

    public void setIndexColumns(List<IndexColumnMeta> indexColumns) {
        this.indexColumns = indexColumns;
    }
}
