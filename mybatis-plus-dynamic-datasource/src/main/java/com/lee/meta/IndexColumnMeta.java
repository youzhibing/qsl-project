package com.lee.meta;

/**
 * @description: 索引列信息
 * @author: 青石路
 * @date: 2024/4/28 15:50
 */
public class IndexColumnMeta {

    /**
     * 列名
     */
    private String columnName;

    /**
     * 列在索引中的位置序号，从 1 开始
     */
    private Short ordinalPosition;

    public IndexColumnMeta() {
    }

    public IndexColumnMeta(String columnName, Short ordinalPosition) {
        this.columnName = columnName;
        this.ordinalPosition = ordinalPosition;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Short getOrdinalPosition() {
        return ordinalPosition;
    }

    public void setOrdinalPosition(Short ordinalPosition) {
        this.ordinalPosition = ordinalPosition;
    }
}
