package com.lee.meta;

import java.sql.ResultSetMetaData;

/**
 * @description: column 元信息
 * CREATE TABLE `tbl_user` (
 * 	`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增主键',
 * 	`username` VARCHAR ( 100 ) NOT NULL COMMENT '姓名',
 * 	`info` text COMMENT '简介',
 * 	`salary` DECIMAL ( 15, 4 ) NOT NULL DEFAULT '5000.0000' COMMENT '薪水',
 * 	`pic` BLOB COMMENT '图片',
 *   PRIMARY KEY ( `id` )
 * );
 *
 * ColumnName = id
 * ColumnType = -5
 * ColumnTypeName = BIGINT UNSIGNED
 * isAutoIncrement = YES
 * isNullable = NO
 * Precision = 20
 * Scale = 0
 * DefaultValue = null
 * Remarks = 自增主键
 * ===============================
 * ColumnName = username
 * ColumnType = 12
 * ColumnTypeName = VARCHAR
 * isAutoIncrement = NO
 * isNullable = NO
 * Precision = 100
 * Scale = 0
 * DefaultValue = null
 * Remarks = 姓名
 * ===============================
 * ColumnName = info
 * ColumnType = -1
 * ColumnTypeName = TEXT
 * isAutoIncrement = NO
 * isNullable = YES
 * Precision = 65535
 * Scale = 0
 * DefaultValue = null
 * Remarks = 简介
 * ===============================
 * ColumnName = salary
 * ColumnType = 3
 * ColumnTypeName = DECIMAL
 * isAutoIncrement = NO
 * isNullable = NO
 * Precision = 15
 * Scale = 4
 * DefaultValue = 5000.0000
 * Remarks = 薪水
 * ===============================
 * ColumnName = pic
 * ColumnType = -4
 * ColumnTypeName = BLOB
 * isAutoIncrement = NO
 * isNullable = YES
 * Precision = 65535
 * Scale = 0
 * DefaultValue = null
 * Remarks = 图片
 * ===============================
 *
 * @author: 青石路
 * @date: 2024/4/26 10:01
 */
public class ColumnMeta {

    /**
     * 列名
     * {@link ResultSetMetaData#getColumnName}
     */
    private String columnName;

    /**
     * 列类型 {@link java.sql.Types}
     * {@link ResultSetMetaData#getColumnType}
     */
    private int columnType;

    /**
     * 列类型名称 {@link java.sql.SQLType#getName()，数据库列类型名称，如：VARCHAR、TEXT、BLOB，依赖于具体的数据库Driver}
     * {@link ResultSetMetaData#getColumnTypeName}
     */
    private String columnTypeName;

    /**
     * 是否自增
     * {@link ResultSetMetaData#isAutoIncrement}
     */
    private boolean ifAutoIncrement;

    /**
     * 是否允许null，0：不允许，1：允许，2：不知道
     * {@link ResultSetMetaData#isNullable}
     */
    private int ifNullable;

    /**
     * 是否无符号
     */
    private boolean ifUnsigned;

    /**
     * 列大小
     *  For numeric data, this is the maximum precision => 对于数值数据，表示最大精度
     *  For character data, this is the length in characters => 对于字符数据，表示字符长度
     *  For datetime datatypes, this is the length in characters of the String representation(assuming the maximum allowed precision of the fractional seconds component )
     *      => 对于日期时间数据类型，表示字符串表示形式的最大长度（假设最大允许的分秒小数部分的精度）
     *      例如："2024-04-30 14:00:00" => 19，"2024-04-30 14:00:00.234" => 23
     *      "14:00:00" => 8，"14:00:00.234" => 11
     *  For binary data, this is the length in bytes => 对于二进制数据，表示字节长度
     *  For the ROWID datatype, this is the length in bytes => 对于 ROWID 类型，表示字节长度
     *  0 is returned for data types where the column size is not applicable => 对于列大小不适用的数据类型，返回0
     * {@link ResultSetMetaData#getPrecision}
     */
    private int precision;

    /**
     * 小数位最大位数
     * {@link ResultSetMetaData#getScale}
     */
    private int scale;

    /**
     * 列默认值
     */
    private String defaultValue;

    /**
     * 列注释
     */
    private String remarks;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public int getColumnType() {
        return columnType;
    }

    public void setColumnType(int columnType) {
        this.columnType = columnType;
    }

    public String getColumnTypeName() {
        return columnTypeName;
    }

    public void setColumnTypeName(String columnTypeName) {
        this.columnTypeName = columnTypeName;
    }

    public boolean getIfAutoIncrement() {
        return ifAutoIncrement;
    }

    public void setIfAutoIncrement(boolean ifAutoIncrement) {
        this.ifAutoIncrement = ifAutoIncrement;
    }

    public int getIfNullable() {
        return ifNullable;
    }

    public void setIfNullable(int ifNullable) {
        this.ifNullable = ifNullable;
    }

    public boolean getIfUnsigned() {
        return ifUnsigned;
    }

    public void setIfUnsigned(boolean ifUnsigned) {
        this.ifUnsigned = ifUnsigned;
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    public int getScale() {
        return scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
