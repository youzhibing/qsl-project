package com.lee.meta;

/**
 * @description: table 元信息
 * CREATE TABLE `tbl_user` (
 * 	`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增主键',
 * 	`username` VARCHAR ( 100 ) NOT NULL COMMENT '姓名',
 * 	`info` text COMMENT '简介',
 * 	`salary` DECIMAL ( 15, 4 ) NOT NULL DEFAULT '5000.0000' COMMENT '薪水',
 * 	`pic` BLOB COMMENT '图片',
 *   PRIMARY KEY ( `id` )
 * )
 *
 * TableName = tbl_user
 * TableType = TABLE
 * Remarks = 用户
 * {@link java.sql.DatabaseMetaData#getTables}
 * @author: 青石路
 * @date: 2024/4/28 14:42
 */
public class TableMeta {

    /**
     * 表名
     */
    private String tableName;
    /**
     * 表类型
     * Typical types are "TABLE", "VIEW", "SYSTEM TABLE",
     *      "GLOBAL TEMPORARY","LOCAL TEMPORARY", "ALIAS", "SYNONYM"
     */
    private String tableType;

    /**
     * 表注释
     */
    private String remarks;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}