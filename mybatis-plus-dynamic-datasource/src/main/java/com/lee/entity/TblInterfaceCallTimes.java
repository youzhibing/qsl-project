package com.lee.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/4/27 17:12
 **/
@Data
@TableName("tbl_interface_call_times")
public class TblInterfaceCallTimes {

    @TableId(value = "ID")
    private Long id;

    private Integer callMonth;

    @TableField("interface")
    private String _interface;

    private Integer times;
}
