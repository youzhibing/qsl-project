package com.qsl;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/11/2 17:39
 **/
public class DateTimeTest {

    public static void main(String[] args) {
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        String time = timeFormatter.format(LocalDateTime.now());
        System.out.println(time);

        LocalDate today = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate parse = LocalDate.parse("2023-08-20", formatter);
        System.out.println("parse = " + parse);

        Timestamp fileModifyTime = new Timestamp(System.currentTimeMillis());
        String fileRealModify = String.valueOf(fileModifyTime.getTime() / 1000 * 1000);
        System.out.println(fileRealModify);

        System.out.println(LocalDate.now().minusYears(25));

    }
}
