package com.qsl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/10/19 15:07
 **/
public class CollectionTest {

    public static void main(String[] args) {
        List<String> rsoRmaList = new ArrayList<>();
        List<String> collect = rsoRmaList.stream().map(e -> e + "123").distinct().collect(Collectors.toList());
        System.out.println(collect);
        List list = Arrays.asList("123", "234");
        System.out.println(String.valueOf(list));
        Long id = null;
        System.out.println(new StringBuilder("hello").append(id));

        List<User> users = new ArrayList<>();
        users.add(new User("国内APP", 11));
        users.add(new User("国内PC-企业", 101));
        users.add(new User("海外用户", 15));
        users.add(new User("国内PC-个人", 50));
        System.out.println(users);
        users.sort(Comparator.comparing(User::getName));
        System.out.println(users);
    }

    static class User {
        private String name;
        private int age;

        public User(){}

        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}
