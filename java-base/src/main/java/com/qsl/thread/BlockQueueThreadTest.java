package com.qsl.thread;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 *      生产者与消费者，线程停止测试
 * @author: 青石路
 * @date: 2024/3/29 16:43
 */
public class BlockQueueThreadTest {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        System.out.println(list instanceof List);
    }
}
