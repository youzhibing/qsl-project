package com.qsl;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/9/27 16:31
 **/
public class StringTest {

    private static String hexString="0123456789ABCDEF";
    private static final String FUZZY_MATCH_FLAG = "*";

    public static void main(String[] args) throws Exception {
//        String str = "中行关联方股票调出_{yyyyMMdd}.xlsx";
//        System.out.println(str.length());
//
//        String obj = "123.{compressType}.{yyyyMMdd}";
//        System.out.println(obj.replaceAll("\\{compressType}", ".tar.gz".substring(1)));
//
//        String format = String.format("jdbc:mysql:%s:%s:%s?useSSL=false&useUnicode=true&characterEncoding=utf-8&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Asia/Shanghai", "127.0.0.1", "3306", "task_config");
//        System.out.println(format);
//
//        String fileName = "=?UTF-8?Q?=E4=B8=AD=E8=A1=8C=E5=85=B3=E8=81=94=E6=96=B9?=\r\n =?UTF-8?Q?=E8=82=A1=E7=A5=A8=E8=B0=83=E5=87=BA=5F2?=\r\n =?UTF-8?Q?0221215=2Exlsx?=";
//        System.out.println(fileName);
//        System.out.println(MimeUtility.decodeWord(fileName));
//
//        System.out.println("EN_STOCKPO".length());
//
//        System.out.println("ab，cd，，".replace("，", ","));
//        char[] chars = "/data/files/tableStandard/20230526/规则23-XGRQ字段类型必须为datetime且不可为空-XGRQ字段类型为非datetime.xls".toCharArray();
//        System.out.println(chars.length);
        System.out.println(unicodeToString("\\u0a"));
        System.out.println(System.getProperty("sun.jnu.encoding"));
        System.out.println(System.getProperty("file.encoding"));
        // 外部文件名是 GBK
        // 代码是 UTF-8

//        System.out.println(System.getProperty("file.encoding"));
//        System.out.println(Charset.defaultCharset());
//        System.out.println(System.getProperty("user.language"));
//        System.out.println(System.getProperty("sun.jnu.encoding"));
//        String name = "境外债券估值20240104";
//        System.out.println(name);
//        String fileName = new String(name.getBytes("UTF-8"), "GBK");
//        System.out.println(fileName);
////        System.out.println(changeCharset(fileName, "ASCII"));
//
////        Path path = Paths.get("d:/hello/world/1231231.txt");
////        Path fileName1 = path.getFileName();
////        System.out.println(fileName1);
//        String fileRealName = midFuzzyMatch("D:\\test", "Bank_Info_*.txt");
//        System.out.println(fileRealName);
    }

    public static String changeCharset(String str, String newCharset) throws UnsupportedEncodingException {
        if(str != null) {
            //用默认字符编码解码字符串。与系统相关，中文windows默认为GB2312
            byte[] bs = str.getBytes();
            return new String(bs, newCharset);    //用新的字符编码生成字符串
        }
        return null;
    }

    public static String unicodeToString(String unicode) {
        if (unicode == null) return null;
        StringBuilder str = new StringBuilder();
        String[] hexs = unicode.split("\\\\u");
        for (String hex : hexs) {
            if (hex != null && hex.length() > 0) {
                str.append((char) Integer.parseInt(hex, 16));
            }
        }
        return str.toString();
    }

    private static String midFuzzyMatch(String realFilePath, String originalFileName) {
        if (!originalFileName.contains("*")) {
            // 不用模糊匹配，直接返回
            return originalFileName;
        }
        Path path = Paths.get(realFilePath);
        // fileNameArr[0]: 文件名 Bank_Info_20240110
        // fileNameArr[1]:文件后缀 .txt
        String[] fileNameArr = originalFileName.split("\\*");
        if (fileNameArr.length < 2) {
            // 只支持中间模糊匹配
            return originalFileName;
        }
        System.out.println("外部文件生成，[" + originalFileName + "]需要进行文件名模糊匹配");
        String realFileName = originalFileName;
        try (Stream<Path> filePathStream = Files.list(path)) {
            Optional<File> any = filePathStream.filter(filePath -> {
                String fileName = filePath.getFileName().toString();

                return fileName.startsWith(fileNameArr[0]) && fileName.endsWith(fileNameArr[1]);
            }).map(Path::toFile).max(Comparator.comparing(File::lastModified));
            if (any.isPresent()) {
                realFileName = any.get().getName();
            }
        } catch (IOException e) {
             e.printStackTrace();
        }
        System.out.println("外部文件生成");
        return realFileName;
    }
}
