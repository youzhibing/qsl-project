package com.qsl;

import java.math.BigDecimal;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/3/10 13:31
 **/
public class ByteArrayTest {

    public static void main(String[] args) {
        writeBigDecimal(new BigDecimal("4577.265800010001"));
    }

    private static void writeBigDecimal(BigDecimal value) {
        String s = value.toPlainString();
        byte[] bytes = s.getBytes();
        System.out.println(new String(bytes));
        if (bytes.length > 13) {
            byte[] newBytes = new byte[13];
            System.arraycopy(bytes, 0, newBytes, 0, 13);
            bytes = newBytes;
        }
        System.out.println(new String(bytes));
    }
}
