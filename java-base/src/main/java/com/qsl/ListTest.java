package com.qsl;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: 青石路
 * @createDate: 2023/8/18 13:48
 **/
public class ListTest {

    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();
        stringList.add("lisi");
        stringList.add("张三");
        stringList.add("zhangsan");
        stringList.add("李四");
        for (int i=0; i<stringList.size(); i++) {
            if (stringList.get(i).equals("zhangsan")) {
                stringList.set(i, "666");
                break;
            }
        }
        System.out.println(stringList);
        System.out.println(Integer.MAX_VALUE);

        List<String> stringList1 = stringList.subList(2, 10);
        System.out.println(stringList1.size());
    }
}
