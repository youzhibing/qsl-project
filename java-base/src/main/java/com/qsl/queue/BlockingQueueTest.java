package com.qsl.queue;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2024/3/28 15:23
 */
public class BlockingQueueTest {

    /**
     * add 和 remove
     * put 和 take
     * offer 和 poll
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        ArrayBlockingQueue<String> arrayBlockingQueue = new ArrayBlockingQueue<>(3);
        arrayBlockingQueue.put("1");
        arrayBlockingQueue.put("2");
        arrayBlockingQueue.put("3");
        arrayBlockingQueue.put("a");
        System.out.println(arrayBlockingQueue);
    }
}
