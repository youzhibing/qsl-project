package com.qsl.datax;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DataxHook {

    private static final String SYSTEM_ENCODING = System.getProperty("sun.jnu.encoding");
    private static final String DATAX_COMMAND = "java -server -Xms1g -Xmx1g -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=G:\\datax-tool\\datax\\log -Dfile.encoding=GBK -Dlogback.statusListenerClass=ch.qos.logback.core.status.NopStatusListener -Djava.security.egd=file:///dev/urandom -Ddatax.home=G:\\datax-tool\\datax -Dlogback.configurationFile=G:\\datax-tool\\datax\\conf\\logback.xml -classpath G:\\datax-tool\\datax\\lib\\* com.alibaba.datax.core.Engine -mode standalone -jobid -1 -job G:\\datax-tool\\datax\\job\\job.json";

    public static void main(String[] args) {
        try {
            Process process = Runtime.getRuntime().exec(DATAX_COMMAND);

            // 另启线程读取
            new Thread(() -> {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), SYSTEM_ENCODING))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        System.out.println(line);
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }).start();

            new Thread(() -> {
                try (BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream(), SYSTEM_ENCODING))) {
                    String line;
                    while ((line = errorReader.readLine()) != null) {
                        System.out.println(line);
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }).start();

            // 等待命令执行完成
            int i = process.waitFor();
            if (i == 0) {
                System.out.println("job执行完成");
            } else {
                System.out.println("job执行失败");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}