package com.qsl;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/1/12 10:15
 **/
public class recurVarTest {

    public static void main(String[] args) {
        recur(0);
    }

    public static void recur(int retryCount) {
        System.out.println("retryCount = " + retryCount);
        if (retryCount > 5) {
            System.out.println("递归大于5次，跳出...");
            throw new RuntimeException("递归大于5次");
        }
        recur(++retryCount);
    }
}
