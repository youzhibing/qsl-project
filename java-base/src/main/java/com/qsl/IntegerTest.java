package com.qsl;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/3/28 10:26
 **/
public class IntegerTest {

    public static void main(String[] args) {
        System.out.println(202301 / 100 * 100 + 1);
        System.out.println(0 % 12);
        System.out.println(11 % 12);
        System.out.println(12 % 12);
        System.out.println(15 % 12);
        System.out.println(202311 % 100);
    }
}
