package com.qsl;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/9/27 10:15
 **/
public class SafePointTest {

    public static AtomicInteger num = new AtomicInteger(0);

    public static void main(String[] args) throws Exception {
        Runnable runnable = () -> {
            for (int i=0; i<200000000; i++) {
                num.getAndAdd(1);
            }
        };
        Thread t1 = new Thread(runnable, "青石路一号");
        Thread t2 = new Thread(runnable, "青石路二号");
        t1.start();
        t2.start();
        System.out.println("main线程开始睡觉......");
        TimeUnit.SECONDS.sleep(1);
        System.out.println("main线程醒了......");
        System.out.println("main线程打印 num = " + num);
    }
}
