package com.qsl.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

/**
 * @author: 青石路
 */
@Component
@ConditionalOnProperty(name = "spring.kafka.first.enabled", havingValue = "true")
public class KafkaConsumer {

    private static final Logger log = LoggerFactory.getLogger(KafkaConsumer.class);

    @KafkaListener(topics = "tp_qsl_order_cancel", groupId = "gid_qsl_order_cancel", containerFactory = "firstKafkaListenerContainerFactory")
    public void listenOrder(String message, Acknowledgment acknowledgment) {
        try {
            // 业务处理
            log.info("收到kafka message: {}", message);
        } finally {
            acknowledgment.acknowledge();
        }
    }
}
