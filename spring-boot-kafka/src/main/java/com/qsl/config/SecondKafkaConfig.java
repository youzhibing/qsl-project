package com.qsl.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;


/**
 * 第二个Kafka配置
 * @author: 青石路
 */
@Configuration
@ConditionalOnProperty(name = "spring.kafka.second.enabled", havingValue = "true")
public class SecondKafkaConfig {

    @ConfigurationProperties(prefix = "spring.kafka.second")
    @Bean("secondKafkaProperties")
    public KafkaProperties secondKafkaProperties() {
        return new KafkaProperties();
    }

    @Bean("secondKafkaTemplate")
    public KafkaTemplate<String, String> secondKafkaTemplate() {
        return new KafkaTemplate<>(secondProducerFactory());
    }

    @Bean("secondKafkaListenerContainerFactory")
    public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> fisrtKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(secondConsumerFactory());
        return factory;
    }

    @Bean("secondConsumerFactory")
    public ConsumerFactory<String, String> secondConsumerFactory() {
        return new DefaultKafkaConsumerFactory<>(secondKafkaProperties().buildConsumerProperties());
    }

    @Bean("secondProducerFactory")
    public DefaultKafkaProducerFactory<String, String> secondProducerFactory() {
        return new DefaultKafkaProducerFactory<>(secondKafkaProperties().buildProducerProperties());
    }
}