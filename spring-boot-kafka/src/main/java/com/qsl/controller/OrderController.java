package com.qsl.controller;

import com.qsl.service.KafkaSender;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: 青石路
 */
@RestController
@RequestMapping("order")
public class OrderController {

    @Resource
    private KafkaSender kafkaSender;

    @GetMapping("add")
    public String add(String orderInfo) {
        // TODO 订单业务处理
        // 下发消息到库存
        kafkaSender.send("tp_qsl_inventory_order_add", orderInfo);
        return "下单成功";
    }
}
