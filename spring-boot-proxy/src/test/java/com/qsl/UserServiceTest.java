package com.qsl;

import com.qsl.entity.User;
import com.qsl.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2024/11/27 10:21
 */
@SpringBootTest
public class UserServiceTest {

    @Resource
    private UserService userService;

    @Test
    public void testProxy() {
        userService.save(new User("qsl", "123456"));
    }
}
