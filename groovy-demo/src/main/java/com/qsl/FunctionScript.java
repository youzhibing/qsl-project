package com.qsl;

import com.alibaba.fastjson.JSON;
import groovy.lang.Script;
import org.apache.groovy.parser.antlr4.util.StringUtils;

import java.util.Map;

public class FunctionScript extends Script {

    @Override
    public Object run() {
        return null;
    }

    /**
     * 两个字符串拼接
     * @param first 第一个字符串
     * @param second 第二个字符串
     * @return 拼接后的字符串
     */
    public String stringConcat(String first, String second) {
        if (StringUtils.isEmpty(first)
                || StringUtils.isEmpty(second)) {
            return "";
        }
        return first.concat(second);
    }

    /**
     * 运算符：等于
     * @param o1 等于号左侧值
     * @param o2 等于号右侧值
     * @return 比较结果
     * @throws Exception
     */
    public boolean isEqual(Object o1, Object o2) {
        if (o1 == null || o2 == null) {
            return false;
        }
        String s1 = o1.toString();
        String s2 = o2.toString();
        return s1.equals(s2);
    }

    /**
     * python函数执行器
     *      处理自定义函数的模板方法
     * @param exprJson 函数体与参数的json字符串
     * @return 执行结果
     */
    public Object pyFuncExec(String exprJson) {
        // 将参数转换成java对象
        PythonFuncModel model = JSON.parseObject(exprJson, PythonFuncModel.class);
        Map paramMap = this.getBinding().getVariables();
        return JythonExecutor.scriptExecute(model, paramMap);
    }
}