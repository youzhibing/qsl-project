package com.qsl;

import groovy.lang.Binding;
import groovy.lang.GroovyCodeSource;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import org.codehaus.groovy.control.CompilationFailedException;
import org.codehaus.groovy.control.CompilerConfiguration;

public class ExpressionShell extends GroovyShell {

    ExpressionShell(CompilerConfiguration config) {
        super(new Binding(), config);
    }

    @Override
    public Script parse(GroovyCodeSource codeSource)
            throws CompilationFailedException {
        try {
            Class<?> scriptClass = getClassLoader()
                    .parseClass(codeSource, false);
            FunctionScript script =
                    (FunctionScript) scriptClass.newInstance();
            script.setBinding(getContext());
            return script;
        } catch (Exception e) {
            throw new RuntimeException("groovy 实例异常" , e);
        } finally {
            resetLoadedClasses();
            getClassLoader().clearCache();
        }
    }
}