package com.qsl;

import java.util.List;

/**
 * python函数表达式
 * @author: 青石路
 */
public class PythonFuncModel {
    /**
     * 函数体（函数脚本）
     */
    private String script;

    /**
     * 返回值类型
     */
    private Integer returnType;

    /**
     * 函数参数(从左至右传参)
     */
    private List<FuncParam> params;

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public Integer getReturnType() {
        return returnType;
    }

    public void setReturnType(Integer returnType) {
        this.returnType = returnType;
    }

    public List<FuncParam> getParams() {
        return params;
    }

    public void setParams(List<FuncParam> params) {
        this.params = params;
    }

    public static class FuncParam {

        /**
         * 参数名称
         */
        private String name;

        /**
         * 参数类型
         */
        private Integer type;

        /**
         * 规则参数类型，0：常量 1：变量
         */
        private Integer ruleAttrType = 0;

        /**
         * 参数值
         */
        private Object value;

        public FuncParam() {
        }

        public FuncParam(String name, Integer type, Object value) {
            this(name, type, value, 0);
        }

        public FuncParam(String name, Integer type, Object value,
                         Integer ruleAttrType) {
            this.name = name;
            this.type = type;
            this.value = value;
            this.ruleAttrType = ruleAttrType;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public Integer getRuleAttrType() {
            return ruleAttrType;
        }

        public void setRuleAttrType(Integer ruleAttrType) {
            this.ruleAttrType = ruleAttrType;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }
    }
}
