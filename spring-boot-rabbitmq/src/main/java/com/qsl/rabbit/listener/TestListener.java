package com.qsl.rabbit.listener;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

import static com.qsl.rabbit.constant.Constant.QSL_QUEUE;

/**
 * @author: 青石路
 */
@Component
@Slf4j
public class TestListener implements ChannelAwareMessageListener {

    @RabbitListener(queues = QSL_QUEUE)
    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        String s = new String(message.getBody(), StandardCharsets.UTF_8);
        log.info("消费者接收到消息：{}", s);
        try {
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            log.error("消息确认异常：", e);
        }
    }
}
