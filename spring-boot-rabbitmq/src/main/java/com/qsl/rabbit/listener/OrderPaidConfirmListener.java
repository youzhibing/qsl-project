package com.qsl.rabbit.listener;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

import static com.qsl.rabbit.constant.Constant.ORDER_DLX_QUEUE;

/**
 * @description: 订单已支付确认描述
 * @author: 青石路
 * @date: 2024/6/1 13:20
 */
@Component
@Slf4j
public class OrderPaidConfirmListener implements ChannelAwareMessageListener {

    @RabbitListener(queues = ORDER_DLX_QUEUE)
    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        String msg = new String(message.getBody(), StandardCharsets.UTF_8);
        log.info("收到订单信息：{}", msg);
        log.info("查询订单信息，确认订单是否已支付");
        log.info("若订单未支付，则回滚库存数据以及其他处理");
        // 消息 ack
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
