package com.qsl.rabbit.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

import static com.qsl.rabbit.constant.Constant.ORDER_DLX_EXCHANGE;
import static com.qsl.rabbit.constant.Constant.ORDER_DLX_QUEUE;
import static com.qsl.rabbit.constant.Constant.ORDER_EXCHANGE;
import static com.qsl.rabbit.constant.Constant.ORDER_QUEUE;
import static com.qsl.rabbit.constant.Constant.ORDER_ROUTING_KEY;

/**
 * @author: 青石路
 */
@Configuration
public class OrderRabbitConfig {

    @Bean
    public DirectExchange orderExchange() {
        return new DirectExchange(ORDER_EXCHANGE, true, false);
    }

    @Bean
    public Queue orderQueue() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-message-ttl", 60000);
        args.put("x-dead-letter-exchange", ORDER_DLX_EXCHANGE);
        return new Queue(ORDER_QUEUE, true, false, false, args);
    }

    @Bean
    public Binding bindOrderQueue() {
        return BindingBuilder.bind(orderQueue()).to(orderExchange()).with(ORDER_ROUTING_KEY);
    }

    @Bean
    public DirectExchange orderDlxExchange() {
        return new DirectExchange(ORDER_DLX_EXCHANGE, true, false);
    }

    @Bean
    public Queue orderDlxQueue() {
        return new Queue(ORDER_DLX_QUEUE, true, false, false);
    }

    @Bean
    public Binding bindOrderDlxQueue() {
        /**
         * 未配置x-dead-letter-routing-key
         * 则路由键与原队列（com.qsl.order.queue）路由键（order_routing_key）一致
         */
        return BindingBuilder.bind(orderDlxQueue()).to(orderDlxExchange()).with(ORDER_ROUTING_KEY);
    }
}
