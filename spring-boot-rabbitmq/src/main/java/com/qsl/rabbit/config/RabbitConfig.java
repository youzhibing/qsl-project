package com.qsl.rabbit.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

import static com.qsl.rabbit.constant.Constant.DLX_EXCHANGE;
import static com.qsl.rabbit.constant.Constant.DLX_QUEUE;
import static com.qsl.rabbit.constant.Constant.MESSAGE_TTL_QUEUE;
import static com.qsl.rabbit.constant.Constant.NORMAL_EXCHANGE;
import static com.qsl.rabbit.constant.Constant.NORMAL_QUEUE;
import static com.qsl.rabbit.constant.Constant.QSL_EXCHANGE;
import static com.qsl.rabbit.constant.Constant.QSL_QUEUE;
import static com.qsl.rabbit.constant.Constant.TTL_QUEUE;

/**
 * @author: 青石路
 **/
@Configuration
public class RabbitConfig {

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(QSL_EXCHANGE, true, false);
    }

    @Bean
    public Queue queue() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-max-priority", 5);
        return new Queue(QSL_QUEUE, true, false, false, args);
    }

    @Bean
    public Binding bindingQueue() {
        return BindingBuilder.bind(queue()).to(directExchange()).with("com.qsl");
    }

    @Bean
    public DirectExchange normalExchange() {
        /*Map<String, Object> args = new HashMap<>();
        args.put("alternate-exchange", ALTERNATE_EXCHANGE);*/
        return new DirectExchange(NORMAL_EXCHANGE, true, false);
    }

    @Bean
    public Queue normalQueue() {
        return new Queue(NORMAL_QUEUE, true, false, false);
    }

    /*@Bean
    public FanoutExchange alternateExchange() {
        return new FanoutExchange(ALTERNATE_EXCHANGE, true, false);
    }

    @Bean
    public Queue unroutedQueue() {
        return new Queue(UNROUTED_QUEUE, true, false, false);
    }*/

    @Bean
    public Binding bindNormal() {
        return BindingBuilder.bind(normalQueue()).to(normalExchange()).with("normalKey");
    }

    /*@Bean
    public Binding bindAlternate() {
        return BindingBuilder.bind(unroutedQueue()).to(alternateExchange());
    }*/

    @Bean
    public Queue messageTtlQueue() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-message-ttl", 3000);
        args.put("x-dead-letter-exchange", DLX_EXCHANGE);
        args.put("x-dead-letter-routing-key", "dlx_routing_key");
        return new Queue(MESSAGE_TTL_QUEUE, true, false, false, args);
    }

    @Bean
    public Binding bindMessageTtlNormal() {
        return BindingBuilder.bind(messageTtlQueue()).to(normalExchange()).with("ttlMessage");
    }

    @Bean
    public Queue ttlQueue() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-expires", 180000);
        return new Queue(TTL_QUEUE, true, false, false, args);
    }

    @Bean
    public Binding bindTtlQueueNormal() {
        return BindingBuilder.bind(ttlQueue()).to(normalExchange()).with("ttlQueue");
    }

    @Bean
    public DirectExchange dlxExchange() {
        return new DirectExchange(DLX_EXCHANGE, true, false);
    }

    @Bean
    public Queue dlxQueue() {
        return new Queue(DLX_QUEUE, true, false, false);
    }

    @Bean
    public Binding bindDlxQueue() {
        /**
         * 这里的路由键需要与x-dead-letter-routing-key的配置值（dlx_routing_key）一致
         * 若未配置x-dead-letter-routing-key，则与原队列（com.qsl.message.ttl.queue）路由键（ttlMessage）一致
         */
        return BindingBuilder.bind(dlxQueue()).to(dlxExchange()).with("dlx_routing_key");
    }
}