package com.qsl.rabbit.constant;

/**

 * @author: 青石路
 **/
public interface Constant {

    String QSL_EXCHANGE = "com.qsl.exchange";
    String QSL_QUEUE= "com.qsl.queue";

    String ALTERNATE_EXCHANGE = "com.qsl.alternate.exchange";
    String NORMAL_EXCHANGE = "com.qsl.normal.exchange";

    String NORMAL_QUEUE = "com.qsl.normal.queue";
    String UNROUTED_QUEUE = "com.qsl.unrouted.queue";

    String MESSAGE_TTL_QUEUE = "com.qsl.message.ttl.queue";

    String TTL_QUEUE = "com.qsl.ttl.queue";

    String DLX_EXCHANGE = "com.qsl.dlx.exchange";
    String DLX_QUEUE = "com.qsl.dlx.queue";

    String ORDER_EXCHANGE = "com.qsl.order.exchange";
    String ORDER_QUEUE = "com.qsl.order.queue";
    String ORDER_DLX_EXCHANGE = "com.qsl.order.dlx.exchange";
    String ORDER_DLX_QUEUE = "com.qsl.order.dlx.queue";
    String ORDER_ROUTING_KEY = "order_routing_key";

    String TASK_EXCHANGE = "com.qsl.task.exchange";
    String TASK_QUEUE = "com.qsl.task.queue";
}
