package com.qsl.rabbit;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;
import com.rabbitmq.client.ReturnListener;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.ChannelCallback;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;

import static com.qsl.rabbit.constant.Constant.NORMAL_EXCHANGE;
import static com.qsl.rabbit.constant.Constant.ORDER_EXCHANGE;
import static com.qsl.rabbit.constant.Constant.ORDER_ROUTING_KEY;
import static com.qsl.rabbit.constant.Constant.QSL_EXCHANGE;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2024/6/1 13:08
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class AdvancedFunctionTest {

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testMandatory() throws IOException {
        rabbitTemplate.execute(new ChannelCallback<Object>() {
            @Override
            public Object doInRabbit(Channel channel) throws Exception {
                channel.basicPublish(QSL_EXCHANGE, "", true,
                        MessageProperties.PERSISTENT_TEXT_PLAIN, "mandatory test".getBytes());
                channel.addReturnListener(new ReturnListener() {
                    @Override
                    public void handleReturn(
                            int replyCode, String replyText, String exchange,
                            String routingKey, AMQP.BasicProperties properties, byte[] body)
                            throws IOException {
                        log.info("Basic.Return 返回结果：{}", new String(body));
                    }
                });
                return null;
            }
        });
    }

    @Test
    public void testImmediate() {
        rabbitTemplate.execute(new ChannelCallback<Object>() {
            @Override
            public Object doInRabbit(Channel channel) throws Exception {
                channel.basicPublish(QSL_EXCHANGE, "", false,
                        MessageProperties.PERSISTENT_TEXT_PLAIN, "mandatory test".getBytes());
                return null;
            }
        });
    }

    @Test
    public void testAlternate() {
        rabbitTemplate.execute(new ChannelCallback<Object>() {
            @Override
            public Object doInRabbit(Channel channel) throws Exception {
                channel.basicPublish(NORMAL_EXCHANGE, "123",
                        MessageProperties.PERSISTENT_TEXT_PLAIN, "alternate test".getBytes());
                return null;
            }
        });
    }

    @Test
    public void testTtl() {
        rabbitTemplate.convertAndSend(NORMAL_EXCHANGE, "ttlMessage",
                "ttl message", message -> {
                    message.getMessageProperties().setExpiration("2000");
                    return message;
                });
    }

    @Test
    public void testTtlQueue() {
        rabbitTemplate.convertAndSend(NORMAL_EXCHANGE, "ttlQueue", "ttl queue");
    }

    @Test
    public void testDlx() {
        rabbitTemplate.convertAndSend(NORMAL_EXCHANGE, "ttlMessage", "dlx test");
    }

    @Test
    public void testOrder() {
        String orderNo = "no20240601133033938";
        rabbitTemplate.convertAndSend(ORDER_EXCHANGE, ORDER_ROUTING_KEY, orderNo);
        log.info("下单完成，订单号[{}]", orderNo);
    }
}
