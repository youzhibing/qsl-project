package com.qsl.rabbit;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.concurrent.ThreadLocalRandom;

import static com.qsl.rabbit.constant.Constant.QSL_EXCHANGE;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2024/1/7 16:17
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class PriorityMessageTest {

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Test
    public void test() {
        for (int i = 1; i < 10; i++) {
            final int priority = ThreadLocalRandom.current().nextInt(1, 10);
            log.info("priority:{}", priority);
            rabbitTemplate.convertAndSend(QSL_EXCHANGE, "com.qsl", String.valueOf(priority), message -> {
                message.getMessageProperties().setPriority(priority);
                return message;
            });
        }
    }
}