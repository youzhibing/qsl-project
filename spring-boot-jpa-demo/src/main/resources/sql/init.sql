CREATE TABLE `tbl_user` (
  `user_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` VARCHAR(100) NOT NULL COMMENT '用户姓名',
  `password` VARCHAR(100) NOT NULL COMMENT '用户密码',
  `birthday` DATE COMMENT '出生日期',
  `last_modified_time` TIMESTAMP COMMENT '最终修改时间'
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB COMMENT='用户';