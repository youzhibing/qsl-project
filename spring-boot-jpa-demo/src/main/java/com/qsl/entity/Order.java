package com.qsl.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @description:
 * @author: 青石路
 * @createDate: 2024/1/17 15:32
 **/
@Data
public class Order {

    private String orderNo;
    private LocalDateTime createdTime;
}
