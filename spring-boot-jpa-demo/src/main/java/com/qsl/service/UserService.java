package com.qsl.service;

import com.qsl.entity.User;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2024/1/17 20:15
 */
public interface UserService {

    User save(User user);
}
