package com.qsl.repository;

import com.qsl.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2024/1/9 21:37
 */
public interface UserRepository extends JpaRepository<User, Long> {
}
