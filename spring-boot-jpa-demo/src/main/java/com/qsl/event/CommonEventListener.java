package com.qsl.event;

import com.qsl.entity.Order;
import com.qsl.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: 青石路
 * @createDate: 2024/1/17 15:29
 **/
@Slf4j
@Component
public class CommonEventListener {

    @EventListener
    public void handleUserEvent(BaseEvent<User> userEvent) {
        log.info("监听到用户注册成功事件：" +
                "{}，你注册成功了哦。记得来玩儿~", userEvent.getData().getUserName());
    }

    @EventListener
    public void handleOrderEvent(BaseEvent<Order> userEvent) {
        log.info("监听到下单事件：" +
                "单号：{} 来了，赶紧处理提成", userEvent.getData().getOrderNo());
    }
}
