package com.qsl.event;

import lombok.Data;
import org.springframework.core.ResolvableType;
import org.springframework.core.ResolvableTypeProvider;

/**
 * @description: https://www.cnblogs.com/thisiswhy/p/17965123
 *  ResolvableTypeProvider 可以解决编译器泛型擦除，运行期拿不到类型的问题
 * @author: 青石路
 * @createDate: 2024/1/17 15:26
 **/
@Data
public class BaseEvent<T> implements ResolvableTypeProvider {

    private T data;

    public BaseEvent(T data) {
        this.data = data;
    }

    @Override
    public ResolvableType getResolvableType() {
        return ResolvableType.forClassWithGenerics(getClass(), ResolvableType.forInstance(getData()));
    }
}
