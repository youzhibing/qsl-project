package com.qsl.event;

import com.qsl.entity.User;
import lombok.Data;

/**
 * @description:
 * @author: 青石路
 * @createDate: 2024/1/17 13:41
 **/
@Data
public class RegisterSuccessEvent {

    private User user;

    public RegisterSuccessEvent(User user) {
        this.user = user;
    }
}
