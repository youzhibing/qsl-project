package com.qsl.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @author: 青石路
 * @createDate: 2024/1/17 13:44
 **/
@Slf4j
@Component
public class RegisterEventListener {

    @EventListener
    @Order(100)
    public void handleNotifyEvent(RegisterSuccessEvent event) {
        log.info("监听到用户注册成功事件：" +
                "{}，你注册成功了哦。记得来玩儿~", event.getUser().getUserName());
    }

    @EventListener
    @Order(1)
    public void handleNotifyEvent1(RegisterSuccessEvent event) throws InterruptedException {
        log.info("----------监听到用户注册成功事件：" +
                "{}，你注册成功了哦。记得来玩儿~", event.getUser().getUserName());
        TimeUnit.SECONDS.sleep(1);
    }
}
