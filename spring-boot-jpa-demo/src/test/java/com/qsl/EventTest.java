package com.qsl;

import com.qsl.entity.Order;
import com.qsl.entity.User;
import com.qsl.event.BaseEvent;
import com.qsl.event.RegisterSuccessEvent;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * @description:
 * @author: 青石路
 * @createDate: 2024/1/17 13:45
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class EventTest {

    @Resource
    private ApplicationContext applicationContext;

    @Test
    public void publishEvent() {
        User user = new User();
        user.setUserName("青石路");
        applicationContext.publishEvent(new RegisterSuccessEvent(user));
    }

    @Test
    public void publicBaseEvent() {
        User user = new User();
        user.setUserName("青石路");

        Order order = new Order();
        order.setOrderNo("qsl_123456789");
        order.setCreatedTime(LocalDateTime.now());

        applicationContext.publishEvent(new BaseEvent<>(user));
        applicationContext.publishEvent(new BaseEvent<>(order));
    }
}
