package com.qsl;

import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @author: 青石路
 * @createDate: 2023/9/4 14:33
 **/
public class RedissonTest {

    public static void main(String[] args) throws Exception {
        Config config = new Config();
        config.useClusterServers().addNodeAddress("redis://10.106.0.219:26379", "redis://10.106.0.220:26379", "redis://10.106.0.221:26379")
                .setPassword("hello-#redis");
        RedissonClient redissonClient = Redisson.create(config);
        RLock rLock = redissonClient.getLock("hello");
        try {
            boolean locked = rLock.tryLock(1000, 120000, TimeUnit.MILLISECONDS);
            if (locked) {
                // 业务处理
                System.out.println("业务处理");
            }
        } catch (Exception e) {

        }
        redissonClient.shutdown();
    }
}
