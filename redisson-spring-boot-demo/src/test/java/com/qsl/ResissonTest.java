package com.qsl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2023/9/9 20:47
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ResissonTest {

    @Resource
    private RedissonClient redissonClient;

    @Test
    public void testLock() {
        RLock lock = redissonClient.getLock("redis:qsl:test");
        try {
            if (lock.tryLock(1, 3, TimeUnit.SECONDS)) {
                System.out.println("thread-id=" + Thread.currentThread().getId()
                        + " 获取锁成功，进行业务操作...");
                TimeUnit.SECONDS.sleep(2);
            } else {
                System.out.println("thread-id=" + Thread.currentThread().getId()
                        + " 锁获取失败");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        } finally {
            // 释放锁
            if (lock.isHeldByCurrentThread()) {
                System.out.println("thread-id=" + Thread.currentThread().getId()
                        + " 开始释放锁");
                lock.unlock();
                System.out.println("thread-id=" + Thread.currentThread().getId()
                        + " 锁释放成功");
            }
        }
    }

    @Test
    public void multiThreadLock() throws InterruptedException {
        for (int i=0; i<2; i++) {
            new Thread(this::testLock).start();
        }
        TimeUnit.SECONDS.sleep(10);
        System.out.println("main 退出");
    }
}
