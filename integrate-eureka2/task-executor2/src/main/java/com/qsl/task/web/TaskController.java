package com.qsl.task.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("task")
public class TaskController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskController.class);

    @GetMapping("exec")
    public String exec(Long taskId) {
        LOGGER.info("收到任务[taskId={}]执行请求", taskId);
        LOGGER.info("任务[taskId={}]执行中...", taskId);
        LOGGER.info("任务[taskId={}]执行完成", taskId);
        return "success";
    }
}
