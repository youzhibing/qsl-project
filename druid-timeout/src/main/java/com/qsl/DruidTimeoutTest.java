package com.qsl;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.util.JdbcConstants;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/7/5 9:06
 **/
public class DruidTimeoutTest {

    private static DruidDataSource dataSource = null;

    /**
     * 初始化
     * @author 博客园@青石路
     */
    static {
        try {
            dataSource = new DruidDataSource();
            dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
            dataSource.setUrl("jdbc:mysql://10.5.108.225:3311/datasource_mysql?useUnicode=true" +
                    "&useSSL=false&characterEncoding=utf8&serverTimezone=Asia/Shanghai");
            dataSource.setUsername("root");
            dataSource.setPassword("123456");
            dataSource.setMaxActive(10);
            dataSource.setMinIdle(5);
            dataSource.setInitialSize(1);
            dataSource.setMaxWait(10000);
            dataSource.setFilters("stat");
            dataSource.setValidationQuery("select 1");
            dataSource.setTestWhileIdle(true);
            dataSource.setTestOnBorrow(true);
            dataSource.setDefaultAutoCommit(true);
            dataSource.setValidationQueryTimeout(3);
            dataSource.setTestOnReturn(false);
            dataSource.setDefaultAutoCommit(false);
            dataSource.setTimeBetweenEvictionRunsMillis(5000);
            dataSource.setDbType(JdbcConstants.MYSQL);
            dataSource.setQueryTimeout(7000);
            dataSource.init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        /**
         * 15个查询线程 > 最大连接数（10）
         */
        /*for (int i=0; i<15; i++) {
            new Thread(() -> query()).start();
        }*/
        // query();
        System.out.println(Long.MAX_VALUE/(365*24*60*60*1000));
    }

    /**
     * sql 查询
     * @author 博客园@青石路
     */
    private static void query() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet("select * from tbl_order");
        SqlRowSetMetaData rsmd = sqlRowSet.getMetaData();
        int columnNum = rsmd.getColumnCount();
        while(sqlRowSet.next()) {
            for (int i = 1; i <= columnNum; i++) {
                Object obj = sqlRowSet.getObject(i);
                System.out.println(obj.getClass().getName() + " " + obj);
            }
            System.out.println("====================");
        }
    }
}
