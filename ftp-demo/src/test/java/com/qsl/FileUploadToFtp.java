package com.qsl;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.VFS;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;

import java.io.File;
import java.net.URI;

public class FileUploadToFtp {

    public static void main(String[] args) throws Exception {
        FileSystemManager systemManager = VFS.getManager();
        File file = new File("F:\\test_file.txt");
        URI uri = new URI("sftp", "root:123456", "192.168.2.118",
                22, "/qsl/file", null, null);

        SftpFileSystemConfigBuilder builder = SftpFileSystemConfigBuilder.getInstance();
        FileSystemOptions opts = new FileSystemOptions();
        builder.setStrictHostKeyChecking(opts, "no");
        builder.setUserDirIsRoot(opts, false);
        builder.setConnectTimeoutMillis(opts, 60000);
        builder.setPreferredAuthentications(opts, "password");
        builder.setDisableDetectExecChannel(opts, true);


        FileObject srcObject = systemManager.resolveFile(file.getParentFile(), file.getName());
        FileObject destObjectDir = systemManager.resolveFile(uri.toString(), opts);
        FileObject destObject = systemManager.resolveFile(destObjectDir, file.getName());
        destObject.copyFrom(srcObject, Selectors.SELECT_SELF);
    }
}
