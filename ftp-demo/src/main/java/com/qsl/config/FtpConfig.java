package com.qsl.config;

import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.provider.ftp.FtpFileSystemConfigBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * FTP 配置
 * @author 青石路
 */
@Configuration
@ConfigurationProperties(prefix = "app.ftp")
public class FtpConfig {

    private String host;
    private String userName;
    private String password;
    private Integer port;
    private String protocol;
    private String baseDir;

    @Bean("fptUri")
    public URI fptUri() throws URISyntaxException {
        return new URI(protocol, userName+":"+password, host,
                port, baseDir, null, null);
    }

    @Bean
    public FileSystemOptions fileSystemOptions() {
        FileSystemOptions opts = new FileSystemOptions();
        FtpFileSystemConfigBuilder builder = FtpFileSystemConfigBuilder.getInstance();
        builder.setControlEncoding(opts, "UTF-8");
        builder.setConnectTimeout(opts, 5000);
        builder.setUserDirIsRoot(opts, true);
        builder.setPassiveMode(opts, true);
        return opts;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getBaseDir() {
        return baseDir;
    }

    public void setBaseDir(String baseDir) {
        this.baseDir = baseDir;
    }
}
