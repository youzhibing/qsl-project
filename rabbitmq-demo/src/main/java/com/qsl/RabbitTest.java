package com.qsl;

import com.qsl.listener.SimpleMessageListenerContainer;
import com.rabbitmq.client.ConnectionFactory;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2024/11/11 14:15
 */
public class RabbitTest {

    public static void main(String[] args) throws Exception {

        CountDownLatch latch = new CountDownLatch(1);
        ConnectionFactory factory = initConnectionFactory();
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(factory);
        container.setAutoAck(true);
        container.setQueues(Arrays.asList("qsl.queue"));
        container.setConcurrentConsumers(3);
        container.setPrefetchCount(1);

        container.start();
        // 主线程不退出，相当于 spring 容器
        latch.await();
        container.shutdown();
    }

    public static ConnectionFactory initConnectionFactory() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("10.5.108.226");
        factory.setPort(5672);
        factory.setUsername("admin");
        factory.setPassword("admin");
        factory.setVirtualHost("/");
        factory.setConnectionTimeout(30000);
        return factory;
    }
}
