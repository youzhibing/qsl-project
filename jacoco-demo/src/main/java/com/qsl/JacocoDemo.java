package com.qsl;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/1/11 15:21
 **/
public class JacocoDemo {

    public int add(int a, int b) {
        if (a > 0 && b > 0) {
            return a + b;
        }
        return 0;
    }
}
