package com.qsl;

import org.junit.Test;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/1/11 15:36
 **/
public class JacocoDemoTest {

    @Test
    public void notCovered() {
    }

    @Test
    public void addTest() {
        JacocoDemo jacocoDemo = new JacocoDemo();
        jacocoDemo.add(1,2);
        jacocoDemo.add(-1, -2);
        jacocoDemo.add(3, -3);
        jacocoDemo.add(-4, 4);
    }
}
