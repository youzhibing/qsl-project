package com.qsl;

import com.qsl.service.Animal;

import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * @Description: spi 测试
 * @Author: 青石路
 * @CreateDate: 2022/5/16 17:28
 **/
public class SpiTest {

    public static void main(String[] args) {
        ServiceLoader<Animal> loader = ServiceLoader.load(Animal.class);
        Iterator<Animal> iterator = loader.iterator();
        System.out.println(iterator.hasNext());
    }
}
