package com.qsl;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: 青石路
 * @CreateDate: 2022/5/24 16:21
 **/
public class CollectionTest {

    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("张三");
        names.add("李四");
        System.out.println(names.toString());
    }
}
