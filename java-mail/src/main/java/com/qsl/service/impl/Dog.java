package com.qsl.service.impl;

import com.qsl.service.Animal;

/**
 * @Description:
 * @Author: 青石路
 * @CreateDate: 2022/5/16 17:26
 **/
public class Dog implements Animal {

    public String getName() {
        return "dog";
    }
}
