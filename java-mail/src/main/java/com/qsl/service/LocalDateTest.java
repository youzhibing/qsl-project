package com.qsl.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @Description:
 * @Author: 青石路
 * @CreateDate: 2022/5/22 10:11
 **/
public class LocalDateTest {

    public static void main(String[] args) {
        String today = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDate.now());
        System.out.println(today);
    }
}
