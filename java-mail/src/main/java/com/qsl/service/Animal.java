package com.qsl.service;

/**
 * @Description:
 * @Author: 青石路
 * @CreateDate: 2022/5/16 17:26
 **/
public interface Animal {

    /**
     * 获取名称
     * @return
     */
    String getName();
}
