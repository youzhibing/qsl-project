package com.qsl;

import com.google.common.util.concurrent.RateLimiter;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * @author 青石路
 * @date 2021/4/10 17:46
 */
public class GuavaTest {

    @Test
    public void guavaRateLimiter() {
        // RateLimiter create(double permitsPerSecond)
        RateLimiter r = RateLimiter.create(10);
        while (true)
        {
            System.out.println("get 1 tokens: " + r.acquire(1) + "s");
            System.out.println("get 1 tokens: " + r.acquire(1) + "s");
            System.out.println("get 1 tokens: " + r.acquire(1) + "s");
            System.out.println("get 1 tokens: " + r.acquire(1) + "s");
            System.out.println("end");
        }
    }
}
