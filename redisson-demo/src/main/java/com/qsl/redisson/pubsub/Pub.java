package com.qsl.redisson.pubsub;

import org.redisson.Redisson;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

/**
 * @author 青石路
 * @date 2021/6/13 17:00
 */
public class Pub {

    public static void main(String[] args) {
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://192.168.1.110:6379");
        RedissonClient redissonClient = Redisson.create(config);
        RTopic topic = redissonClient.getTopic("dict_name");

        // 发布
        long receivedClientCount = topic.publish(new Msg(2, "张三", "你好，我是李四"));
        System.out.println("一共 " + receivedClientCount + " 客户端收到了消息");

        redissonClient.shutdown();
    }
}
