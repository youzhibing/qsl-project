package com.qsl.redisson.pubsub;

import org.redisson.Redisson;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

/**
 * @author 青石路
 * @date 2021/6/13 17:00
 */
public class Sub {

    public static void main(String[] args) {
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://192.168.1.110:6379");
        RedissonClient redissonClient = Redisson.create(config);
        RTopic topic = redissonClient.getTopic("dict_name");

        // 订阅
        int listenerId = topic.addListener(Msg.class, (channel, msg) -> {
            System.out.println("channel : " + channel.toString());
            System.out.println("msg : " + msg);
        });

        System.out.println("线程不会阻塞, 可以取消订阅");
        // topic.removeListener(listenerId);
    }
}
