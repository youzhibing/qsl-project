package com.qsl.redisson.pubsub;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 青石路
 * @date 2021/6/13 17:02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Msg implements Serializable {
    private Integer id;
    private String title;
    private String content;
}
