CREATE TABLE `tbl_user` (
  `user_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` VARCHAR(100) NOT NULL COMMENT '用户姓名',
  `password` VARCHAR(100) NOT NULL COMMENT '用户密码',
  `birthday` DATE COMMENT '出生日期',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB COMMENT='用户';

INSERT INTO tbl_user(user_name, password, birthday) VALUES
('张三', 'zhangsan', '1999-12-23'),
('李四', 'lisi', '1995-08-11'),
('王五', '王五', '2005-05-05');