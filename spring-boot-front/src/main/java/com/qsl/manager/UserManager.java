package com.qsl.manager;

import com.qsl.entity.User;
import com.qsl.service.UserService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import static com.qsl.constant.Constant.QSL_FANOUT_EXCHANGE;

/**
 * @description: User Manager
 * @author: 博客园@青石路
 * @date: 2024/1/25 19:48
 */
@Component
public class UserManager {

    @Resource
    private UserService userService;
    @Resource
    private RabbitTemplate rabbitTemplate;

    public boolean update(User user) {
        // 1、更新用户
        boolean updated = userService.updateUser(user);
        // 2、发送消息
        rabbitTemplate.convertAndSend(QSL_FANOUT_EXCHANGE, null, user.getUserId().toString());
        return updated;
    }
}
