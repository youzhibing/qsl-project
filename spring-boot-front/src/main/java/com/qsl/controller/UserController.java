package com.qsl.controller;

import com.qsl.entity.User;
import com.qsl.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2024/1/7 16:25
 */
@RequestMapping("/user")
@RestController
@Slf4j
public class UserController {

    @Resource
    private UserService userService;

    @GetMapping("/{userId}")
    public User getUser(@PathVariable Long userId) {
        User user = userService.getById(userId);
        log.info("user = {}", user);
        return user;
    }
}
