package com.qsl;

import com.qsl.entity.User;
import com.qsl.manager.UserManager;
import com.qsl.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2024/1/7 16:17
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {

    @Resource
    private UserService userService;
    @Resource
    private UserManager userManager;

    @Test
    public void update() {
        User user = new User();
        user.setUserId(1L);
        user.setPassword("zhangsan2");
        userService.update(user);
    }
}
