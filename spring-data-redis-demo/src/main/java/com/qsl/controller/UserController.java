package com.qsl.controller;

import com.qsl.entity.User;
import com.qsl.service.IUserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: 青石路
 */
@RestController
@RequestMapping("user")
public class UserController {

    @Resource
    private IUserService userService;

    @PostMapping("save")
    public String save(@RequestBody User user) {
        return userService.saveNotExist(user);
    }
}
