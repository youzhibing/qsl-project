package com.qsl.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qsl.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: 青石路
 */
@Mapper
public interface UserDao extends BaseMapper<User> {
}
