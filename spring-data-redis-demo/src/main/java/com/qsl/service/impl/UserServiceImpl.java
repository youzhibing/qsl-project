package com.qsl.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qsl.dao.UserDao;
import com.qsl.entity.User;
import com.qsl.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author: 青石路
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements IUserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String saveNotExist(User user) {
        Object o = redisTemplate.opsForValue().get("dataredis:user:" + user.getUserName());
        if (o != null) {
            LOG.info("用户已存在");
            return "用户已存在";
        }
        redisTemplate.opsForValue().set("dataredis:user:" + user.getUserName(), user);
        this.save(user);
        return "用户保存成功";
    }
}
