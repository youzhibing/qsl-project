package com.qsl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qsl.entity.User;

/**
 * @author: 青石路
 */
public interface IUserService extends IService<User> {

    String saveNotExist(User user);
}
