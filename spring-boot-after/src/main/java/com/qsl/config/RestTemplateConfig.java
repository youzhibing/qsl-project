package com.qsl.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestTemplateConfig {

    /**
     * 连接池获取连接的超时时长，单位毫秒；默认 10秒
     */
    @Value("${rest.connection.connection-request-timeout:10000}")
    private int connectionRequestTimeout;
    /**
     * 与外部系统建立连接的超时时长，单位毫秒；默认10秒
     */
    @Value("${rest.connection.connect-timeout:10000}")
    private int connectTimeout;
    /**
     * 等待外部系统响应的超时时长，单位毫秒；默认60秒
     */
    @Value("${rest.connection.read-timeout:60000}")
    private int readTimeout;

    @Bean
    public RestTemplate restTemplate() {
        //配置HTTP超时时间
        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setConnectionRequestTimeout(connectionRequestTimeout);
        httpRequestFactory.setConnectTimeout(connectTimeout);
        httpRequestFactory.setReadTimeout(readTimeout);
        return new RestTemplate(httpRequestFactory);
    }
}
