package com.qsl.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.qsl.constant.Constant.QSL_FANOUT_EXCHANGE;
import static com.qsl.constant.Constant.QSL_QUEUE;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2024/1/7 15:26
 */
@Configuration
public class RabbitConfig {

    @Bean
    public FanoutExchange qslExchange() {
        return new FanoutExchange(QSL_FANOUT_EXCHANGE, true, false);
    }

    @Bean
    public Queue qslQueue() {
        return new Queue(QSL_QUEUE);
    }

    @Bean
    public Binding bindingQslQueueToExchange() {
        return BindingBuilder.bind(qslQueue()).to(qslExchange());
    }
}
