package com.qsl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qsl.entity.User;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2024/1/7 15:45
 */
public interface UserMapper extends BaseMapper<User> {
}
