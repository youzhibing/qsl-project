package com.qsl.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qsl.entity.User;
import com.qsl.mapper.UserMapper;
import com.qsl.service.UserService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static com.qsl.constant.Constant.QSL_FANOUT_EXCHANGE;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2024/1/7 15:48
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private RabbitTemplate rabbitTemplate;
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(User user) {
        // 1、更新数据
        boolean updated = this.saveOrUpdate(user);
        // 2、发消息，通知下游数据更新了
        rabbitTemplate.convertAndSend(QSL_FANOUT_EXCHANGE, null, user.getUserId().toString());
        return updated;
    }
}
