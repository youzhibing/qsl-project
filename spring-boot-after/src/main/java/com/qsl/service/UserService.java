package com.qsl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qsl.entity.User;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2024/1/7 15:47
 */
public interface UserService extends IService<User> {

    /**
     * 用户更新
     * @param user 用户信息
     * @return 更新是否成功
     */
    boolean update(User user);
}
