package com.qsl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2024/1/7 15:42
 */
@Data
@TableName("tbl_user")
public class User {

    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    private String userName;

    private String password;

    private LocalDate birthday;
}
