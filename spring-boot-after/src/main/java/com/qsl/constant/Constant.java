package com.qsl.constant;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2024/1/7 15:28
 */
public interface Constant {

    String QSL_FANOUT_EXCHANGE = "qsl_exchange";

    String QSL_QUEUE = "qsl_queue";
}
