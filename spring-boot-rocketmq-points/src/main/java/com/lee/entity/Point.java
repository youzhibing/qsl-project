package com.lee.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/***
 * @description: 积分
 * @author : 青石路
 * @date: 2021/11/13 16:41
 */
@TableName("t_point")
@Setter
@Getter
public class Point {

    @TableId(type = IdType.AUTO)
    private Long pointId;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 积分数量
     */
    private BigDecimal pointNum;

    /**
     * 备注
     */
    private String note;
}
