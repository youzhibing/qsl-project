package com.lee.mq.consumer;

import com.lee.mq.listener.OrderSuccessListener;
import lombok.Setter;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Objects;

/***
 * @description: 下单成功消费者
 * @author : 青石路
 * @date: 2021/11/13 12:04
 */
@Component
@ConfigurationProperties(prefix = "rocket")
@Setter
public class OrderSuccessConsumer implements InitializingBean, DisposableBean {

    private String nameSrvAddr;
    private String pointsGroupName;
    private String pointsTopic;
    private String pointsTag;

    @Resource
    private OrderSuccessListener orderSuccessListener;

    private DefaultMQPushConsumer defaultMQPushConsumer;

    @Override
    public void destroy() throws Exception {
        if (Objects.nonNull(defaultMQPushConsumer)) {
            defaultMQPushConsumer.shutdown();
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        defaultMQPushConsumer = new DefaultMQPushConsumer(pointsGroupName);
        defaultMQPushConsumer.setNamesrvAddr(nameSrvAddr);
        defaultMQPushConsumer.subscribe(pointsTopic,pointsTag);
        defaultMQPushConsumer.registerMessageListener(orderSuccessListener);
        defaultMQPushConsumer.start();
    }
}
