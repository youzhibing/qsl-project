package com.lee.controller;

import com.lee.entity.Point;
import com.lee.service.PointService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/***
 * @description: point controller
 * @author : 青石路
 * @date: 2021/11/13 16:50
 */
@RestController
@RequestMapping("/point")
public class PointController {

    @Resource
    private PointService pointService;

    @GetMapping("/{pointId}")
    public Point get(@PathVariable Long pointId) {
        return pointService.getById(pointId);
    }

}
