package com.lee.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lee.entity.Point;
import org.apache.ibatis.annotations.Mapper;

/***
 * @description: point mapper
 * @author : 青石路
 * @date: 2021/11/13 16:44
 */
@Mapper
public interface PointMapper extends BaseMapper<Point> {
}
