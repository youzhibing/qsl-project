package com.lee.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lee.entity.Point;
import com.lee.vo.OrderDTO;

/***
 * @description: point service
 * @author : 青石路
 * @date: 2021/11/13 16:45
 */
public interface PointService extends IService<Point> {

    /**
     * 添加积分
     * @param order
     */
    void add(OrderDTO order);
}
