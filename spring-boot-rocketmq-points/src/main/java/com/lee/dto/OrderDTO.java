package com.lee.vo;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/***
 * @description: order dto，更优的做法是 order 服务暴露出实体，point 服务引用（只在一个地方维护，方便后续拓展）
 * @author : 青石路
 * @date: 2021/11/14 10:37
 */
@Setter
@Getter
public class OrderDTO {

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 下单金额
     */
    private BigDecimal orderAmount;

    /**
     * 备注
     */
    private String note;
}
