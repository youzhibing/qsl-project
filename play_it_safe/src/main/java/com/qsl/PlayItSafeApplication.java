package com.qsl;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2024/2/21 9:24
 */
@MapperScan(basePackages = "com.qsl.mapper")
@SpringBootApplication
public class PlayItSafeApplication {
    public static void main(String[] args) {
        SpringApplication.run(PlayItSafeApplication.class, args);
    }
}