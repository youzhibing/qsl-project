package com.qsl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qsl.entity.TblOrder;

/**
 * @Description:
 * @Author: 博客园@青石路
 **/
public interface OrderMapper extends BaseMapper<TblOrder> {
}
