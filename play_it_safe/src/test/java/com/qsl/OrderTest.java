package com.qsl;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2024/2/21 16:56
 */

import com.qsl.entity.TblOrder;
import com.qsl.mapper.OrderMapper;
import com.sun.deploy.util.StringUtils;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderTest {

    @Resource
    private OrderMapper orderMapper;

    @Test
    public void orderListAllTest() {
        List<TblOrder> tblOrders = orderMapper.selectList(null);
        System.out.println(tblOrders);
    }

    /**
     * 优化了不该优化的bug
     */
    @Test
    public void optimizeBug() {
        // 模拟数据
        Long executionId = 100L;
        List<String> fileIds = Arrays.asList("1", "2", "3");
        String status = "success";
        String dataDate = "2022-02-21";

        // 优化后的校验逻辑
        List<String> dbFileIds = listFileGenerateLog(executionId, fileIds, status, dataDate);
        List<String> notGeneratedFileIds = fileIds.stream().filter(fileId -> !dbFileIds.contains(fileId))
                .collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(notGeneratedFileIds)) {
            throw new RuntimeException("依赖的资源[" + StringUtils.join(notGeneratedFileIds, ",") + "]未生成");
        }
    }


    /**
     * 获取文件生成日志
     * @param executionId 执行id
     * @param fileIds 文件id集合
     * @param status 生成状态
     * @param dataDate 数据日期
     * @return 文件id集合
     */
    public static List<String> listFileGenerateLog(Long executionId, List<String> fileIds, String status, String dataDate) {
        // TODO 从数据库去查
        List<String> dbFileIds = Lists.newArrayList();
        return dbFileIds;
    }
}
