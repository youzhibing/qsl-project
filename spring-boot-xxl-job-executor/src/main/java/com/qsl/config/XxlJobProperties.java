package com.qsl.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "xxl.job.executor")
public class XxlJobProperties {
	private String adminAddresses;
	private String appName;
	private String ip;
	private int port;
	private String accessToken;
	private String logPath;
	private int logRetentionDays;

	public synchronized String getAdminAddresses() {
		return adminAddresses;
	}

	public synchronized void setAdminAddresses(String adminAddresses) {
		this.adminAddresses = adminAddresses;
	}

	public synchronized String getAppName() {
		return appName;
	}

	public synchronized void setAppName(String appName) {
		this.appName = appName;
	}

	public synchronized String getIp() {
		return ip;
	}

	public synchronized void setIp(String ip) {
		this.ip = ip;
	}

	public synchronized int getPort() {
		return port;
	}

	public synchronized void setPort(int port) {
		this.port = port;
	}

	public synchronized String getAccessToken() {
		return accessToken;
	}

	public synchronized void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public synchronized String getLogPath() {
		return logPath;
	}

	public synchronized void setLogPath(String logPath) {
		this.logPath = logPath;
	}

	public synchronized int getLogRetentionDays() {
		return logRetentionDays;
	}

	public synchronized void setLogRetentionDays(int logRetentionDays) {
		this.logRetentionDays = logRetentionDays;
	}

}
