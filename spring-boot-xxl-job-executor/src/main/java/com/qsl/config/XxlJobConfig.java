package com.qsl.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;

/**
 * @author 青石路
 */
@Configuration
public class XxlJobConfig {
	private final Logger logger = LoggerFactory.getLogger(XxlJobConfig.class);

	@Bean(initMethod = "start",destroyMethod = "destroy")
	public XxlJobSpringExecutor xxlJobExecutor(XxlJobProperties xxlJob) {
		logger.info(">>>>>>>>>>> xxl-job config init.");
		XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
		xxlJobSpringExecutor.setAdminAddresses(xxlJob.getAdminAddresses());
		xxlJobSpringExecutor.setAppName(xxlJob.getAppName());
		xxlJobSpringExecutor.setIp(xxlJob.getIp());
		xxlJobSpringExecutor.setPort(xxlJob.getPort());
		xxlJobSpringExecutor.setAccessToken(xxlJob.getAccessToken());
		xxlJobSpringExecutor.setLogPath(xxlJob.getLogPath());
		xxlJobSpringExecutor.setLogRetentionDays(xxlJob.getLogRetentionDays());
		return xxlJobSpringExecutor;
	}
}
