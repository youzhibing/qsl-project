package com.qsl.jobhandler;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author 青石路
 */
@Component
@JobHandler("qslJobHandler")
public class QslJobHandler extends IJobHandler {

    private static final Logger LOG = LoggerFactory.getLogger(QslJobHandler.class);

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        LOG.info("param = {}", param);

        LOG.info("QslJobHandler 业务处理开始");
        // TODO 业务处理
        TimeUnit.SECONDS.sleep(4);
        LOG.info("QslJobHandler 业务处理完成");
        return ReturnT.SUCCESS;
    }
}
