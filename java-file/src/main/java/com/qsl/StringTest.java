package com.qsl;

import org.apache.commons.lang3.StringUtils;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/7/5 14:11
 **/
public class StringTest {

    private static final String CHECK_REPORT_SQL = "SELECT \n" +
            "\tCASE WHEN im.check_type = 'ADD' THEN '新增表'\n" +
            "\t\t\tWHEN im.check_type = 'MODIFY' THEN '修改表'\n" +
            "\t\t\tELSE im.check_type\n" +
            "\t\tEND AS '校验类型', \n" +
            "\tCASE WHEN im.check_status = 0 THEN '未校验'\n" +
            "\t\t\tWHEN im.check_status = 1 THEN '通过'\n" +
            "\t\t\tWHEN im.check_status = 2 THEN '未通过'\n" +
            "\t\t\tELSE im.check_status\n" +
            "\t\tEND AS '检查状态',\n" +
            "\tit.rule_id AS '表规范规则.xlsx的规则编号',it.fail_msg AS '未通过原因'\n" +
            "FROM table_standard_check_import im\n" +
            "LEFT JOIN table_standard_check_fail_item it ON it.import_id = im.import_id\n" +
            "WHERE im.import_id = %s";


    public static void main(String[] args) throws Exception {
        final String PREFIX = "{", SUFFIX = "}";
        StringBuilder sb = new StringBuilder("tsxs_{yyyyMMdd}_{n}.txt"); // tsxs_{yyyyMMdd}_{n}.txt
        int start = 0, end = 0;
        start = sb.indexOf(PREFIX, start);
        System.out.println(start);

        end = sb.indexOf(SUFFIX, start + SUFFIX.length());

        System.out.println(end);

        String placeHolder = sb.substring(start + PREFIX.length(), end);

        System.out.println(placeHolder);

        System.out.println("".getBytes(StandardCharsets.UTF_8).length);

        System.out.println(System.getProperty("java.io.tmpdir"));

        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ofPattern(".yyyyMMddHHmmssSSS")));

        System.out.println(String.format(CHECK_REPORT_SQL, 1000));

        String str = "MD401|00001|长和                          |1231234|";

        String[] split = str.split("\\|");
        System.out.println(split.length);
        System.out.println(split[2].substring(0, split[2].indexOf(" ")) + ";");
        System.out.println(StringUtils.join(new String[]{"hello", null, "world"}, "|"));
        byte[] gbks = " ".getBytes("GBK");
        System.out.println(gbks.length);
        String[] split1 = "abc,".split(",");
        System.out.println(split1.length);

        String encodeData = "  123";
        String encodeDataNoWhitespace = StringUtils.trim(encodeData);
        String leadingWhitespace = encodeData.substring(0, encodeData.indexOf(encodeDataNoWhitespace));
        String trailingWhitespace = encodeData.substring(encodeData.indexOf(encodeDataNoWhitespace) + encodeDataNoWhitespace.length());
        System.out.println(leadingWhitespace.length());
        System.out.println(trailingWhitespace.length());
        System.out.println("123|1".endsWith("|"));
    }

    public int strHash(String str) {
        int result = 0;
        for (char c : str.toCharArray()) {
            result = (int) ((31L * result + c) % Integer.MAX_VALUE);
        }
        return result;
    }
}



