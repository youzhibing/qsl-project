package com.qsl.excel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/2/27 13:11
 **/
public class WriteTest {

    public static void main(String[] args) throws IOException {
        overrideTest();
    }

    public static void overrideTest() throws IOException {
        try(FileOutputStream fileOutputStream = new FileOutputStream("D:/hello123.xlsx");
            Workbook workbook = new HSSFWorkbook();) {
            Sheet sheet1 = workbook.createSheet("a");
            Sheet sheet2 = workbook.createSheet("b");
            // Sheet sheet3 = workbook.createSheet("3");
            workbook.write(fileOutputStream);
        }
    }
}
