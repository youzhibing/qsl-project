package com.qsl.sftp;

import org.apache.commons.vfs2.FileContent;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.VFS;
import org.apache.commons.vfs2.provider.ftp.FtpFileSystemConfigBuilder;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/12/1 10:05
 **/
public class VfsTest {

    public static void main(String[] args) throws Exception {
        // readRemoteFtp();
        copyLocalToRemote();
    }

    public static void copyLocalToRemote() throws Exception {
        FileSystemManager fsManager = VFS.getManager();

        FileSystemOptions opts = new FileSystemOptions();
        FtpFileSystemConfigBuilder instance = FtpFileSystemConfigBuilder.getInstance();
        instance.setControlEncoding(opts, "UTF-8");
        instance.setConnectTimeout(opts, 5000);
        instance.setUserDirIsRoot(opts, true);
        instance.setPassiveMode(opts, true);
        instance.setSoTimeout(opts, 10000);
        instance.setDataTimeout(opts, 10000);

        final FileObject local = fsManager.resolveFile("file://D:\\xxl-job-executor-sample-springboot-2.1.0.0.jar");
        FileObject remote = fsManager.resolveFile("ftp://foo:123@192.168.9.222/xxl-job-executor-sample-springboot-2.1.0.0.jar", opts);
        // FileObject local = fsManager.resolveFile("file://D:\\qsl.txt");
        // FileObject remote = fsManager.resolveFile("ftp://foo:123@192.168.9.222/qsl.txt");


        long startTime = System.currentTimeMillis();
        remote.copyFrom(local, Selectors.SELECT_SELF);
        System.out.println("花费时间：" + (System.currentTimeMillis() - startTime)/1000);
    }

    public static void readLocal() throws Exception {
        FileSystemManager fsManager = VFS.getManager();
        final FileObject fileObject = fsManager.resolveFile("file://D:\\qsl.txt");
        final FileContent content = fileObject.getContent();
        System.out.println(content.getString("utf-8"));
    }

    public static void readRemoteFtp() throws  Exception{
        FileSystemManager manager = VFS.getManager();
        /* 5. Ftp、Ftps、Sftp，需要依赖于com.jcraft.jsch*/
        FileSystemOptions opts = new FileSystemOptions();
        FtpFileSystemConfigBuilder instance = FtpFileSystemConfigBuilder.getInstance();

        instance.setControlEncoding(opts, "UTF-8");
        instance.setConnectTimeout(opts, 5000);
        instance.setUserDirIsRoot(opts, true);
        instance.setPassiveMode(opts, true);

        final FileObject sftpFileObject = manager.resolveFile("ftp://foo:123@192.168.9.223/abc/qsl.txt", opts);
        FileContent content = sftpFileObject.getContent();
        System.out.println(content.getString("utf-8"));
    }

    public static void readRemoteSftp() throws Exception {
        FileSystemManager manager = VFS.getManager();
        /* 5. Ftp、Ftps、Sftp，需要依赖于com.jcraft.jsch*/
        FileSystemOptions opts = new FileSystemOptions();
        // 不严格检查连接的HostKey
        SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(opts, "no");

        final FileObject sftpFileObject = manager.resolveFile("sftp://foo:123@192.168.9.223/qsl.txt", opts);
        final String sftpContent = sftpFileObject.getContent().getString("utf-8");
        System.out.println("string = " + sftpContent);
    }
}
