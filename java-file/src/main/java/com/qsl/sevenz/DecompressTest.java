package com.qsl.sevenz;

import net.sf.sevenzipjbinding.ArchiveFormat;
import net.sf.sevenzipjbinding.IInArchive;
import net.sf.sevenzipjbinding.ISequentialOutStream;
import net.sf.sevenzipjbinding.SevenZip;
import net.sf.sevenzipjbinding.SevenZipException;
import net.sf.sevenzipjbinding.impl.RandomAccessFileInStream;
import net.sf.sevenzipjbinding.simple.ISimpleInArchive;
import net.sf.sevenzipjbinding.simple.ISimpleInArchiveItem;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/2/23 9:22
 **/
public class DecompressTest {

    public static void main(String[] args) {
        String filePath = "C:\\Users\\youzb\\Desktop\\EB00006D.LLX.gz.202302221";
        String targetFileDir = "D:\\compress";
        decompressFile(filePath, targetFileDir);
    }

    public static void decompressFile(String inputFilePath, final String targetFileDir) {
        RandomAccessFile randomAccessFile = null;
        IInArchive inArchive = null;

        try {
            File newdir = new File(targetFileDir);
            if (!newdir.exists()) {
                newdir.mkdirs();
                newdir = null;
            }

            randomAccessFile = new RandomAccessFile(inputFilePath, "r");
            RandomAccessFileInStream t = new RandomAccessFileInStream(randomAccessFile);
            String rar = ".*\\.[Rr][Aa][Rr]$";
            String zip7 = "7z";
            String upzip7 = "7Z";
            if (inputFilePath.matches(rar)) {
                boolean flag = false;

                try {
                    inArchive = SevenZip.openInArchive(ArchiveFormat.RAR5, t);
                } catch (Exception var32) {
                    flag = true;
                    var32.printStackTrace();
                }

                if (flag) {
                    System.out.println("尝试低版本解压rar");
                    inArchive = SevenZip.openInArchive(ArchiveFormat.RAR, t);
                    System.out.println("低版本解压rar成功");
                }
            } else if (inputFilePath.contains(".gz.") || inputFilePath.contains(".GZ.")) {
                inArchive = SevenZip.openInArchive(ArchiveFormat.GZIP, t);
            }  else if (!inputFilePath.endsWith(zip7) && !inputFilePath.endsWith(upzip7)) {
                inArchive = SevenZip.openInArchive(ArchiveFormat.ZIP, t);
            } else {
                inArchive = SevenZip.openInArchive(ArchiveFormat.SEVEN_ZIP, t);
            }

            ISimpleInArchive simpleInArchive = inArchive.getSimpleInterface();
            ISimpleInArchiveItem[] var10 = simpleInArchive.getArchiveItems();
            int var11 = var10.length;

            for(int var12 = 0; var12 < var11; ++var12) {
                final ISimpleInArchiveItem item = var10[var12];
                String targetFile = targetFileDir + File.separator + item.getPath();
                File f = new File(targetFile);
                if (f.exists()) {
                    f.delete();
                    f.createNewFile();
                } else {
                    f.createNewFile();
                }

                final int[] hash = new int[]{0};
                if (!item.isFolder()) {
                    final long[] sizeArray = new long[1];
                    item.extractSlow(new ISequentialOutStream() {
                        @Override
                        public int write(byte[] data) throws SevenZipException {
                            FileOutputStream fos = null;

                            try {
                                if (item.getPath().indexOf(File.separator) > 0) {
                                    String path = targetFileDir + File.separator + item.getPath().substring(0, item.getPath().lastIndexOf(File.separator));
                                    File folderExisting = new File(path);
                                    if (!folderExisting.exists()) {
                                        (new File(path)).mkdirs();
                                    }
                                }

                                fos = new FileOutputStream(targetFileDir + File.separator + item.getPath(), true);
                                fos.write(data);
                            } catch (Exception var13) {
                                var13.printStackTrace();
                            } finally {
                                if (fos != null) {
                                    try {
                                        fos.close();
                                    } catch (Exception var12) {
                                        var12.printStackTrace();
                                    }
                                }

                            }

                            int[] var10000 = hash;
                            var10000[0] ^= Arrays.hashCode(data);
                            long[] var15 = sizeArray;
                            var15[0] += (long)data.length;
                            return data.length;
                        }
                    });
                }
            }
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            if (inArchive != null) {
                try {
                    inArchive.close();
                } catch (SevenZipException var31) {
                    var31.printStackTrace();
                }
            }

            if (randomAccessFile != null) {
                try {
                    randomAccessFile.close();
                } catch (IOException var30) {
                    var30.printStackTrace();
                }
            }

        }

    }
}
