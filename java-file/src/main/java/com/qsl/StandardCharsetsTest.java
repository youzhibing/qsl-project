package com.qsl;

import java.nio.charset.StandardCharsets;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/8/8 8:49
 **/
public class StandardCharsetsTest {

    public static void main(String[] args) {
        System.out.println(StandardCharsets.UTF_8.name());
        System.out.println(StandardCharsets.UTF_8.displayName());
    }
}
