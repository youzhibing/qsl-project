package com.qsl;

import com.google.common.collect.Lists;
import com.google.common.primitives.Bytes;

import java.util.List;

/**
 * @Description:
 * @Author: 青石路
 * @CreateDate: 2022/7/29 11:21
 **/
public class ByteTest {

    public static void main(String[] args) throws Exception {
        // GBK 下，一个汉字 2 byte，一个字符 1 byte
        // UTF-8 下，一个汉字 3 byte，一个字符 1 byte
        // UTF-16LE 下，一个汉字 2 byte，一个字符 2 byte
        String str = " ";
        byte[] bytes = str.getBytes("UTF-16LE");
        System.out.println(bytes.length);
        List<Byte> lineBytes = Lists.newLinkedList();
        lineBytes.addAll(Bytes.asList("".getBytes("GBK")));
        System.out.println(lineBytes.size());

        byte[] bytes1 = "沛嘉医疗-B".getBytes("UTF-16LE");
        System.out.println(new String(bytes1, "UTF-16LE"));
    }
}
