package com.qsl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @Description: 洗牌 - 即乱序处理
 * @Author:
 * @CreateDate: 2022/7/20 17:43
 **/
public class ShuffleTest {

    public static void main(String[] args) {
        String[] paths = new String[]{"/opt/UserData1", "/opt/UserData2"};
        System.out.println(paths);
        // paths 乱序处理
        List<String> pathList = Arrays.asList(paths);
        Collections.shuffle(pathList);

        paths = pathList.toArray(new String[0]);
        System.out.println(paths);
        for (String path : paths) {
            System.out.println(path);
        }
    }
}
