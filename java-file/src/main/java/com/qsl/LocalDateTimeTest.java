package com.qsl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/8/17 17:03
 **/
public class LocalDateTimeTest {

    public static void main(String[] args) {
        /*DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);
        System.out.println(dtf.format(now));
        Timestamp timestamp = Timestamp.valueOf(now);
        System.out.println(timestamp);*/

        /*SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = dateFormat.parse("2022-08-15 13:23:33");
        System.out.println(date.getTime());*/

        long timestamp = LocalDateTime.now().toEpochSecond(ZoneOffset.of("+8")) * 1000;
        System.out.println(timestamp);

        Timestamp t = new Timestamp(System.currentTimeMillis());
        System.out.println(t.getTime());
        System.out.println(t.getTime() / 1000 * 1000);
    }
}
