package com.qsl;

import java.nio.ByteBuffer;
import java.util.Optional;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/7/29 10:00
 **/
public class OptionalTest {

    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(2048);
        ByteBuffer byteBuffer = Optional.ofNullable(buffer).orElse(ByteBuffer.allocate(1024));
        int capacity = byteBuffer.capacity();
        System.out.println(capacity);
    }
}
