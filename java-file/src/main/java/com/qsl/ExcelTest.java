package com.qsl;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/10/31 17:08
 **/
public class ExcelTest {

    public static void main(String[] args) throws Exception {
        //test2007();
        test2003();
    }

    private static void test2007() throws IOException {
        File file = new File("D:/123.xlsx");
        FileOutputStream fos = new FileOutputStream(file);
        SXSSFWorkbook sheets = new SXSSFWorkbook(3000);
        // sheets.createSheet();
        if (sheets.getNumberOfSheets() < 1) {
            System.out.println("没有 sheet");
            sheets.createSheet();
        }
        sheets.write(fos);
        fos.flush();
        sheets.close();
        fos.close();
    }

    private static void test2003() throws Exception {
        File file = new File("D:/123.xls");
        FileOutputStream fos = new FileOutputStream(file);
        Workbook workbook = new HSSFWorkbook();
        if (workbook.getNumberOfSheets() < 1) {
            System.out.println("没有 sheet");
            workbook.createSheet();
        }
        workbook.write(fos);
        fos.flush();
        workbook.close();
        fos.close();
    }
}
