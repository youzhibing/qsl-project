package com.qsl.easyexcel;

import com.alibaba.excel.EasyExcel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/4/3 10:32
 **/
public class ExcelWrite {

    public static void main(String[] args) {
        List<DemoData> datas = initDate();
        EasyExcel.write("D:/easyexcel.xlsx", DemoData.class).sheet("模板").doWrite(datas);
    }

    public static List<DemoData> initDate() {
        List<DemoData> dates = new ArrayList<>();
        DemoData demoData =  new DemoData();
        demoData.setBillingType("国内 APP");
        demoData.setFee(new BigDecimal("50.00"));
        demoData.setNums(10);
        demoData.setMonthlyRemainDiscountPackages(50);
        demoData.setOrgName("全部");
        demoData.setReportDate(202303);
        demoData.setUseMonthlyDiscountPackages(3);
        demoData.setUseMonthlyGiveaways(2);
        demoData.setTotalFee(new BigDecimal("150.00"));
        dates.add(demoData);

        demoData =  new DemoData();
        demoData.setBillingType("海外用户");
        demoData.setFee(new BigDecimal("100.00"));
        demoData.setNums(1);
        demoData.setMonthlyRemainDiscountPackages(0);
        demoData.setOrgName("全部");
        demoData.setReportDate(202303);
        demoData.setUseMonthlyDiscountPackages(0);
        demoData.setUseMonthlyGiveaways(0);
        demoData.setTotalFee(new BigDecimal("100.00"));
        dates.add(demoData);

        return dates;
    }
}
