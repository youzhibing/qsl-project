package com.qsl.easyexcel;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentLoopMerge;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.enums.poi.HorizontalAlignmentEnum;
import com.alibaba.excel.enums.poi.VerticalAlignmentEnum;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/3/27 14:04
 **/
@Data
public class DemoData {

    /**
     * 机构名称
     *      招银国际、永隆银行、全部
     */
    @ExcelProperty("公司名称")
    @ColumnWidth(20)
    @ContentLoopMerge(eachRow = 2)
    @ContentStyle(verticalAlignment = VerticalAlignmentEnum.CENTER, horizontalAlignment = HorizontalAlignmentEnum.CENTER)
    private String orgName;
    /**
     * 月份
     */
    @ExcelIgnore
    private Integer reportDate;

    /**
     * 类型
     */
    @ExcelProperty("类型")
    @ColumnWidth(20)
    private String billingType;

    /**
     * 人数
     */
    @ExcelProperty("人数")
    @ColumnWidth(15)
    private Integer nums = 0;

    /**
     * 当前月份使用国内赠送人数
     */
    @ExcelProperty("当前月份使用国内赠送人月")
    @ColumnWidth(35)
    private Integer useMonthlyGiveaways = 0;

    /**
     * 当前月份使用优惠包人数
     */
    @ExcelProperty("当前月份使用优惠包人月")
    @ColumnWidth(35)
    private Integer useMonthlyDiscountPackages = 0;

    /**
     * 当前月后，年优惠包剩余数量
     */
    @ExcelProperty("当前年剩余优惠包人月")
    @ColumnWidth(35)
    private Integer monthlyRemainDiscountPackages = 0;

    /**
     * 费用
     */
    @ExcelProperty("费用（HKD）")
    @ColumnWidth(20)
    private BigDecimal fee = BigDecimal.ZERO;

    /**
     * 总计费用
     */
    @ExcelProperty("总计费用（HKD）")
    @ContentLoopMerge(eachRow = 2)
    @ColumnWidth(25)
    @ContentStyle(verticalAlignment = VerticalAlignmentEnum.CENTER, horizontalAlignment = HorizontalAlignmentEnum.CENTER)
    private BigDecimal totalFee = BigDecimal.ZERO;
}
