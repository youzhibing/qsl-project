package com.qsl.dbf;

import com.linuxense.javadbf.DBFException;
import com.linuxense.javadbf.DBFField;
import com.linuxense.javadbf.DBFReader;
import com.linuxense.javadbf.DBFUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class JavaDBFReaderTest {

	public static void main(String args[]) {

		DBFReader reader = null;
		try {

			// create a DBFReader object
			FileInputStream fis = new FileInputStream(new File("D:\\恒生聚源\\IDG 数据迁移\\第十一批迁移\\测试不一致文件再生成\\dbf\\目录2\\方正富邦基金恒生估值含央行报送\\20221109\\tb_trusteeship_20221109.dbf"));
			reader = new DBFReader(fis);

			// get the field count if you want for some reasons like the following

			int numberOfFields = reader.getFieldCount();

			// use this count to fetch all field information
			// if required

			for (int i = 0; i < numberOfFields; i++) {

				DBFField field = reader.getField(i);

				// do something with it if you want
				// refer the JavaDoc API reference for more details
				//
				System.out.println(field.getName());
			}

			// Now, lets us start reading the rows

			Object[] rowObjects;

			while ((rowObjects = reader.nextRecord()) != null) {

				for (int i = 0; i < rowObjects.length; i++) {
					System.out.println(rowObjects[i]);
				}
			}

			// By now, we have iterated through all of the rows

		} catch (DBFException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			DBFUtils.close(reader);
		}
	}
}