package com.qsl.file;

import java.io.File;
import java.io.RandomAccessFile;
import java.math.BigDecimal;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/2/14 16:49
 **/
public class RandomAccessFileTest {

    public static void main(String[] args) throws Exception {
        String filePath = "D:/unicode.txt";
        RandomAccessFile raf = new RandomAccessFile(new File(filePath), "rw");
        // 文件分隔符 \u2028,记录分隔符 \u2029，组分隔符 \u2030
        String str = unicodeToString("\\ua") + unicodeToString("\\u1C") + unicodeToString("\\u1D") + unicodeToString("\\u1E") + unicodeToString("\\u1F") + unicodeToString("\\u21\\u5e");
        raf.write(str.getBytes("utf-8"));
        BigDecimal num = new BigDecimal("123.12000");
        raf.close();
        System.out.println((char) 30);
    }

    public static String unicodeToString(String unicode) {
        if (unicode == null) return null;
        StringBuilder str = new StringBuilder();
        String[] hexs = unicode.split("\\\\u");
        for (String hex : hexs) {
            if (hex != null && hex.length() > 0) {
                str.append((char) Integer.parseInt(hex, 16));
            }
        }
        return str.toString();
    }
}
