package com.qsl.file;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.regex.Pattern;

/**
 * @Description: 读取工程源码
 * @Author: 博客园@青石路
 * @CreateDate: 2022/7/1 8:58
 **/
public class ReadProjectSource {
	private static Pattern pattern = Pattern.compile("/\\*.+?\\*/", Pattern.DOTALL);

	public static void main(String[] args) throws IOException {
		Collection<File> files = FileUtils.listFiles(new File("E:\\IDG\\ift-parent\\ift-server"),
				new String[] { "java" }, true);
		File f = new File("E:\\temp\\源码.txt");
		FileWriter fileWritter = new FileWriter(f.getAbsoluteFile(), true);	
		for (File file : files) {
			String filePath = file.getAbsolutePath();
			if ((!filePath.contains("org\\apache")) && (!filePath.contains("src\\test\\"))) {
				String str = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
				str.replaceAll("//.+\\r\\n", "");
				str = pattern.matcher(str).replaceAll("");
				fileWritter.write(str);
				fileWritter.flush();
			}
		}
		fileWritter.close();
	}
}