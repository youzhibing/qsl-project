package com.qsl.file;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * @Description: 文件test
 * @Author: 博客园@青石路
 * @CreateDate: 2022/8/24 15:19
 **/
public class FileTest {

    public static void main(String[] args) throws Exception {
        File file = new File("D:\\hello.txt");
        System.out.println(file.getName());
        System.out.println(file.getPath());
        // Files.move(file.toPath(), new File("D:\\qsl.txt").toPath(), StandardCopyOption.REPLACE_EXISTING);
        Files.copy(Paths.get("D:\\qsl.txt"), new File("D:\\hello.txt").toPath(),StandardCopyOption.COPY_ATTRIBUTES , StandardCopyOption.REPLACE_EXISTING);

        /*System.out.println(file.lastModified());
        Instant instant = Instant.ofEpochMilli(file.lastModified())
                .atZone(ZoneId.systemDefault())
                .toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant();
        System.out.println(instant.toEpochMilli());*/
    }

    public static void test () throws Exception {
        File file = new File("D:\\qsl.txt");
        FileInputStream fis = new FileInputStream(file);
        String md5 = DigestUtils.md5Hex(fis);
        fis.close();

        File copyFile = new File("D:\\qsl_2.txt");
        FileUtils.copyFile(file, copyFile);

        FileInputStream cp = new FileInputStream(copyFile);
        String md51 = DigestUtils.md5Hex(cp);
        cp.close();

        System.out.println(md5);
        System.out.println(md51);

        System.out.println(copyFile.getPath());
        System.out.println(copyFile.getAbsolutePath());
    }
}
