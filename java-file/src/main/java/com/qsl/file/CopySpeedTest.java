package com.qsl.file;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/9/6 15:13
 **/
public class CopySpeedTest {

    private static File src = new File("D:\\big_file.zip");
    private static File dest = new File("E:\\copy\\2022\\09\\06\\9861\\big_file_copy.zip");

    public static void main(String[] args) throws Exception {
        File parentFile = dest.getParentFile();
        System.out.println(parentFile.getPath() + " 是否存在：" + parentFile.exists());
        /*if (!parentFile.exists()) {
            Files.createDirectories(Paths.get(parentFile.getPath()));
        }*/
        //Files.createDirectories(Paths.get(parentFile.getPath()));
        /*boolean newFile = dest.createNewFile();
        System.out.println(newFile);*/
        jdkCopy();
        // fileUtilsCopy();
        // ioUtilsCopy();
    }

    public static void jdkCopy() throws Exception {
        long startTime = System.currentTimeMillis();
        Files.copy(src.toPath(), dest.toPath(), StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        System.out.println(System.currentTimeMillis() - startTime);
    }

    public static void fileUtilsCopy() throws Exception {
        long startTime = System.currentTimeMillis();
        FileUtils.copyFile(src, dest);
        System.out.println(System.currentTimeMillis() - startTime);
    }

    public static void ioUtilsCopy() throws Exception {
        long startTime = System.currentTimeMillis();
        try(FileInputStream fis = new FileInputStream(src);
            FileOutputStream fos = new FileOutputStream(dest)
        ) {
            IOUtils.copy(fis, fos);
        }
        System.out.println(System.currentTimeMillis() - startTime);
    }
}
