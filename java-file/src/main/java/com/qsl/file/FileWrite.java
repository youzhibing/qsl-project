package com.qsl.file;

import java.io.File;
import java.io.FileWriter;
import java.util.concurrent.TimeUnit;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/2/7 9:08
 **/
public class FileWrite {

    public static void main(String[] args) throws Exception {
        // writeTxt();
        createFile();
    }

    public static void writeTxt() throws Exception {
        File file = new File("E:\\home\\share_yzb\\UserData\\Rsync\\20230207\\外部资源是否重复上传_20230207.txt");
        FileWriter fw = new FileWriter(file, true);
        for (int i=0;;i++) {
            fw.write("hello" + "\t\n");
            TimeUnit.MILLISECONDS.sleep(1);
            fw.flush();
            if (i<0) {
                break;
            }
        }
        fw.close();
    }

    public static void createFile() throws Exception {
        for (int i=1; ; i++) {
            File file = new File("E:\\home\\share_yzb\\UserData\\Rsync\\20230207\\" + i + ".txt");
            file.createNewFile();
            FileWriter fw = new FileWriter(file);
            fw.write(i + "");
            fw.flush();
            fw.close();
            TimeUnit.MILLISECONDS.sleep(100);
        }
    }
}
