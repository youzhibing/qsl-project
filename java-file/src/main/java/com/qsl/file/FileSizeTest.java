package com.qsl.file;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/9/13 9:47
 **/
public class FileSizeTest {

    public static void main(String[] args) throws Exception {
        File file = new File("E:\\randomAccess.txt");
        System.out.println("file length = " + file.length());

        FileInputStream fis = new FileInputStream(file);
        System.out.println("channel size = " + fis.getChannel().size());
        // fis.available() 返回的是int类型
        System.out.println("available = " + fis.available());
        fis.close();

        System.out.println("Files size = " + Files.size(file.toPath()));
    }
}
