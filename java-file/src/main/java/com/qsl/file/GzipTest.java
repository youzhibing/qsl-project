package com.qsl.file;

import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipParameters;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/2/2 16:11
 **/
public class GzipTest {

    public static void main(String[] args) throws Exception {
        jdkGzip();
    }

    public static void compressGz() {
        // gzip压缩
        String file = "D:\\test.js";
        GzipParameters parameters = new GzipParameters();
        parameters.setFilename(FilenameUtils.getName(file));
        //parameters.setComment("Test file");
        //parameters.setModificationTime(System.currentTimeMillis());
        try (
                FileOutputStream fos = new FileOutputStream("D:\\test" + ".gz");
                GzipCompressorOutputStream gzos = new GzipCompressorOutputStream(fos, parameters);
                InputStream is = new FileInputStream(file)) {
            IOUtils.copy(is, gzos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void jdkGzip() {
        try (
                FileOutputStream fos = new FileOutputStream("D:\\test.js.gz");
                GZIPOutputStream gos = new GZIPOutputStream(fos);
                InputStream is = new FileInputStream("D:\\test.js");
        ){
            org.apache.commons.io.IOUtils.copy(is, gos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void jdkZip() {

    }
}
