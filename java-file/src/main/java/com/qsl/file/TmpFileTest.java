package com.qsl.file;

import java.io.File;
import java.io.IOException;

/**
 * @Description:
 * @Author: 青石路
 * @CreateDate: 2023/7/10 13:27
 **/
public class TmpFileTest {
    public static void main(String[] args) throws IOException {
        File tmp = File.createTempFile("tmp", ".zip");
        System.out.println(tmp.getAbsolutePath());
        File file = new File(System.getProperty("java.io.tmpdir") + File.separator + "test.txt");
        System.out.println(file.getAbsolutePath());
    }
}
