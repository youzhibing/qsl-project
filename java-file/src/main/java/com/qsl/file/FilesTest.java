package com.qsl.file;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/12/5 16:07
 **/
public class FilesTest {

    public static void main(String[] args) throws Exception {
        File copyFile = new File("E:\\hello.txt.20240409133722");
        File file = new File("D:\\hello.txt");
        /*Path path = Paths.get("D:\\hello", "qsl.txt");
        System.out.println(Files.size(path));
        System.out.println("path = " + path);
        System.out.println("path.toString = " + path.toString());
        System.out.println(path.getFileName().toString());
        System.out.println(path.toFile().getAbsolutePath());*/
        // FileUtils.copyFile(file, copyFile);
        Files.copy(file.toPath(), copyFile.toPath(), StandardCopyOption.COPY_ATTRIBUTES);
        String parentFullPath = file.getParent();
        System.out.println(parentFullPath);
        System.out.println(parentFullPath.substring(parentFullPath.lastIndexOf(File.separator)) + 1);
    }
}
