package com.qsl.file;

import java.io.File;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/1/30 9:42
 **/
public class FileRenameTest {

    public static void main(String[] args) {
        File file = new File("C:\\Users\\youzb\\Desktop\\新建文件夹\\未重命名\\HRZC_STOCK_20230131.json.gz");
        File renameFile = new File("C:\\Users\\youzb\\Desktop\\新建文件夹\\未重命名\\HRZC_STOCK_20230131.gz.20230202");
        file.renameTo(renameFile);
        System.out.println(file.getPath());
        System.out.println(renameFile.getPath());
    }
}
