package com.qsl.file;

import java.io.RandomAccessFile;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/9/9 9:22
 **/
public class FileNulTest {

    public static void main(String[] args) throws Exception {
        RandomAccessFile file = new RandomAccessFile("D:\\randomAccess.txt", "rw");
        long len = file.length();
        long start = file.getFilePointer();
        long nextEnd = start + len - 1;
        // 移动指针
        file.seek(nextEnd);
        byte[] buf = new byte[1];
        int read = file.read(buf, 0, 1);
        System.out.println(buf[0]);
        if (buf[0] == 0) {
            System.out.println("NUL 结尾");
        }
    }
}
