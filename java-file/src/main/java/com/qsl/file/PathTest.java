package com.qsl.file;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;

/**
 * @description:
 * @author: 青石路
 * @createDate: 2023/11/30 9:53
 **/
public class PathTest {

    public static void main(String[] args) throws Exception {
        Path path = Paths.get("D:/notText123");
        System.out.println(path.resolve("123.txt"));
        System.out.println(path.toString());
        File file = new File("D:/notText/123.txt.zip");
        System.out.println(file.getName().substring(file.getName().lastIndexOf(".")));
        try (DirectoryStream<Path> paths = Files.newDirectoryStream(path)) {
            if (!path.iterator().hasNext()) {
                Files.delete(path);
            }
        }
        /*List<Path> dirAllFilePaths = null;
        try (Stream<Path> filePathStream = Files.list(Paths.get("E:\\home\\share_yzb\\Rsync\\20250107etf"))) {
            dirAllFilePaths = filePathStream.filter(e -> !Files.isDirectory(e)).sorted(Comparator.comparing(Path::getFileName)).collect(Collectors.toList());
        }
        System.out.println(dirAllFilePaths.get(dirAllFilePaths.size()-1).getFileName());
        Map<String, Path> localPathMap = Maps.newLinkedHashMap();
        for (Path p1 : dirAllFilePaths) {
            localPathMap.put(p1.getFileName().toString(), p1);
        }
        String nameCandidate = dirAllFilePaths.get(dirAllFilePaths.size() - 1).getFileName().toString();
        String compressFileName = getCompressFileName(localPathMap, "", nameCandidate);
        System.out.println(compressFileName);*/
    }


    public static void upload (Path path) {
        path = Paths.get("D:/notText");
    }

    private static String getCompressFileName(Map<String, Path> localPathMap, String fileRename, String nameCandidate) {
        if (localPathMap.size() > 1) {
            if (StringUtils.isNotBlank(fileRename) && localPathMap.containsKey(fileRename)) {
                return fileRename + ".zip";
            }
            return nameCandidate + ".zip";
        }
        Optional<String> fileNameOpt = localPathMap.keySet().stream().findFirst();
        if (fileNameOpt.isPresent()) {
            return fileNameOpt.get() + ".zip";
        }
        throw new RuntimeException("本地目录下没有文件，请确认文件复制是否正常完成");
    }
}
