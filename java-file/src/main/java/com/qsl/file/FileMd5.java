package com.qsl.file;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2023/2/18 13:17
 **/
public class FileMd5 {

    public static void main(String[] args) throws IOException {
        // 31659c761de05a060f6e154d9021e65e
        File file = new File("D:\\qsl.txt");
        try(InputStream is = new FileInputStream(file)) {
            String md5 = DigestUtils.md5Hex(is);
            System.out.println(md5);
        }
        System.out.println(file.lastModified());
    }
}
