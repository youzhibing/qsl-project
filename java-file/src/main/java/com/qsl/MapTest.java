package com.qsl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/8/22 10:32
 **/
public class MapTest {

    private static final Map<String, String> FILE_TYPE_MAP;
    static {
        FILE_TYPE_MAP = new ConcurrentHashMap<>();
        FILE_TYPE_MAP.put("DEL", "del");
        FILE_TYPE_MAP.put("TXT", "txt");
        FILE_TYPE_MAP.put("CSV", "csv");
        FILE_TYPE_MAP.put("OUT", "out");
        FILE_TYPE_MAP.put("SCHEMA", "schema");
        FILE_TYPE_MAP.put("DAT", "dat");
    }

    public static void main(String[] args) {
        boolean dat = FILE_TYPE_MAP.containsKey("dat");
        System.out.println(dat);
    }
}