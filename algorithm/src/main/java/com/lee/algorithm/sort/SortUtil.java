package com.lee.algorithm.sort;

/***
 * @description: 排序工具类
 * @author : 青石路
 * @date: 2021/11/22 11:16
 */
public class SortUtil {

    /**
     * 元素交换
     * @param arr
     * @param i
     * @param j
     */
    public static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    /**
     * 打印
     * @param arr
     */
    public static void print(int[] arr) {
        for (int cur : arr) {
            System.out.print(cur + " ");
        }
        System.out.println();
    }
}
