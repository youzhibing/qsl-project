package com.lee.algorithm.sort;

/***
 * @description: 选择排序
 * @author : 青石路
 * @date: 2021/11/22 8:39
 */
public class SelectSort {

    public static void main(String[] args) {
        int[] arr = new int[]{1,0,4,3,2,9,7};
        selectSort(arr);
        SortUtil.print(arr);
    }

    /**
     * 选择排序
     *      从 arr[i] 至 arr[n] 中找出最小的元素与 arr[i] 进行交换
     * @param arr
     */
    public static void selectSort(int[] arr) {

        if (arr == null || arr.length < 2) {
            return;
        }

        for (int i=0; i<arr.length-1; i++) {          // 0 ~ i-1 已经有序，处理 i ~ n-1
            int minIndex = i;
            for (int j=i+1; j<arr.length; j++) {        // 从 i ~ n-1 中找出最小元素的下标
                minIndex = arr[minIndex] > arr[j] ? j : minIndex;
            }
            SortUtil.swap(arr, i, minIndex);
        }
    }
}
