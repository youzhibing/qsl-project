package com.lee.algorithm.tree;

/***
 * @description: 折纸问题
 *      请把纸条竖着放在桌⼦上，然后从纸条的下边向上方对折，压出折痕后再展开
 *      此时有1条折痕，突起的方向指向纸条的背⾯，这条折痕叫做“凹”折痕 ；突起的方向指向纸条正⾯的折痕叫做“凸”折痕
 *      如果每次都从下边向上方对折，对折 N 次，请从上到下计算出所有折痕的方向
 *
 *      满二叉树的中序遍历
 * @author 博客园 @ 青石路
 * @date: 2021/12/11 14:55
 */
public class PaperFolding {

    public static void main(String[] args) {
        printAllFolds(3);
    }

    /**
     * 打印所有的折痕方向
     * @param n
     * @author 博客园 @ 青石路
     */
    public static void printAllFolds(int n) {
        printLevelFolds(1, n, "凹 ");
        System.out.println();
    }

    /**
     * 打印第 level 层的折痕
     * @param level 当前处理的层数
     * @param n 总共的层数
     * @param direction
     * @author 博客园 @ 青石路
     */
    private static void printLevelFolds(int level, int n, String direction) {
        if (level > n) {
            return;
        }
        printLevelFolds(level + 1, n, "凹 ");
        System.out.print(direction);
        printLevelFolds(level + 1, n, "凸 ");
    }
}
