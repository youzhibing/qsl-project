package com.lee.algorithm.tree.struct;

/***
 * @description: 二叉树节点（包含父节点的引用）
 *      根节点的 parent = null
 * @author : 青石路
 * @date: 2021/12/4 15:19
 */
public class TreeParentNode<V> {

    public V value;
    public TreeParentNode left;
    public TreeParentNode right;
    public TreeParentNode parent;

    public TreeParentNode(V value) {
        this.value = value;
    }
}
