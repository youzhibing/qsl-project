package com.lee.algorithm.tree.struct;

/***
 * @description: 二叉树节点
 * @author 博客园 @ 青石路
 * @date: 2021/12/4 15:19
 */
public class TreeNode<V> {

    public V value;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(V value) {
        this.value = value;
    }
}
