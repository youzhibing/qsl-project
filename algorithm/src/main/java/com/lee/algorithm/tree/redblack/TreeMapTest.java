package com.lee.algorithm.tree.redblack;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.TreeMap;

/***
 * @description TreeMap 底层就是红黑树 + 单向链表
 * @author 青石路
 * @date 2022/3/8 14:19
 */
public class TreeMapTest {

    public static void main(String[] args) {
        TreeMap<Integer, Integer> map = new TreeMap<>();
        map.put(5,5);
        map.put(10,10);
        map.put(20,20);
        map.put(3,3);
        map.remove(3);
    }
}
