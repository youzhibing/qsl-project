package com.lee.algorithm.linkedlist.struct;

/***
 * @description: 单向节点
 * @author : 博客园 @ 青石路
 * @date: 2021/11/29 21:20
 */
public class OneWayNode<V> {

    public V value;
    public OneWayNode next;

    public OneWayNode(V value) {
        this.value = value;
    }
}
