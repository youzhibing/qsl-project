package com.lee.algorithm.linkedlist.struct;

/***
 * @description: 双向链表节点
 * @author : 青石路
 * @date: 2021/11/29 21:21
 */
public class TwoWayNode<V> {

    public V value;
    public TwoWayNode pre;
    public TwoWayNode next;
}
