package com.lee.algorithm.linkedlist.struct;

/***
 * @description: 含随机指针的单项链表
 * @author : 青石路
 * @date: 2021/11/29 22:35
 */
public class OneWayRandNode<V> {

    public V value;
    public OneWayRandNode next;
    public OneWayRandNode rand;

    public OneWayRandNode(V value) {
        this.value = value;
    }
}
