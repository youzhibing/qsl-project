package com.lee.algorithm.array;

/**
 * @Description: 堆 test
 * @Author: 青石路
 * @CreateDate: 2022/4/10 21:46
 **/
public class HeapTest {

    public static void main(String[] args) {
        Heap heap = new Heap();
        heap.buildHeap(new Integer[]{2,3,1,4,8,9});
        System.out.println(heap.peek());
    }
}
