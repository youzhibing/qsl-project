package com.lee.algorithm;

import java.math.BigDecimal;

/***
 * @description: 引用传递 & 值传递 测试
 *      基本数据类型
 *      基本类型对应的包装类型、BigDecimal 等，看源码，底层的数据存储是否有 final 修饰
 *      对象类型
 * @author : 青石路
 * @date: 2021/12/25 21:02
 */
public class ReferenceAndValueTest {


    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("123");
        strAppend(sb);
        System.out.println(sb);

        BigDecimal bg = new BigDecimal("0");
        bigDecimalChange(bg);
        System.out.println(bg);

        System.out.println("".equals(new StringBuilder().toString()));
    }

    private static void strAppend(StringBuilder sb) {
        sb.append("aaa");
    }

    private static void bigDecimalChange(BigDecimal bg) {
        bg = bg.add(new BigDecimal("12"));
        System.out.println(bg);
    }
}
