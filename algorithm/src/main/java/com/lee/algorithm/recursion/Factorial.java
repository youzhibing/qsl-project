package com.lee.algorithm.recursion;

/***
 * @description 阶乘
 * @author 青石路
 * @date 2022/1/7 9:10
 */
public class Factorial {

    /**
     * 暴力递归
     * @param n
     * @return
     */
    public static long recur(int n) {
        if (n < 1) {
            return 0;
        }
        if (n <= 2) {
            return n;
        }
        return n * recur(n-1);
    }

    /**
     * 递推
     * @param n
     * @return
     */
    public static long iterate(int n) {
        long res = 1;
        for (int i=1; i<=n; i++) {
            res *= i;
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(recur(10));
        System.out.println(iterate(10));
    }
}
