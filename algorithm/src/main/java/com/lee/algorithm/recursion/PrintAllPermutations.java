package com.lee.algorithm.recursion;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/***
 * @description: 打印一个字符串的全部排列
 * @author : 青石路
 * @date: 2021/12/24 21:46
 */
public class PrintAllPermutations {

    public static void main(String[] args) {
        ArrayList<String> res = permutation("sbca");
        res.stream().forEach(e -> System.out.println(e));
        System.out.println();

        Set<String> set = new HashSet<>();
        set.addAll(res);
        System.out.println(set.size() == res.size());
    }

    public static ArrayList<String> permutation(String str) {
        ArrayList<String> res = new ArrayList<>();      // 所有的排列结果
        if (str == null || str.length() == 0) {
            return res;
        }
        char[] chs = str.toCharArray();
        process(chs, 0, res);
        // res.sort(null);
        return res;
    }

    /**
     * chs[index..] 范围上，所有的字符都可以在 index 位置上，后续都去尝试
     * chs[0...index-1]范围上，是之前做的选择
     * 找出所有的排列组合，放入到 res 中
     * @param chs
     * @param index
     * @param res
     */
    public static void process(char[] chs, int index, ArrayList<String> res) {
        if (index == chs.length) {
            res.add(String.valueOf(chs));
        }
        boolean[] visit = new boolean[26];
        // chs[index..] 范围上，所有的字符都可以在 index 位置上，后续都去尝试
        for (int j = index; j<chs.length; j++) {
            // 去重处理（分支限界）
            if (!visit[chs[j] - 'a']) {
                visit[chs[j] - 'a'] = true;
                swap(chs, index, j);
                process(chs, index+1, res);
                swap(chs, index, j);
            }
        }
    }

    public static void swap(char[] chs, int i, int j) {
        char tmp = chs[i];
        chs[i] = chs[j];
        chs[j] = tmp;
    }

}
