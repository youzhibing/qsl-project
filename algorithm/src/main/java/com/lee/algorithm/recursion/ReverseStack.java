package com.lee.algorithm.recursion;

import java.util.Stack;

/***
 * @description: 对栈元素进行逆序（不用额外数据结构）
 * @author : 青石路
 * @date: 2021/12/24 20:43
 */
public class ReverseStack {

    public static void reverse(Stack<Object> stack) {
        if (stack.isEmpty()) {
            return;
        }
        Object last = getLast(stack);
        reverse(stack);
        stack.push(last);
    }

    /**
     * 返回栈底元素，其他元素保持顺序不变往下落
     * @param stack
     * @return
     */
    public static Object getLast(Stack<Object> stack) {
        Object result = stack.pop();
        if (stack.isEmpty()) {
            return result;
        } else {
            Object last = getLast(stack);
            stack.push(result);
            return last;
        }
    }
}
