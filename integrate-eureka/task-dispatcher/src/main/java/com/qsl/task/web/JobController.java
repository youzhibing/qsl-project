package com.qsl.task.web;

import com.qsl.task.service.TaskExecutorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("job")
public class JobController {

    @Resource
    private TaskExecutorService taskExecutorService;

    private static final Logger LOGGER = LoggerFactory.getLogger(JobController.class);

    @GetMapping("dispatch")
    public String dispatch(Long jobId) {
        LOGGER.info("收到作业[jobId={}]执行请求", jobId);
        LOGGER.info("作业[jobId={}]拆分任务中...", jobId);
        List<Long> taskIds = Arrays.asList(123L, 666L, 888L, 999L);
        LOGGER.info("作业[jobId={}]拆分完成，得到作业列表[{}]", jobId, taskIds);
        for (Long taskId : taskIds) {
            String execResult = taskExecutorService.exec(taskId);
            LOGGER.info("任务[{}]执行结果：{}", taskId, execResult);
        }
        return "success";
    }
}
