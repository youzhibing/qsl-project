package com.qsl.task.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("TASK-EXECUTOR")
public interface TaskExecutorService {

    @GetMapping("task/exec")
    String exec(@RequestParam("taskId") Long taskId);
}