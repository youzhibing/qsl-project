package com.qsl;

import com.google.common.util.concurrent.RateLimiter;

/**
 * @author 青石路
 * @date 2021/4/12 18:01
 */
public class RateLimiterTest {

    public static void main(String[] args) {
        RateLimiter r = RateLimiter.create(5);     // 1 秒产生 5 个令牌

        while (true) {
            System.out.println("get 1 tokens: " + r.acquire(5) + "s");
            System.out.println("get 1 tokens: " + r.acquire(1) + "s");
            System.out.println("get 1 tokens: " + r.acquire(1) + "s");
            System.out.println("get 1 tokens: " + r.acquire(1) + "s");
            System.out.println("end");

        }
        // System.out.println(r.getRate());
    }
}
