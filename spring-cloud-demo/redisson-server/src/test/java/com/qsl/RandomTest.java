package com.qsl;

import java.security.SecureRandom;

/**
 * @author 青石路
 * @date 2021/4/12 16:19
 */
public class RandomTest {

    public static void main(String[] args) {
        SecureRandom secureRandom = new SecureRandom();

        for (int j=1; j<=20; j++) {

            int firstCount = 0;
            int secondCount = 0;
            int thirdCount = 0;
            int otherCount = 0;
            for (int i = 1; i <= 100000; i++) {
                int nextInt = secureRandom.nextInt(1000);   // 产生 0 ~ 999 的随机整数
                if (nextInt <= 0) {
                    firstCount++;
                } else if (nextInt > 0 && nextInt <= 10) {
                    secondCount++;
                } else if (nextInt > 10 && nextInt <= 30) {
                    thirdCount++;
                } else {
                    otherCount++;
                }
            }

            System.out.println("firstCount = " + firstCount + "; secondCount = " + secondCount + "; thirdCount = " + thirdCount + "; otherCount = " + otherCount);
        }
    }

}
