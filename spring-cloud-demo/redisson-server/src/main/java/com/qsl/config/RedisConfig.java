package com.qsl.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * @author 青石路
 * @date 2021/4/11 21:38
 */
@Configuration
public class RedisConfig {

    @Value("${redis.address}")
    private String redisAddress;

    @Bean(destroyMethod="shutdown")
    @ConditionalOnMissingBean
    public RedissonClient redissonClient() throws IOException {
        Config config = new Config();
        config.useSingleServer()
                .setAddress(redisAddress);
        return Redisson.create(config);
    }
}
