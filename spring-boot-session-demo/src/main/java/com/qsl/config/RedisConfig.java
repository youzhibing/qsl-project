package com.qsl.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;

@Configuration
public class RedisConfig {

	@Autowired
	private RedisTemplate redisTemplate;

	@Bean({"redisOperationsSessionRepository", "sessionRepository"})
	public RedisOperationsSessionRepository redisOperationsSessionRepository() {
		return new RedisOperationsSessionRepository(redisTemplate);
	}
}
