package com.qsl.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("hello")
public class HelloController {
    @GetMapping("/set")
    public String set(HttpSession session) {
        session.setAttribute("user", "qsl");
        return "qsl";
    }
    @GetMapping("/get")
    public String get(HttpSession session) {
        return session.getAttribute("user") + "";
    }
}