package com.qsl.task;

import com.qsl.service.AdaptorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author 青石路
 * @date 2021/4/20 16:58
 */
@Component
@EnableScheduling
@Slf4j
public class HelloTask {

    @Resource
    private AdaptorService adaptorService;

    @Scheduled(cron = "*/1 * * * * ?")
    public void task() {
        log.info("定时任务开始...");
        String msg = adaptorService.task();
        log.info("msg = {}", msg);
    }
}
