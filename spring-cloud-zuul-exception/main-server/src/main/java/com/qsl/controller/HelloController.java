package com.qsl.controller;

import com.qsl.service.AdaptorService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 青石路
 * @date 2021/4/20 16:35
 */
@RestController
@RequestMapping("hello")
public class HelloController {

    @Resource
    private AdaptorService adaptorService;

    @RequestMapping("hi")
    public String hi(String name) {
        return adaptorService.hi(name);
    }
}
