package com.qsl.controller;

import com.qsl.service.IHelloService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 青石路
 * @date 2021/4/20 16:46
 */
@RestController
@RequestMapping("hello")
public class HelloController {

    @Resource
    private IHelloService helloService;

    @RequestMapping("hi")
    public String hi(String name) {
        return helloService.hi(name);
    }


    @RequestMapping("task")
    public String task() {
        return helloService.task();
    }
}
