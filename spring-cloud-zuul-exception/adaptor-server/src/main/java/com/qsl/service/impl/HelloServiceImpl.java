package com.qsl.service.impl;

import com.qsl.service.IHelloService;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author 青石路
 * @date 2021/4/20 16:44
 */
@Service
public class HelloServiceImpl implements IHelloService {

    @Override
    public String hi(String name) {
        return "hi, " + name;
    }

    @Override
    public String task() {

        try {
            // 模拟业务执行，需要 20 秒
            TimeUnit.SECONDS.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "success";
    }
}
