package com.qsl.service;

/**
 * @author 青石路
 * @date 2021/4/20 16:44
 */
public interface IHelloService {

    String hi(String name);

    String task();
}
