package com.qsl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author 青石路
 * @date 2021/4/16 10:22
 */
@SpringBootApplication
@EnableEurekaClient
public class AdaptorServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdaptorServerApplication.class, args);
    }
}
