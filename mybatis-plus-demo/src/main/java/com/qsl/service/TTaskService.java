package com.qsl.service;

import com.qsl.entity.TTask;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 任务信息 服务类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-08-21
 */
public interface TTaskService extends IService<TTask> {

}
