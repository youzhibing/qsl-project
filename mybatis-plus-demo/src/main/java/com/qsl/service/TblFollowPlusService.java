package com.qsl.service;

import com.qsl.entity.TblFollowPlus;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 关注关系 服务类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-06-03
 */
public interface TblFollowPlusService extends IService<TblFollowPlus> {

    /**
     * 关注
     * @param followId 关注者
     * @param userId 被关注者
     */
    void follow(int followId, int userId);

}
