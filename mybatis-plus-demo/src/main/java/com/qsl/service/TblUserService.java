package com.qsl.service;

import com.qsl.entity.TblUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-05-07
 */
public interface TblUserService extends IService<TblUser> {

}
