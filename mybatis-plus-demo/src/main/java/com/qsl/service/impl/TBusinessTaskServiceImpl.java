package com.qsl.service.impl;

import com.qsl.entity.TBusinessTask;
import com.qsl.mapper.TBusinessTaskMapper;
import com.qsl.service.TBusinessTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 业务任务关系 服务实现类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-08-21
 */
@Service
public class TBusinessTaskServiceImpl extends ServiceImpl<TBusinessTaskMapper, TBusinessTask> implements TBusinessTaskService {

}
