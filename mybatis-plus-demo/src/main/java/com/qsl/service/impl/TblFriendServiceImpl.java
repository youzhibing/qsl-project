package com.qsl.service.impl;

import com.qsl.entity.TblFriend;
import com.qsl.mapper.TblFriendMapper;
import com.qsl.service.TblFriendService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 朋友关系 服务实现类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-06-03
 */
@Service
public class TblFriendServiceImpl extends ServiceImpl<TblFriendMapper, TblFriend> implements TblFriendService {

}
