package com.qsl.service.impl;

import com.qsl.entity.TTaskExecLog;
import com.qsl.mapper.TTaskExecLogMapper;
import com.qsl.service.TTaskExecLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 任务执行日志 服务实现类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-08-21
 */
@Service
public class TTaskExecLogServiceImpl extends ServiceImpl<TTaskExecLogMapper, TTaskExecLog> implements TTaskExecLogService {

}
