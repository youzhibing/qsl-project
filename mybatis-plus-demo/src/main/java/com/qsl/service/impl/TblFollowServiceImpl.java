package com.qsl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qsl.entity.TblFollow;
import com.qsl.entity.TblFriend;
import com.qsl.mapper.TblFollowMapper;
import com.qsl.service.TblFollowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qsl.service.TblFriendService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 关注关系 服务实现类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-06-03
 */
@Service
public class TblFollowServiceImpl extends ServiceImpl<TblFollowMapper, TblFollow> implements TblFollowService {

    @Resource
    private TblFriendService friendService;

    /**
     * 关注
     * @param followId 关注者id
     * @param userId 被关注者id
     * @author 博客园@青石路
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void follow(int followId, int userId) {
        // 1、查 userId 是否关注了 followId
        LambdaQueryWrapper<TblFollow> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(TblFollow::getUserId, followId);
        queryWrapper.eq(TblFollow::getFollowerId, userId);
        int count = this.count(queryWrapper);
        System.out.println("count = " + count);

        try {
            // 睡 2 秒，便于并发问题复现
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (count >= 1) {
            // userId 关注了 followId，则成为朋友
            TblFriend friend = new TblFriend();
            friend.setOneSideId(followId).setOtherSideId(userId);
            friendService.save(friend);
        } else {
            // user 未关注 followId
            TblFollow follow = new TblFollow();
            follow.setFollowerId(followId);
            follow.setUserId(userId);
            this.save(follow);
        }
    }

    /**
     * 先插再查
     *      注意 加@Transactional 与不加 @Transactional 的执行区别
     *      加 @Transactional，就需要看具体的数据了（insert语句的锁）
     * @param followId
     * @param userId
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void follow1(int followId, int userId) {

        // 1、先插
        TblFollow follow = new TblFollow();
        follow.setFollowerId(followId);
        follow.setUserId(userId);
        this.save(follow);

        try {
            // 睡 2 秒，便于并发问题复现
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 2、再查
        LambdaQueryWrapper<TblFollow> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(TblFollow::getUserId, followId);
        queryWrapper.eq(TblFollow::getFollowerId, userId);
        int count = this.count(queryWrapper);
        System.out.println("count = " + count);

        if (count >= 1) {
            TblFriend friend = new TblFriend();
            friend.setOneSideId(followId).setOtherSideId(userId);
            friendService.save(friend);
        }
    }
}
