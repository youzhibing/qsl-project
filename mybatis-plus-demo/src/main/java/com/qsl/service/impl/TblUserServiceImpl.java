package com.qsl.service.impl;

import com.qsl.entity.TblUser;
import com.qsl.mapper.TblUserMapper;
import com.qsl.service.TblUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-05-07
 */
@Service
public class TblUserServiceImpl extends ServiceImpl<TblUserMapper, TblUser> implements TblUserService {

}
