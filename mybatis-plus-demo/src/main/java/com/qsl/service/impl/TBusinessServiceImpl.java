package com.qsl.service.impl;

import com.qsl.entity.TBusiness;
import com.qsl.mapper.TBusinessMapper;
import com.qsl.service.TBusinessService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 业务信息 服务实现类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-08-21
 */
@Service
public class TBusinessServiceImpl extends ServiceImpl<TBusinessMapper, TBusiness> implements TBusinessService {

}
