package com.qsl.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qsl.entity.TblFollowPlus;
import com.qsl.enums.FollowPlusRelationShipEnum;
import com.qsl.mapper.TblFollowPlusMapper;
import com.qsl.mapper.TblFriendMapper;
import com.qsl.service.TblFollowPlusService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 关注关系 服务实现类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-06-03
 */
@Service
public class TblFollowPlusServiceImpl extends ServiceImpl<TblFollowPlusMapper, TblFollowPlus> implements TblFollowPlusService {

    @Resource
    private TblFriendMapper friendMapper;

    /**
     * 关注
     *      不能自己关注自己，所以 followId != userId
     * @param followId 关注者id
     * @param userId 被关注者id
     * @author 博客园@青石路
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void follow(int followId, int userId) {
        if (followId == userId) {
            System.out.println("不能自己关注自己");
            return;
        }
        if (followId < userId) {
            // one_side_id < other_side_id
            // 1 insert into tbl_follow_plus
            this.baseMapper.saveFollow(followId, userId, FollowPlusRelationShipEnum.ONE.getValue());

            try {
                // 睡 2 秒，便于并发问题复现
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // 2 查询 relation_ship
            TblFollowPlus followPlus = getRelationShip(followId, userId);
            System.out.println("relationShip = " + followPlus.getRelationShip());
            // 3、判断 relation_ship 的值
            if (followPlus.getRelationShip() == FollowPlusRelationShipEnum.ONE.getValue()) {
                // relation_ship = 1
                return;
            }
            if (followPlus.getRelationShip() == FollowPlusRelationShipEnum.THREE.getValue()) {
                // 4、relation_ship = 3
                friendMapper.saveFriend(followId, userId);
            }
        }

        if (followId > userId) {
            // one_side_id > other_side_id
            // 1 insert into tbl_follow_plus
            this.baseMapper.saveFollow(userId, followId, FollowPlusRelationShipEnum.TWO.getValue());
            try {
                // 睡 2 秒，便于并发问题复现
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 2 查询 relation_ship
            TblFollowPlus followPlus = getRelationShip(userId, followId);
            System.out.println("relationShip = " + followPlus.getRelationShip());
            // 3、判断 relation_ship 的值
            if (followPlus.getRelationShip() == FollowPlusRelationShipEnum.TWO.getValue()) {
                // relation_ship = 2
                return;
            }
            if (followPlus.getRelationShip() == FollowPlusRelationShipEnum.THREE.getValue()) {
                // 4、relation_ship = 3
                friendMapper.saveFriend(userId, followId);
            }
        }
    }

    /**
     * 查询 relation_ship
     * @param oneSideId
     * @param otherSideId
     * @author 博客园@青石路
     * @return
     */
    private TblFollowPlus getRelationShip(int oneSideId, int otherSideId) {
        LambdaQueryWrapper<TblFollowPlus> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TblFollowPlus::getOneSideId, oneSideId);
        queryWrapper.eq(TblFollowPlus::getOtherSideId, otherSideId);
        return this.getOne(queryWrapper);
    }
}
