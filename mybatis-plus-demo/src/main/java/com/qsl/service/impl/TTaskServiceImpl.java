package com.qsl.service.impl;

import com.qsl.entity.TTask;
import com.qsl.mapper.TTaskMapper;
import com.qsl.service.TTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 任务信息 服务实现类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-08-21
 */
@Service
public class TTaskServiceImpl extends ServiceImpl<TTaskMapper, TTask> implements TTaskService {

}
