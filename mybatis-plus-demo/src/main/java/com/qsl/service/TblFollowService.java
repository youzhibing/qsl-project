package com.qsl.service;

import com.qsl.entity.TblFollow;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 关注关系 服务类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-06-03
 */
public interface TblFollowService extends IService<TblFollow> {

    /**
     * 关注
     * @param followId 关注者id
     * @param userId 被关注者id
     */
    void follow(int followId, int userId);

    void follow1(int followId, int userId);
}
