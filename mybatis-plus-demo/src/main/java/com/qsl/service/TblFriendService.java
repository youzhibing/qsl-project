package com.qsl.service;

import com.qsl.entity.TblFriend;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 朋友关系 服务类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-06-03
 */
public interface TblFriendService extends IService<TblFriend> {

}
