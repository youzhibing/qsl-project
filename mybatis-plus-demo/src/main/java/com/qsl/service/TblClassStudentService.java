package com.qsl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qsl.entity.TblClassStudent;

import java.util.List;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2023/7/30 10:32
 */
public interface TblClassStudentService extends IService<TblClassStudent> {

    /**
     * 批量保存，存在则更新，不存在则插入
     * @param classStudents 目标集合
     */
    void batchSaveOrUpdate(List<TblClassStudent> classStudents);

    /**
     * 批量保存，存在则更新，不存在则插入
     * @param classStudents 目标集合
     */
    void batchSaveOrUpdatePlus(List<TblClassStudent> classStudents);
}
