package com.qsl.service;

import com.qsl.entity.TBusinessTask;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 业务任务关系 服务类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-08-21
 */
public interface TBusinessTaskService extends IService<TBusinessTask> {

}
