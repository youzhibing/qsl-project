package com.qsl.service;

import com.qsl.entity.TTaskExecLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 任务执行日志 服务类
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-08-21
 */
public interface TTaskExecLogService extends IService<TTaskExecLog> {

}
