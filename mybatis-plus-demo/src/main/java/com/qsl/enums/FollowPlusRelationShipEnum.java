package com.qsl.enums;

/**
 * @description: 关注 关系枚举
 * @author: 博客园@青石路
 * @date: 2022/6/3 17:15
 */
public enum FollowPlusRelationShipEnum {
    ONE(1),
    TWO(2),
    THREE(3),
    ;

    private int value;

    FollowPlusRelationShipEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
