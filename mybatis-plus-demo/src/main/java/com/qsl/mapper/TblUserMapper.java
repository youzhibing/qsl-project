package com.qsl.mapper;

import com.qsl.entity.TblUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 青石路
 * @since 2022-05-07
 */
public interface TblUserMapper extends BaseMapper<TblUser> {

}
