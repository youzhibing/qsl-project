package com.qsl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qsl.entity.TblClassStudent;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2023/7/30 10:32
 */
public interface TblClassStudentMapper extends BaseMapper<TblClassStudent> {

    int saveOrUpdate(@Param("classStudent") TblClassStudent classStudent);

    int batchSaveOrUpdate(@Param("classStudents") List<TblClassStudent> classStudents);
}
