package com.qsl.mapper;

import com.qsl.entity.TblFriend;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 朋友关系 Mapper 接口
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-06-03
 */
public interface TblFriendMapper extends BaseMapper<TblFriend> {

    int saveFriend(@Param("oneSideId") int oneSideId, @Param("otherSideId") int otherSideId);
}
