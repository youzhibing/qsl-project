package com.qsl.mapper;

import com.qsl.entity.TblFollowPlus;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 关注关系 Mapper 接口
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-06-03
 */
public interface TblFollowPlusMapper extends BaseMapper<TblFollowPlus> {

    int saveFollow(@Param("oneSideId") int oneSideId,
                    @Param("otherSideId") int otherSideId,
                    @Param("relationShip") int relationShip);
}
