package com.qsl.mapper;

import com.qsl.entity.TBusiness;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 业务信息 Mapper 接口
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-08-21
 */
public interface TBusinessMapper extends BaseMapper<TBusiness> {

}
