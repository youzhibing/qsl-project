package com.qsl.mapper;

import com.qsl.entity.TTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 任务信息 Mapper 接口
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-08-21
 */
public interface TTaskMapper extends BaseMapper<TTask> {

}
