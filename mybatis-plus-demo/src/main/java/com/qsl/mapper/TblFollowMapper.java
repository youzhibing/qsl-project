package com.qsl.mapper;

import com.qsl.entity.TblFollow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 关注关系 Mapper 接口
 * </p>
 *
 * @author 青石路
 * @since 2022-06-03
 */
public interface TblFollowMapper extends BaseMapper<TblFollow> {

}
