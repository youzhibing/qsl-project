package com.qsl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 任务执行日志
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-08-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TTaskExecLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日志id
     */
    @TableId(value = "log_id", type = IdType.AUTO)
    private Long logId;

    /**
     * 任务id
     */
    private Long taskId;

    /**
     * 执行状态, 失败：fail，成功：success
     */
    private String execStatus;

    /**
     * 数据日期
     */
    private LocalDate dataDate;

    /**
     * 备注
     */
    private String note;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 最终修改人
     */
    private Long modifyUser;

    /**
     * 最终修改时间
     */
    private LocalDateTime modifyTime;


}
