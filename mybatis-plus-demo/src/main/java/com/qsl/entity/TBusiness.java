package com.qsl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 业务信息
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-08-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TBusiness implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 业务id
     */
    @TableId(value = "business_id", type = IdType.AUTO)
    private Long businessId;

    /**
     * 业务名
     */
    private String businessName;

    /**
     * 备注
     */
    private String note;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 最终修改人
     */
    private Long modifyUser;

    /**
     * 最终修改时间
     */
    private LocalDateTime modifyTime;


}
