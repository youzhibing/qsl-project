package com.qsl.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 关注关系
 * </p>
 *
 * @author 博客园@青石路
 * @since 2022-06-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TblFollowPlus implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 一方
     */
    private Integer oneSideId;

    /**
     * 另一方
     */
    private Integer otherSideId;

    /**
     * 1：一方关注另一方，2：另一方关注一方，3：双方互相关注
     */
    private Integer relationShip;


}
