package com.qsl;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2022/5/7 21:35
 */
@MapperScan(basePackages = "com.qsl.mapper")
@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
