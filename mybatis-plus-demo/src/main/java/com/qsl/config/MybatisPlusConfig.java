package com.qsl.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description: 引入分页插件
 * @author: 博客园 @ 青石路
 * @date: 2022/5/8 9:06
 */
@Configuration
public class MybatisPlusConfig {
   
   @Bean
    public PaginationInterceptor paginationInterceptor(){
       return new PaginationInterceptor();
    }
}