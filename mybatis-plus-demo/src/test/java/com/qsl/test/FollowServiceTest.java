package com.qsl.test;

import com.qsl.service.TblFollowService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.concurrent.CountDownLatch;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2022/6/3 15:20
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FollowServiceTest {

    @Resource
    private TblFollowService followService;

    @Test
    public void follow() throws Exception {

        int userOne = 123;
        int userTwo = 456;

        CountDownLatch downLatch = new CountDownLatch(2);
        // userOne 关注 userTwo
        new Thread(() -> {
            followService.follow(userOne, userTwo);
            downLatch.countDown();
        }).start();

        // userTwo 关注 userOne
        new Thread(() -> {
            followService.follow(userTwo, userOne);
            downLatch.countDown();
        }).start();

        downLatch.await();
        System.out.println("主线程执行完毕...");
    }

    @Test
    public void follow1() throws Exception {

        int userOne = 123;
        int userTwo = 456;

        CountDownLatch downLatch = new CountDownLatch(2);
        // userOne 关注 userTwo
        new Thread(() -> {
            followService.follow1(userOne, userTwo);
            downLatch.countDown();
        }).start();

        // userTwo 关注 userOne
        new Thread(() -> {
            followService.follow1(userTwo, userOne);
            downLatch.countDown();
        }).start();

        downLatch.await();
        System.out.println("主线程执行完毕...");
    }
}
