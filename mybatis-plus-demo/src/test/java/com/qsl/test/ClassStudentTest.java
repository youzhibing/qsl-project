package com.qsl.test;

import com.qsl.entity.TblClassStudent;
import com.qsl.service.TblClassStudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2023/7/30 10:18
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ClassStudentTest {

    @Resource
    private TblClassStudentService studentService;

    @Test
    public void initData() {
        List<TblClassStudent> classStudents = new ArrayList<>();
        for (int i=10; i<=60; i++) {
            for (int j=100; j<=200; j++) {
                TblClassStudent classStudent = new TblClassStudent();
                classStudent.setClassNo("202310" + i);
                classStudent.setStudentNo("202310" + i + j);
                classStudents.add(classStudent);
                if (classStudents.size() == 500) {
                    // 批量保存
                    studentService.saveBatch(classStudents);
                    classStudents.clear();
                }
            }
        }
        if (classStudents.size() > 0) {
            studentService.saveBatch(classStudents);
        }
    }

    @Test
    public void batchSaveOrUpdateTest() throws InterruptedException {

        TblClassStudent classStudent = new TblClassStudent();
        classStudent.setId(1);
        classStudent.setClassNo("20231010");
        classStudent.setStudentNo("20231010201");

        TblClassStudent classStudent1 = new TblClassStudent();
        classStudent1.setId(2);
        classStudent1.setClassNo("20231010");
        classStudent1.setStudentNo("20231010202");

        List<TblClassStudent> classStudents1 = new ArrayList<>();
        classStudents1.add(classStudent);
        classStudents1.add(classStudent1);

        List<TblClassStudent> classStudents2 = new ArrayList<>();
        classStudents2.add(classStudent1);
        classStudents2.add(classStudent);

        CountDownLatch latch = new CountDownLatch(2);
        new Thread(() -> {
            studentService.batchSaveOrUpdate(classStudents1);
            latch.countDown();
        }, "t1").start();
        new Thread(() -> {
            studentService.batchSaveOrUpdate(classStudents2);
            latch.countDown();
        }, "t2").start();
        latch.await();
        System.out.println("主线程执行完毕");
    }

    @Test
    public void batchSaveOrUpdatePlus() {
        TblClassStudent classStudent = new TblClassStudent();
        classStudent.setId(1);
        classStudent.setClassNo("20231010");
        classStudent.setStudentNo("20231010201");

        TblClassStudent classStudent1 = new TblClassStudent();
        classStudent1.setId(2);
        classStudent1.setClassNo("20231010");
        classStudent1.setStudentNo("20231010202");

        List<TblClassStudent> classStudents1 = new ArrayList<>();
        classStudents1.add(classStudent);
        classStudents1.add(classStudent1);
        studentService.batchSaveOrUpdatePlus(classStudents1);
    }
}
