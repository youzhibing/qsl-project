package com.qsl.test;

import com.qsl.service.TblFollowPlusService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.concurrent.CountDownLatch;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2022/6/3 17:13
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FollowPlusServiceTest {

    @Resource
    private TblFollowPlusService followPlusService;

    @Test
    public void follow() throws Exception {

        int oneSideId = 123;
        int otherSideId = 456;

        CountDownLatch downLatch = new CountDownLatch(2);
        // oneSideId 关注 otherSideId
        new Thread(() -> {
            followPlusService.follow(oneSideId, otherSideId);
            downLatch.countDown();
        }).start();

        // otherSideId 关注 oneSideId
        new Thread(() -> {
            followPlusService.follow(otherSideId, oneSideId);
            downLatch.countDown();
        }).start();

        downLatch.await();
        System.out.println("主线程执行完毕...");
    }
}
