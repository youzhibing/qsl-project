package com.qsl.test;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qsl.entity.TblUser;
import com.qsl.service.TblUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2022/5/8 9:06
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Resource
    private TblUserService userService;

    @Test
    public void listAll() {
        List<TblUser> all = userService.list();
        System.out.println(all);
    }

    /**
     * @author 博客园 @ 青石路
     */
    @Test
    public void listPage() {
        Page<TblUser> page = new Page<>(1, 5);
        IPage<TblUser> resultPage = userService.page(page);
        System.out.println(resultPage);
    }

    /**
     * @author 博客园 @ 青石路
     */
    @Test
    public void listPage2() {
        Page<TblUser> page = new Page<>(1, 5);
        LambdaQueryWrapper<TblUser> lambda = new QueryWrapper<TblUser>().lambda();
        lambda.eq(TblUser::getName, "吴用");
        IPage<TblUser> resultPage = userService.page(page, lambda);
        System.out.println(resultPage);
    }
}
