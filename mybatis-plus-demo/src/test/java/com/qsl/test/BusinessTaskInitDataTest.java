package com.qsl.test;

import com.qsl.entity.TBusiness;
import com.qsl.entity.TBusinessTask;
import com.qsl.entity.TTask;
import com.qsl.entity.TTaskExecLog;
import com.qsl.service.TBusinessService;
import com.qsl.service.TBusinessTaskService;
import com.qsl.service.TTaskExecLogService;
import com.qsl.service.TTaskService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sun.util.resources.LocaleData;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @description: xxx描述
 * @author: 博客园@青石路
 * @date: 2022/8/21 12:24
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BusinessTaskInitDataTest {

    @Resource
    private TBusinessService businessService;
    @Resource
    private TTaskService taskService;
    @Resource
    private TBusinessTaskService businessTaskService;
    @Resource
    private TTaskExecLogService execLogService;

    @Test
    public void initData() {
        long userId = 123L;
        // 业务 1 ~ 2000
        List<TBusiness> businessList = new ArrayList<>(2000);
        for (long i=1; i<=2000; i++) {
            TBusiness business = new TBusiness();
            business.setBusinessId(i);
            business.setBusinessName("业务" + i);
            business.setCreateUser(userId);
            business.setModifyUser(userId);
            businessList.add(business);
        }
        businessService.saveBatch(businessList);

        // 任务 1 ~ 4000
        List<TTask> taskList = new ArrayList<>(4000);
        for (long i=1; i<=4000; i++) {
            TTask task = new TTask();
            task.setTaskId(i);
            task.setTaskName("任务" + i);
            task.setCreateUser(userId);
            task.setModifyUser(userId);
            taskList.add(task);
        }
        taskService.saveBatch(taskList);

        // 业务任务 每个业务 35 个任务
        List<TBusinessTask> businessTasks = new ArrayList<>(4000);
        businessList.forEach(e -> {
            for (long i=1; i<=35; i++) {
                TBusinessTask businessTask = new TBusinessTask();
                businessTask.setBusinessId(e.getBusinessId());
                businessTask.setTaskId(e.getBusinessId()+i*10);
                businessTasks.add(businessTask);
            }
        });
        businessTaskService.saveBatch(businessTasks);

        // 任务日志 300W 条记录
        String[] statusArr = new String[]{"fail", "success"};
        LocalDateTime todayTime = LocalDateTime.now();
        LocalDate today = todayTime.toLocalDate();
        List<TTaskExecLog> logs = new ArrayList<>(2000);
        for (int i=1; i<=3000000; i++) {
            int statusIndex = ThreadLocalRandom.current().nextInt(0, 2);
            String status = statusArr[statusIndex];
            Long taskId = ThreadLocalRandom.current().nextLong(1, 4001);
            int beforeDays = ThreadLocalRandom.current().nextInt(0, 500);
            LocalDateTime createTime = todayTime.minusDays(ThreadLocalRandom.current().nextInt(0, 500))
                    .minusHours(ThreadLocalRandom.current().nextInt(0, 25))
                    .minusMinutes(ThreadLocalRandom.current().nextInt(0,61));

            TTaskExecLog log = new TTaskExecLog();
            log.setTaskId(taskId).setExecStatus(status).setDataDate(today.minusDays(beforeDays))
                    .setCreateUser(userId).setModifyUser(userId).setCreateTime(createTime).setModifyTime(createTime);
            logs.add(log);
            if (logs.size() == 2000) {
                execLogService.saveBatch(logs);
                logs.clear();
            }
        }
        if (!logs.isEmpty()) {
            execLogService.saveBatch(logs);
        }
    }
}
