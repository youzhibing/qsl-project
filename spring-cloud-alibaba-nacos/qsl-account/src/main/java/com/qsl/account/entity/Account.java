package com.qsl.account.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

/***
 * @description 账户
 * @author 青石路
 * @date 2022/1/9 17:44
 */
@Data
@TableName("t_account")
public class Account {

    /**
     * 自增id
     */
    @TableId(type = IdType.AUTO)
    private Long accountId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 余额
     */
    private BigDecimal balance;

    /**
     * 冻结金额
     */
    private BigDecimal lock_amount;

    /**
     * 备注
     */
    private String note;
}
