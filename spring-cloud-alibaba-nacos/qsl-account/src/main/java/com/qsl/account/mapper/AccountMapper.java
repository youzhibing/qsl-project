package com.qsl.account.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qsl.account.entity.Account;
import org.apache.ibatis.annotations.Mapper;

/***
 * @description account mapper
 * @author 青石路
 * @date 2022/1/9 17:48
 */
@Mapper
public interface AccountMapper extends BaseMapper<Account> {
}
