package com.qsl.account.controller;

import com.qsl.account.service.AccountService;
import com.qsl.account.vo.AccountVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;

/***
 * @description account
 * @author 青石路
 * @date 2022/1/8 15:40
 */
@RestController
@RequestMapping("/account")
public class AccountController {

    @Resource
    private AccountService accountService;

    @GetMapping("/hello")
    public String hello() {
        return "hello, account!";
    }

    @GetMapping("/{accountId}")
    public AccountVo getById(@PathVariable Long accountId) {
        return accountService.getAccountVo(accountId);
    }

    @PostMapping("/debit/{userId}/{money}")
    public boolean debit(@PathVariable Long userId, @PathVariable BigDecimal money) {
        System.out.println("debit=====================================");
        return accountService.debit(userId, money);
    }
}
