package com.qsl.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qsl.account.entity.Account;
import com.qsl.account.vo.AccountVo;

import java.math.BigDecimal;

/***
 * @description account service
 * @author 青石路
 * @date 2022/1/9 17:49
 */
public interface AccountService extends IService<Account> {

    /**
     * 根据账户id查询账户信息
     * @param accountId
     * @return
     */
    AccountVo getAccountVo(Long accountId);

    /**
     * 扣款
     * @param userId
     * @param money
     * @return
     */
    boolean debit(Long userId, BigDecimal money);
}
