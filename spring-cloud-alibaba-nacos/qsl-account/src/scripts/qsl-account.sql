CREATE DATABASE qsl_account;

-- 用户账户
CREATE TABLE `t_account` (
  `account_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` BIGINT(20) NOT NULL COMMENT '用户id',
  `balance` DECIMAL(16,2) NOT NULL COMMENT '余额',
  `lock_amount` DECIMAL(16,2) NOT NULL COMMENT '冻结金额',
  `note` VARCHAR(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT = '账户';

-- 用户账户变动记录

-- seata
CREATE TABLE `undo_log`(
    `id`            bigint(20) NOT NULL AUTO_INCREMENT,
    `branch_id`     bigint(20) NOT NULL,
    `xid`           varchar(100) NOT NULL,
    `context`       varchar(128) NOT NULL,
    `rollback_info` longblob     NOT NULL,
    `log_status`    int(11) NOT NULL,
    `log_created`   datetime     NOT NULL,
    `log_modified`  datetime     NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;