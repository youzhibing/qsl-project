CREATE DATABASE qsl_order;

CREATE TABLE `t_order` (
  `order_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_no` CHAR(20) NOT NULL COMMENT '订单号',
  `user_id` BIGINT(20) NOT NULL COMMENT '用户id',
  `order_amount` DECIMAL(16,2) NOT NULL,
  `note` VARCHAR(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT = '订单';

CREATE TABLE `t_order_ware` (
  `order_ware_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_id` BIGINT(20) NOT NULL COMMENT '订单id',
  `ware_id` BIGINT(20) NOT NULL COMMENT '订单商品id',
  `transaction_qty` decimal(16,4) NOT NULL COMMENT '成交数量',
  `transaction_price` DECIMAL(16,2) NOT NULL COMMENT '成交价',
  PRIMARY KEY (`order_ware_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT = '订单商品';


-- seata
CREATE TABLE `undo_log`(
    `id`            bigint(20) NOT NULL AUTO_INCREMENT,
    `branch_id`     bigint(20) NOT NULL,
    `xid`           varchar(100) NOT NULL,
    `context`       varchar(128) NOT NULL,
    `rollback_info` longblob     NOT NULL,
    `log_status`    int(11) NOT NULL,
    `log_created`   datetime     NOT NULL,
    `log_modified`  datetime     NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;