package com.qsl.order.controller;

import com.qsl.order.service.OrderService;
import com.qsl.order.vo.OrderVo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/***
 * @description order
 * @author 青石路
 * @date 2022/1/8 15:40
 */
@RestController
@RequestMapping("/order")
@RefreshScope
public class OrderController {

    @Resource
    private OrderService orderService;

    @Resource
    private RestTemplate restTemplate;

    @Value("${useLocalCache:false}")
    private boolean useLocalCache;

    @GetMapping("/hello")
    public String hello() {
        return "hello, order!";
    }

    @GetMapping("/user-local-cache")
    public boolean getUseLocalCache() {
        return useLocalCache;
    }

    @RequestMapping(value = "/stock/hello/{str}", method = RequestMethod.GET)
    public String stockHello(@PathVariable String str) {
        return restTemplate.getForObject("http://qsl-stock/stock/hello/" + str, String.class);
    }

    @GetMapping("/{orderId}")
    public OrderVo getById(@PathVariable Long orderId) {
        return orderService.get(orderId);
    }

    @PostMapping("/add")
    public boolean add(@RequestBody OrderVo orderVo) {
        orderService.add(orderVo);
        return true;
    }
}
