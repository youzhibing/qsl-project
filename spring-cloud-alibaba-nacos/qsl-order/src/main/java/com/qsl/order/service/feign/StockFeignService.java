package com.qsl.order.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/***
 * @description stock feign 接口
 * @author 青石路
 * @date 2022/1/8 18:15
 */
@FeignClient("qsl-stock")
@RequestMapping("/stock")
public interface StockFeignService {

    /**
     * hello
     * @param str
     * @return
     */
    @GetMapping("/hello/{str}")
    String hello(@PathVariable String str);

    /**
     * 扣减库存
     * @param wareId
     * @param deductQty
     * @return
     */
    @PostMapping("/deduct")
    boolean deduct(@RequestParam("wareId") Long wareId, @RequestParam("deductQty") BigDecimal deductQty);
}
