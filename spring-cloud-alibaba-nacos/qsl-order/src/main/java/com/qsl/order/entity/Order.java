package com.qsl.order.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

/***
 * @description 订单
 * @author 青石路
 * @date 2022/1/9 15:34
 */
@Data
@TableName("t_order")
public class Order {

    /**
     * 自增主键
     */
    @TableId(type = IdType.AUTO)
    private Long orderId;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 订单总金额
     */
    private BigDecimal orderAmount;

    /**
     * 备注
     */
    private String note;
}
