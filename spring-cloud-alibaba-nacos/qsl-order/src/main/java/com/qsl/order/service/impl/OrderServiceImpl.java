package com.qsl.order.service.impl;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qsl.order.entity.Order;
import com.qsl.order.entity.OrderWare;
import com.qsl.order.mapper.OrderMapper;
import com.qsl.order.service.OrderService;
import com.qsl.order.service.OrderWareService;
import com.qsl.order.service.feign.AccountFeignService;
import com.qsl.order.service.feign.StockFeignService;
import com.qsl.order.vo.OrderVo;
import com.qsl.order.vo.OrderWareVo;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/***
 * @description order service impl
 * @author 青石路
 * @date 2022/1/9 15:54
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Resource
    private OrderWareService orderWareService;
    @Resource
    private AccountFeignService accountFeignService;
    @Resource
    private StockFeignService stockFeignService;

    @Override
    public OrderVo get(Long orderId) {
        Order order = getById(orderId);
        if (Objects.isNull(order)) {
            return null;
        }
        OrderVo orderVo = new OrderVo();
        BeanUtils.copyProperties(order, orderVo);

        LambdaQueryChainWrapper<OrderWare> wareQuery = orderWareService.lambdaQuery().eq(OrderWare::getOrderId, orderId);
        List<OrderWare> wares = wareQuery.list();
        if (!CollectionUtils.isEmpty(wares)) {
            List<OrderWareVo> wareVos = new ArrayList<>(wares.size());
            wares.stream().forEach(e -> {
                OrderWareVo orderWareVo = new OrderWareVo();
                BeanUtils.copyProperties(e, orderWareVo);
                wareVos.add(orderWareVo);
            });
            orderVo.setWares(wareVos);
        }
        return orderVo;
    }

    @Override
    @GlobalTransactional(rollbackFor = Exception.class)
    public void add(OrderVo orderVo) {

        // 1、添加订单
        BigDecimal orderAmount = BigDecimal.ZERO;
        List<OrderWare> wares = new ArrayList<>(orderVo.getWares().size());
        for (OrderWareVo wareVo : orderVo.getWares()) {
            OrderWare orderWare = new OrderWare();
            BeanUtils.copyProperties(wareVo, orderWare);
            orderAmount = orderAmount.add(wareVo.getTransactionPrice().multiply(wareVo.getTransactionQty()));
            wares.add(orderWare);
        }
        orderVo.setOrderAmount(orderAmount);
        this.save(orderVo);
        wares.stream().forEach(e -> e.setOrderId(orderVo.getOrderId()));
        orderWareService.saveBatch(wares);

        // 2、扣减库存
        OrderWareVo orderWareVo = orderVo.getWares().get(0);
        stockFeignService.deduct(orderWareVo.getWareId(), orderWareVo.getTransactionQty());

        // 3、扣减用户余额
        accountFeignService.debit(orderVo.getUserId(), orderVo.getOrderAmount());
    }
}
