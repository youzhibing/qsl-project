package com.qsl.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qsl.order.entity.OrderWare;
import com.qsl.order.mapper.OrderWareMapper;
import com.qsl.order.service.OrderWareService;
import org.springframework.stereotype.Service;

/***
 * @description order ware
 * @author 青石路
 * @date 2022/1/9 16:13
 */
@Service
public class OrderWareServiceImpl extends ServiceImpl<OrderWareMapper, OrderWare> implements OrderWareService {
}
