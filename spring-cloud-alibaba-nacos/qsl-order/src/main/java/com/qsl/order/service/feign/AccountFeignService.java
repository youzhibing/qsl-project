package com.qsl.order.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;

/***
 * @description account feign 接口
 * @author 青石路
 * @date 2022/1/9 19:18
 */
@FeignClient("qsl-account")
@RequestMapping("/account")
public interface AccountFeignService {

    /**
     * 扣减余额
     * @param userId
     * @param money
     * @return
     */
    @PostMapping("/debit/{userId}/{money}")
    boolean debit(@PathVariable Long userId, @PathVariable BigDecimal money);
}
