package com.qsl.order.vo;

import com.qsl.order.entity.Order;
import lombok.Data;

import java.util.List;

/***
 * @description order
 * @author 青石路
 * @date 2022/1/9 15:59
 */
@Data
public class OrderVo extends Order {

    private List<OrderWareVo> wares;
}
