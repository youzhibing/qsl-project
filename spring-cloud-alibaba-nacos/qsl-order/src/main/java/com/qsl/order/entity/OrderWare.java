package com.qsl.order.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

/***
 * @description 订单商品
 * @author 青石路
 * @date 2022/1/9 15:39
 */
@Data
@TableName("t_order_ware")
public class OrderWare {

    /**
     * 自增主键
     */
    @TableId(type = IdType.AUTO)
    private Long orderWareId;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 商品id
     */
    private Long wareId;

    /**
     * 成交数量
     */
    private BigDecimal transactionQty;

    /**
     * 成交价
     */
    private BigDecimal transactionPrice;
}
