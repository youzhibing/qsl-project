package com.qsl.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qsl.order.entity.Order;
import com.qsl.order.vo.OrderVo;

/***
 * @description order service
 * @author 青石路
 * @date 2022/1/9 15:50
 */
public interface OrderService extends IService<Order> {

    /**
     * 根据订单id查询订单信息
     * @param orderId
     * @return
     */
    OrderVo get(Long orderId);

    /**
     * 添加订单
     * @param orderVo
     */
    void add(OrderVo orderVo);
}
