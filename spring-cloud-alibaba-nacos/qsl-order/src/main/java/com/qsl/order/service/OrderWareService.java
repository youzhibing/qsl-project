package com.qsl.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qsl.order.entity.OrderWare;

/***
 * @description order ware
 * @author 青石路
 * @date 2022/1/9 16:12
 */
public interface OrderWareService extends IService<OrderWare> {
}
