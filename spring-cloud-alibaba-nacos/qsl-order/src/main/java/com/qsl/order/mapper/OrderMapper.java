package com.qsl.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qsl.order.entity.Order;
import org.apache.ibatis.annotations.Mapper;

/***
 * @description order mapper
 * @author 青石路
 * @date 2022/1/9 15:43
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {
}
