package com.qsl.order.controller;

import com.qsl.order.service.feign.StockFeignService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/***
 * @description xxxx
 * @author 青石路
 * @date 2022/1/8 18:18
 */
@RestController
@RequestMapping("/order-feign")
public class OrderFeignController {

    @Resource
    private StockFeignService stockFeignService;

    @GetMapping("/stock/hello/{message}")
    public String stackHello(@PathVariable String message) {
        return stockFeignService.hello(message);
    }
}
