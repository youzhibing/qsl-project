package com.qsl.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qsl.order.entity.OrderWare;
import org.apache.ibatis.annotations.Mapper;

/***
 * @description order ware mapper
 * @author 青石路
 * @date 2022/1/9 15:44
 */
@Mapper
public interface OrderWareMapper extends BaseMapper<OrderWare> {
}
