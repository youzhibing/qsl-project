package com.qsl.order.vo;

import com.qsl.order.entity.OrderWare;
import lombok.Data;

/***
 * @description order ware
 * @author 青石路
 * @date 2022/1/9 16:00
 */
@Data
public class OrderWareVo extends OrderWare {
}
