CREATE DATABASE qsl_stock;

CREATE TABLE `t_ware_stock` (
  `stock_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ware_id` BIGINT(20) NOT NULL COMMENT '商品id',
  `qualified_qty` DECIMAL(16,4) COMMENT '合格数量',
  `reserved_qty` DECIMAL(16,4) COMMENT '合格预留数量',
  `to_be_inspected_qty` DECIMAL(16,4) COMMENT '待验数量',
  `unqualified_qty` DECIMAL(16,4) COMMENT '不合格数量',
  `expected_qty` DECIMAL(16,4) COMMENT '预期数量',
  PRIMARY KEY (`stock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT = '库存';

-- 库存变动记录

-- seata
CREATE TABLE `undo_log`(
    `id`            bigint(20) NOT NULL AUTO_INCREMENT,
    `branch_id`     bigint(20) NOT NULL,
    `xid`           varchar(100) NOT NULL,
    `context`       varchar(128) NOT NULL,
    `rollback_info` longblob     NOT NULL,
    `log_status`    int(11) NOT NULL,
    `log_created`   datetime     NOT NULL,
    `log_modified`  datetime     NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;