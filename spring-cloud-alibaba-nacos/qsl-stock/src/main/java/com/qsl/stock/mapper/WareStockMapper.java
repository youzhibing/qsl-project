package com.qsl.stock.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qsl.stock.entity.WareStock;
import org.apache.ibatis.annotations.Mapper;

/***
 * @description ware stock mapper
 * @author 青石路
 * @date 2022/1/9 18:45
 */
@Mapper
public interface WareStockMapper extends BaseMapper<WareStock> {
}
