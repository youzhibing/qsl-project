package com.qsl.stock.controller;

import com.qsl.stock.service.WareStockService;
import com.qsl.stock.vo.WareStockVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;

/***
 * @description stock
 * @author 青石路
 * @date 2022/1/8 15:37
 */
@RestController
@RequestMapping("/stock")
public class StockController {

    @Resource
    private WareStockService wareStockService;

    @GetMapping("/hello/{str}")
    public String hello(@PathVariable String str) {
        return "hello " + str + ", I'm stock";
    }

    @GetMapping("/{stockId}")
    public WareStockVo getById(@PathVariable Long stockId) {
        return wareStockService.get(stockId);
    }

    @PostMapping("/deduct")
    public boolean deduct(Long wareId, BigDecimal deductQty) {
        System.out.println("deduct=================================");
        return wareStockService.deduct(wareId, deductQty);
    }
}
