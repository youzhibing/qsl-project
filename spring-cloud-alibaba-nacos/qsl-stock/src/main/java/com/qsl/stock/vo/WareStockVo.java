package com.qsl.stock.vo;

import com.qsl.stock.entity.WareStock;
import lombok.Data;

/***
 * @description ware stock vo
 * @author 青石路
 * @date 2022/1/9 18:46
 */
@Data
public class WareStockVo extends WareStock {
}
