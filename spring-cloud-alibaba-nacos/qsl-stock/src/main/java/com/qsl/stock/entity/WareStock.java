package com.qsl.stock.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

/***
 * @description 库存
 * @author 青石路
 * @date 2022/1/9 18:37
 */
@Data
@TableName("t_ware_stock")
public class WareStock {

    /**
     * 自增主键
     */
    @TableId(type = IdType.AUTO)
    private Long stockId;

    /**
     * 商品id
     */
    private Long wareId;

    /**
     * 合格数量
     */
    private BigDecimal qualifiedQty;

    /**
     * 合格预留数量
     */
    private BigDecimal reservedQty;

    /**
     * 待验数量
     */
    private BigDecimal to_be_inspectedQty;

    /**
     * 不合格数量
     */
    private BigDecimal unqualifiedQty;

    /**
     * 预期数量
     */
    private BigDecimal expectedQty;

}
