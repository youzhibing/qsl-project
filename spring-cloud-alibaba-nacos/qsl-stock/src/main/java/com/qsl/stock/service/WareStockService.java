package com.qsl.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qsl.stock.entity.WareStock;
import com.qsl.stock.vo.WareStockVo;

import java.math.BigDecimal;

/***
 * @description ware stock
 * @author 青石路
 * @date 2022/1/9 18:47
 */
public interface WareStockService extends IService<WareStock> {

    /**
     * 根据 stockId 查询库存信息
     * @param stockId
     * @return
     */
    WareStockVo get(Long stockId);

    /**
     * 扣减库存
     * @param wareId
     * @param deductQty
     * @return
     */
    boolean deduct(Long wareId, BigDecimal deductQty);
}
