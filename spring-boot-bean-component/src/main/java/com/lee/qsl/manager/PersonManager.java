package com.lee.qsl.manager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/***
 * @description: Person Manager
 * @author : 青石路
 * @date: 2021/10/2 20:14
 */
// @Component
public class PersonManager {

    @Value("${person.name}")
    private String personName;

    public PersonManager() {
        System.out.println("Person 空构造方法 this = " + this);
    }

    public PersonManager(String personName) {
        this.personName = personName;
        System.out.println("Person 非空构造方法 this = " + this);
    }
}
