package com.lee.qsl.manager;

import org.springframework.stereotype.Component;

/***
 * @description: User Manager
 * @author : 青石路
 * @date: 2021/10/2 20:00
 */
@Component
public class UserManager {

    private String userName;

    public UserManager() {
        System.out.println("User 空构造方法 this = " + this);
    }

    public UserManager(String userName) {
        this.userName = userName;
        System.out.println("User 非空构造方法 this = " + this);
    }

    public String getUserName() {
        return userName;
    }
}
