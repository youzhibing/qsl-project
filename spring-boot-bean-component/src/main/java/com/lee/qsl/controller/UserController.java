package com.lee.qsl.controller;

import com.lee.qsl.manager.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 * @description: User Controller
 * @author : 青石路
 * @date: 2021/10/2 20:26
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserManager userManager;

    @GetMapping("/name")
    public String getUserName() {
        System.out.println("userManager 类型为：" + userManager.getClass().getName());
        return userManager.getUserName();
    }
}
