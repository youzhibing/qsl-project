package com.lee.qsl.config;

import com.lee.qsl.manager.UserManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/***
 * @description: User Config
 * @author : 青石路
 * @date: 2021/10/2 19:15
 */
@Configuration
public class UserConfig {

    @Value("${user.name}")
    private String userName;

    @Bean
    public UserManager userManager() {
        UserManager userManager = new UserManager(userName);
        return userManager;
    }
}
