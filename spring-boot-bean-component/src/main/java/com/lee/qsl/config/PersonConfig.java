package com.lee.qsl.config;

import com.lee.qsl.manager.PersonManager;
import org.springframework.beans.factory.annotation.Value;

/***
 * @description: Person Config
 * @author : 青石路
 * @date: 2021/10/2 20:21
 */
// @Configuration
public class PersonConfig {

    @Value("${person.name}")
    private String personName;

    // @Bean
    public PersonManager userManager() {
        PersonManager personManager = new PersonManager(personName);
        return personManager;
    }
}
