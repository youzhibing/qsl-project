package com.qsl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: 青石路
 */
@SpringBootApplication
public class ExternalConfigApplication {
    public static void main(String[] args) {
        SpringApplication.run(ExternalConfigApplication.class, args);
    }
}