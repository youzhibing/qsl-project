package com.qsl.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author: 青石路
 */
@Component
public class ConfigDemo implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigDemo.class);

    @Value("${retry.times}")
    private Integer retryTimes;
    @Value("${http.url}")
    private String httpUrl;

    @Override
    public void afterPropertiesSet() throws Exception {
        LOGGER.info("retryTimes:{}, httpUrl:{}", retryTimes, httpUrl);
    }
}
