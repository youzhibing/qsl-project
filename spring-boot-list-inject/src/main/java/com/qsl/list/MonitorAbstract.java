package com.qsl.list;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/9/7 15:04
 **/
public abstract class MonitorAbstract {

    public void execute() {
        this.run();
    }

    /**
     * 运行
     */
    protected abstract void run();
}
