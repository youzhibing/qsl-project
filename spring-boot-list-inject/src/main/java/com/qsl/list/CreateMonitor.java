package com.qsl.list;

import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: 博客园@游智兵
 * @CreateDate: 2022/9/7 15:06
 **/
@Component
public class CreateMonitor extends MonitorAbstract {

    @Override
    protected void run() {
        System.out.println("CreateMonitor");
    }
}
