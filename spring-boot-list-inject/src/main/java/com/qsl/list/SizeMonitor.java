package com.qsl.list;

import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/9/7 15:06
 **/
@Component
public class SizeMonitor extends MonitorAbstract {

    @Override
    protected void run() {
        System.out.println("SizeMonitor");
    }
}
