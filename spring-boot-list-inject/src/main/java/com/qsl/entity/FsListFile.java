package com.qsl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * @Author: Haocheng Sun
 * @Date: 2019.10.17
 */
@Entity
@Table(name = "fs_list_file")
public class FsListFile {

    @Id
    @Column
    private String id;

    @Column
    private String ossSourceName;

    /**
     * 文件路径
     */
    @Column
    private String filePath;

    /**
     * 文件大小
     */
    @Column
    private Long fileSize;

    /**
     * MD5码
     */
    @Column
    private String md5;

    /**
     * 资源ID
     */
    @Column
    private String resourceId;

    /**
     * 文件读取数据时间
     */
    @Column
    private Timestamp fileReadDataTime;

    /**
     * 文件最后修改时间
     */
    @Column
    private Timestamp fileModifyTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOssSourceName() {
        return ossSourceName;
    }

    public void setOssSourceName(String ossSourceName) {
        this.ossSourceName = ossSourceName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public Timestamp getFileReadDataTime() {
        return fileReadDataTime;
    }

    public void setFileReadDataTime(Timestamp fileReadDataTime) {
        this.fileReadDataTime = fileReadDataTime;
    }

    public Timestamp getFileModifyTime() {
        return fileModifyTime;
    }

    public void setFileModifyTime(Timestamp fileModifyTime) {
        this.fileModifyTime = fileModifyTime;
    }

    @Override
    public String toString() {
        return "FsListFile{" +
                "id='" + id + '\'' +
                "ossSourceName='" + ossSourceName + '\'' +
                ", filePath='" + filePath + '\'' +
                ", fileSize=" + fileSize +
                ", md5='" + md5 + '\'' +
                ", resourceId='" + resourceId + '\'' +
                ", fileReadDataTime=" + fileReadDataTime +
                ", fileModifyTime=" + fileModifyTime +
                '}';
    }
}
