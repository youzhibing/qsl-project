package com.qsl.manager;

import com.qsl.list.MonitorAbstract;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:
 * @Author: 博客园@青石路
 * @CreateDate: 2022/9/7 15:08
 **/
@Component
public class MonitorManager implements InitializingBean {

    // MonitorAbstract 类型的实例对象会自动添加到 monitors； @Resource 注解也可以
    @Autowired
    private List<MonitorAbstract> monitors;

    @Override
    public void afterPropertiesSet() throws Exception {
        for (MonitorAbstract monitor : monitors) {
            monitor.execute();
        }
    }
}
