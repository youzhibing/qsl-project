package com.qsl.mapper;

import com.qsl.entity.FsListFile;
import com.qsl.model.ListFileMessageModel;
import org.mapstruct.Mapper;

/**
 * @Author: Haocheng Sun
 * @Date: 2019.10.24
 */
@Mapper(componentModel = "spring")
public interface FsListFileToMsgMapper {

    ListFileMessageModel filetomsg(FsListFile fs);

    FsListFile msgtoFile(ListFileMessageModel model);


}
