package com.qsl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.qsl.entity.FsListFile;
import com.qsl.mapper.FsListFileToMsgMapper;
import com.qsl.model.ListFileMessageModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @description:
 * @author: 青石路
 * @createDate: 2024/1/8 15:14
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class TimestampTest {

    @Resource
    private FsListFileToMsgMapper fsListFileToMsgMapper;

    @Test
    public void test() throws JsonProcessingException {
        String modelJson = "{\"changeType\":\"CREATE\",\"fileModifyTime\":1704634426643,\"fileName\":\"GIL_LC_INSTIARCHIVE_20240107.dat.zip\",\"filePath\":\"UserData/8335/20240107/GIL_LC_INSTIARCHIVE_20240107.dat.zip\",\"fileReadDataTime\":1704556800000,\"fileSize\":302821985,\"md5\":\"fff3b1ff3b813e4d9bfc1f2601b8e3a5\",\"ossSourceName\":\"IDG\",\"resourceId\":\"8335\"}";
        ListFileMessageModel model = ListFileMessageModel.toModel(modelJson);
        System.out.println("字符串转对象完成");

        FsListFile fsListFile = fsListFileToMsgMapper.msgtoFile(model);
        System.out.println(fsListFile.getFileModifyTime());
    }
}
