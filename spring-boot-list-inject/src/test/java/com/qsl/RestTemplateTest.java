package com.qsl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.junit.platform.commons.util.StringUtils;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.Resource;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * @description: RestTemplate 测试
 * @author: 博客园@青石路
 * @date: 2023/11/26 15:31
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RestTemplateTest {

    @Resource
    private RestTemplate restTemplate;

    @Test
    public void testOss() throws URISyntaxException {
        String ossUrl = "http://118.178.175.19/file/gilDownload/JYPRIME/usrGSGGYWFWB/2018/05/29/591636389887.HTML?token=44805593628e4dd1a92cc6a4585ee96f&params=neTCsC66XoCZmnilRefrKr9EpFzU1pLdfnvcx3Umarzpy8RMQ7WocSruYo%2BpP3555g4CRd4gVsxtbAG%2Bmp6iL9QKGsc8A2kFF%2FCRmTWEGZvqk%2FE6XhLNLbJaxvNwDnQLbNmcHs0Bx%2F%2F4ysfdxirSJVMCYliqAWzBSvhokSPMINxQvTR7gEUnSJ0WCGadN5nw&fileSize=878183";
//        String s = convertOssDomain(ossUrl, "http://118.178.175.19:8080", "http://118.178.175.19/file/url");
//        System.out.println("s = " + s);
        URI uri = UriComponentsBuilder.fromUriString(ossUrl).build(true).toUri();
        long speedLimit = 512 * 1024;
        long start = 0;
        HttpHeaders httpHeaders = new HttpHeaders();
        while (start < 878183) {
            long end = start + speedLimit - 1;
            String endStr = end < 878183 ? end + "" : "";
            httpHeaders.set("Range", "bytes=" + start + "-" + endStr);
            ResponseEntity<byte[]> resp = restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<byte[]>(httpHeaders), byte[].class);
            byte[] body = resp.getBody();
            String checkMsg = checkRespStatus(body);
            if (checkMsg.length() > 0) {
                System.out.println(checkMsg);
                throw new RuntimeException(checkMsg);
            }
            start = start + body.length;
        }
    }

    private String checkRespStatus(byte[] body) {
        if (body.length > 500) {
            return "";
        }
        JSONObject jsonObject = JSON.parseObject(body, JSONObject.class);
        String code = jsonObject.getString("code");
        if (StringUtils.isBlank(code)) {
            return "";
        }
        String detail = jsonObject.getString("detail");
        return "下载失败，code=" + code + ",detail=" + detail;
    }

    private String convertOssDomain(String url, String ossDomain, String fileServer) {
        if (StringUtils.isBlank(ossDomain)) {
            // 如果没有配置OSS域名,则直接返回,不做转换
            return url;
        }
        try {
            URL downLoadUrl = new URL(url);
            URL ossUrl = new URL(ossDomain);
            // IP相同则不必转换
            if (downLoadUrl.getHost().equals(ossUrl.getHost())) {
                return url;
            }
            // 如果是80端口或没有指定端口，使用fileServer的接口地址
            if (downLoadUrl.getPort() == 80 || downLoadUrl.getPort() == -1) {
                URL fileServerUrl = new URL(fileServer);
                return new URL(fileServerUrl.getProtocol(), fileServerUrl.getHost(), fileServerUrl.getPort(),
                        downLoadUrl.getFile()).toString();
            }
            return new URL(ossUrl.getProtocol(), ossUrl.getHost(), ossUrl.getPort(), downLoadUrl.getFile())
                    .toString();
        } catch (Exception e) {
            return url;
        }
    }
}