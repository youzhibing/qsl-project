1、Spring Bean 的加载顺序
    @Configuration、@Service、@Component 哪个先被加载

2、SpringBoot 版本（2.0.3 和 2.1.5）
    版本不同，循环依赖的提示不用，同时对应用的启动也会有不同的影响（非正常启动、不能启动）