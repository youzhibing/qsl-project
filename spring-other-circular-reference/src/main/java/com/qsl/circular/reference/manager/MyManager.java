package com.qsl.circular.reference.manager;

import com.qsl.circular.reference.sender.MySender;
import com.qsl.circular.reference.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/***
 * @description xxxx
 * @author 青石路
 * @date 2022/1/18 20:14
 */
@Component
public class MyManager {

    @Autowired
    private MySender mySender;

    @Autowired
    private MyService myService;

    public MyManager() {
        System.out.println("MyManager...");
    }

    public String sayHello(String name) {
        return myService.sayHello(name);
    }
}
