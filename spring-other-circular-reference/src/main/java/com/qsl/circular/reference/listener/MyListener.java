package com.qsl.circular.reference.listener;

import com.qsl.circular.reference.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/***
 * @description xxxx
 * @author 青石路
 * @date 2022/1/18 20:07
 */
@Component
public class MyListener {

    @Autowired
    private MyService myService;

    public MyListener() {
        System.out.println("MyListener...");
    }

    public String sayHello(String name) {
        return myService.sayHello(name);
    }
}
