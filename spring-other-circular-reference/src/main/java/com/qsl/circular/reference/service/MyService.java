package com.qsl.circular.reference.service;

/***
 * @description xxxx
 * @author 青石路
 * @date 2022/1/18 20:14
 */
public interface MyService {

    /**
     * 打招呼
     * @param name
     * @return
     */
    String sayHello(String name);
}
