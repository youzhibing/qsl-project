package com.qsl.circular.reference.config;

import com.qsl.circular.reference.listener.MyListener;
import com.qsl.circular.reference.sender.MySender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/***
 * @description xxxx
 * @author 青石路
 * @date 2022/1/18 20:12
 */
@Configuration
public class MyConfig {

    @Autowired
    MyListener myListener;

    public MyConfig() {
        System.out.println("MyConfig...");
    }

    @Bean
    public MySender mySender() {
        System.out.println("mySender...........");
        return new MySender(myListener);
    }
}
