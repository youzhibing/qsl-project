package com.qsl.circular.reference.sender;

import com.qsl.circular.reference.listener.MyListener;
import org.springframework.stereotype.Component;

/***
 * @description xxxx
 * @author 青石路
 * @date 2022/1/18 20:08
 */
@Component
public class MySender {

    private MyListener myListener;

    public MySender(MyListener myListener) {
        System.out.println("MySender 有参构造方法...");
        this.myListener = myListener;
    }

    public MySender() {
        System.out.println("MySender...");
    }
}
