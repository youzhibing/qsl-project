package com.qsl.circular.reference;

import com.qsl.circular.reference.config.MyConfig;
import com.qsl.circular.reference.listener.MyListener;
import com.qsl.circular.reference.manager.MyManager;
import com.qsl.circular.reference.service.impl.MyServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/***
 * @description 启动类
 * @author 青石路
 * @date 2022/1/18 20:02
 */
@SpringBootApplication
// @Import({MyConfig.class, MyListener.class, MyManager.class, MyServiceImpl.class})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
