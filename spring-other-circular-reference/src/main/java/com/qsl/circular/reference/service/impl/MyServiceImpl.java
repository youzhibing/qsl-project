package com.qsl.circular.reference.service.impl;

import com.qsl.circular.reference.manager.MyManager;
import com.qsl.circular.reference.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/***
 * @description xxxx
 * @author 青石路
 * @date 2022/1/18 20:16
 */
@Service
public class MyServiceImpl implements MyService {

    @Autowired
    private MyManager myManager;

    public MyServiceImpl() {
        System.out.println("MyServiceImpl...");
    }

    @Override
    public String sayHello(String name) {
        return "hello，" + name;
    }
}
