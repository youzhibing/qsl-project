package com.qsl.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2023/9/9 20:47
 */
@RestController
@RequestMapping("test")
public class TestWeb {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestWeb.class);

    @GetMapping("hello")
    public String hello(@RequestParam("name") String name) {
        LOGGER.info("hello接口入参：{}", name);
        return "hello, " + name;
    }
}
