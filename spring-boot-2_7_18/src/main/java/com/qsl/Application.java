package com.qsl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2023/9/9 20:47
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        System.setProperty("org.springframework.boot.logging.LoggingSystem", "none");
        SpringApplication.run(Application.class, args);
    }
}
