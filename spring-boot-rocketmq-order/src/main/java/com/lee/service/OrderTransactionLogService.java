package com.lee.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lee.entity.OrderTransactionLog;

/***
 * @description: OrderTransactionLog service
 * @author : 青石路
 * @date: 2021/11/14 9:32
 */
public interface OrderTransactionLogService extends IService<OrderTransactionLog> {

}
