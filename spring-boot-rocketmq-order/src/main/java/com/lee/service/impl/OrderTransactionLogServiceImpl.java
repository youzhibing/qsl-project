package com.lee.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lee.entity.OrderTransactionLog;
import com.lee.mapper.OrderTransactionLogMapper;
import com.lee.service.OrderTransactionLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/***
 * @description: xxxx
 * @author : 青石路
 * @date: 2021/11/14 9:35
 */
@Service
@Slf4j
public class OrderTransactionLogServiceImpl extends ServiceImpl<OrderTransactionLogMapper, OrderTransactionLog> implements OrderTransactionLogService {

}
