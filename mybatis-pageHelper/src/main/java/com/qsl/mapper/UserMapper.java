package com.qsl.mapper;

import com.qsl.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2022/5/8 16:34
 */
public interface UserMapper {

    /**
     *  根据姓名查询
     * @param name
     * @return
     */
    List<User> list(@Param("name") String name);
}
