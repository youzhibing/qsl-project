package com.qsl;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2022/5/8 16:35
 */
@MapperScan(basePackages = "com.qsl.mapper")
@SpringBootApplication
public class PageHelperApplication {

    public static void main(String[] args) {
        SpringApplication.run(PageHelperApplication.class, args);
    }
}
