package com.qsl.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qsl.entity.User;
import com.qsl.mapper.UserMapper;
import com.qsl.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2022/5/8 16:37
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public List<User> list(String name) {
        return userMapper.list(name);
    }

    @Override
    public PageInfo<User> listPage(int pageNum, int pageSize, String name) {
        PageHelper.startPage(pageNum, pageSize);
        List<User> users = userMapper.list(name);
        PageInfo pageInfo = new PageInfo(users);
        return pageInfo;
    }
}
