package com.qsl.service;

import com.github.pagehelper.PageInfo;
import com.qsl.entity.User;

import java.util.List;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2022/5/8 16:37
 */
public interface UserService {

    /**
     * 查询所有
     * @param name 姓名
     * @return
     */
    List<User> list(String name);

    /**
     * 分页查询
     * @param pageNum 当前页
     * @param pageSize 每页记录数
     * @param name 姓名
     * @return
     */
    PageInfo<User> listPage(int pageNum, int pageSize, String name);
}
