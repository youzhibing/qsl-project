package com.qsl.entity;

import lombok.Data;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2022/5/8 16:42
 */
@Data
public class User {

    private Long id;
    private String name;
    private Integer age;
}
