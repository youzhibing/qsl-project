package com.qsl.test;

import com.github.pagehelper.PageInfo;
import com.qsl.entity.User;
import com.qsl.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: xxx描述
 * @author: 青石路
 * @date: 2022/5/8 16:47
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Resource
    private UserService userService;

    @Test
    public void list() {
        List<User> list = userService.list("");
        System.out.println(list);
    }

    @Test
    public void listPage() {
        PageInfo<User> pageInfo = userService.listPage(1, 1, "6668");
        System.out.println();
        System.out.println(pageInfo.getList());
    }
}
