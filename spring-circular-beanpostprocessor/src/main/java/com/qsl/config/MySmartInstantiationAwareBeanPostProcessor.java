package com.qsl.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.SmartInstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Proxy;
import java.util.Objects;

/***
 * @description xxxx
 * @author 博客园 @ 青石路
 * @date 2022/2/18 15:51
 */
@Component
public class MySmartInstantiationAwareBeanPostProcessor implements SmartInstantiationAwareBeanPostProcessor {

    @Override
    public Object getEarlyBeanReference(Object bean, String beanName) throws BeansException {

        if (Objects.equals(beanName, "serviceAImpl")) {
            return Proxy.newProxyInstance(MyBeanPostProcessor.class.getClassLoader(),
                    bean.getClass().getInterfaces(), (proxy, method, args) -> method.invoke(proxy, args));
        }
        return bean;
    }
}
