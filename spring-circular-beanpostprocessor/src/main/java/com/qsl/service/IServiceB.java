package com.qsl.service;

/***
 * @description xxxx
 * @author 青石路
 * @date 2022/2/18 13:53
 */
public interface IServiceB {

    /**
     * say hi
     * @return
     */
    String sayHi();
}
