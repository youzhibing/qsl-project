package com.qsl.service;

/***
 * @description xxxx
 * @author 青石路
 * @date 2022/2/18 13:52
 */
public interface IServiceA {

    /**
     * say hello
     * @return
     */
    String sayHello();
}
