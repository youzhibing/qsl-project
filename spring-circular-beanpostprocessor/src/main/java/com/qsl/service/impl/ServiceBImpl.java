package com.qsl.service.impl;

import com.qsl.service.IServiceA;
import com.qsl.service.IServiceB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/***
 * @description xxxx
 * @author 博客园 @ 青石路
 * @date 2022/2/18 13:54
 */
@Service
public class ServiceBImpl implements IServiceB {

    @Autowired
    private IServiceA serviceA;

    @Override
    public String sayHi() {
        return null;
    }
}
