package com.qsl;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

import java.util.Random;

@Configuration
@MessageEndpoint
public class flow {

    @Bean
    public MessageChannel beep(){
        return new QueueChannel();
    }
    
    @InboundChannelAdapter(value = "beep", poller = @Poller(fixedRate = "2000"))
    public int input(){
        Random random = new Random();
        int i = random.nextInt();
        System.out.println("Generate: " + i);
        return i;
    }
    
    @ServiceActivator(inputChannel = "beep", poller = @Poller(fixedRate = "500") )
    public void output(Message<Integer> message){
        System.out.println("Msg: "+ message.getPayload());
    }

}